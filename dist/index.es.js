import React__default, { Component, createElement, createRef } from 'react';
import ReactDOM__default, { render } from 'react-dom';

function styleInject(css, ref) {
  if ( ref === void 0 ) ref = {};
  var insertAt = ref.insertAt;

  if (!css || typeof document === 'undefined') { return; }

  var head = document.head || document.getElementsByTagName('head')[0];
  var style = document.createElement('style');
  style.type = 'text/css';

  if (insertAt === 'top') {
    if (head.firstChild) {
      head.insertBefore(style, head.firstChild);
    } else {
      head.appendChild(style);
    }
  } else {
    head.appendChild(style);
  }

  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
}

var css = ".WallpaperWrapper_w_wrapper_container__2Yylw {\n  display:flex;\n  flex-direction: row;\n  width: 100%;\n  height: 100%;\n}\n\n.WallpaperWrapper_w_column__3o3wM {\n  display:flex;\n  flex-direction: column;\n  overflow: hidden;\n  flex: 1;\n}\n\n.WallpaperWrapper_w_row__3XHZb {\n  display:flex;\n  flex-direction: column;\n}\n";
var styles = { "w_wrapper_container": "WallpaperWrapper_w_wrapper_container__2Yylw", "w_column": "WallpaperWrapper_w_column__3o3wM", "w_row": "WallpaperWrapper_w_row__3XHZb" };
styleInject(css);

var commonjsGlobal = typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

function unwrapExports (x) {
	return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x.default : x;
}

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

function getCjsExportFromNamespace (n) {
	return n && n.default || n;
}

/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var ReactPropTypesSecret = 'SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED';

var ReactPropTypesSecret_1 = ReactPropTypesSecret;

function emptyFunction() {}

var factoryWithThrowingShims = function() {
  function shim(props, propName, componentName, location, propFullName, secret) {
    if (secret === ReactPropTypesSecret_1) {
      // It is still safe when called from React.
      return;
    }
    var err = new Error(
      'Calling PropTypes validators directly is not supported by the `prop-types` package. ' +
      'Use PropTypes.checkPropTypes() to call them. ' +
      'Read more at http://fb.me/use-check-prop-types'
    );
    err.name = 'Invariant Violation';
    throw err;
  }  shim.isRequired = shim;
  function getShim() {
    return shim;
  }  // Important!
  // Keep this list in sync with production version in `./factoryWithTypeCheckers.js`.
  var ReactPropTypes = {
    array: shim,
    bool: shim,
    func: shim,
    number: shim,
    object: shim,
    string: shim,
    symbol: shim,

    any: shim,
    arrayOf: getShim,
    element: shim,
    instanceOf: getShim,
    node: shim,
    objectOf: getShim,
    oneOf: getShim,
    oneOfType: getShim,
    shape: getShim,
    exact: getShim
  };

  ReactPropTypes.checkPropTypes = emptyFunction;
  ReactPropTypes.PropTypes = ReactPropTypes;

  return ReactPropTypes;
};

var propTypes = createCommonjsModule(function (module) {
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

{
  // By explicitly using `prop-types` you are opting into new production behavior.
  // http://fb.me/prop-types-in-prod
  module.exports = factoryWithThrowingShims();
}
});

var tabbable_1 = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = findTabbableDescendants;
/*!
 * Adapted from jQuery UI core
 *
 * http://jqueryui.com
 *
 * Copyright 2014 jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/category/ui-core/
 */

var tabbableNode = /input|select|textarea|button|object/;

function hidesContents(element) {
  var zeroSize = element.offsetWidth <= 0 && element.offsetHeight <= 0;

  // If the node is empty, this is good enough
  if (zeroSize && !element.innerHTML) return true;

  // Otherwise we need to check some styles
  var style = window.getComputedStyle(element);
  return zeroSize ? style.getPropertyValue("overflow") !== "visible" : style.getPropertyValue("display") == "none";
}

function visible(element) {
  var parentElement = element;
  while (parentElement) {
    if (parentElement === document.body) break;
    if (hidesContents(parentElement)) return false;
    parentElement = parentElement.parentNode;
  }
  return true;
}

function focusable(element, isTabIndexNotNaN) {
  var nodeName = element.nodeName.toLowerCase();
  var res = tabbableNode.test(nodeName) && !element.disabled || (nodeName === "a" ? element.href || isTabIndexNotNaN : isTabIndexNotNaN);
  return res && visible(element);
}

function tabbable(element) {
  var tabIndex = element.getAttribute("tabindex");
  if (tabIndex === null) tabIndex = undefined;
  var isTabIndexNaN = isNaN(tabIndex);
  return (isTabIndexNaN || tabIndex >= 0) && focusable(element, !isTabIndexNaN);
}

function findTabbableDescendants(element) {
  return [].slice.call(element.querySelectorAll("*"), 0).filter(tabbable);
}
module.exports = exports["default"];
});

unwrapExports(tabbable_1);

var focusManager = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.handleBlur = handleBlur;
exports.handleFocus = handleFocus;
exports.markForFocusLater = markForFocusLater;
exports.returnFocus = returnFocus;
exports.popWithoutFocus = popWithoutFocus;
exports.setupScopedFocus = setupScopedFocus;
exports.teardownScopedFocus = teardownScopedFocus;



var _tabbable2 = _interopRequireDefault(tabbable_1);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var focusLaterElements = [];
var modalElement = null;
var needToFocus = false;

function handleBlur() {
  needToFocus = true;
}

function handleFocus() {
  if (needToFocus) {
    needToFocus = false;
    if (!modalElement) {
      return;
    }
    // need to see how jQuery shims document.on('focusin') so we don't need the
    // setTimeout, firefox doesn't support focusin, if it did, we could focus
    // the element outside of a setTimeout. Side-effect of this implementation
    // is that the document.body gets focus, and then we focus our element right
    // after, seems fine.
    setTimeout(function () {
      if (modalElement.contains(document.activeElement)) {
        return;
      }
      var el = (0, _tabbable2.default)(modalElement)[0] || modalElement;
      el.focus();
    }, 0);
  }
}

function markForFocusLater() {
  focusLaterElements.push(document.activeElement);
}

/* eslint-disable no-console */
function returnFocus() {
  var toFocus = null;
  try {
    if (focusLaterElements.length !== 0) {
      toFocus = focusLaterElements.pop();
      toFocus.focus();
    }
    return;
  } catch (e) {
    console.warn(["You tried to return focus to", toFocus, "but it is not in the DOM anymore"].join(" "));
  }
}
/* eslint-enable no-console */

function popWithoutFocus() {
  focusLaterElements.length > 0 && focusLaterElements.pop();
}

function setupScopedFocus(element) {
  modalElement = element;

  if (window.addEventListener) {
    window.addEventListener("blur", handleBlur, false);
    document.addEventListener("focus", handleFocus, true);
  } else {
    window.attachEvent("onBlur", handleBlur);
    document.attachEvent("onFocus", handleFocus);
  }
}

function teardownScopedFocus() {
  modalElement = null;

  if (window.addEventListener) {
    window.removeEventListener("blur", handleBlur);
    document.removeEventListener("focus", handleFocus);
  } else {
    window.detachEvent("onBlur", handleBlur);
    document.detachEvent("onFocus", handleFocus);
  }
}
});

unwrapExports(focusManager);
var focusManager_1 = focusManager.handleBlur;
var focusManager_2 = focusManager.handleFocus;
var focusManager_3 = focusManager.markForFocusLater;
var focusManager_4 = focusManager.returnFocus;
var focusManager_5 = focusManager.popWithoutFocus;
var focusManager_6 = focusManager.setupScopedFocus;
var focusManager_7 = focusManager.teardownScopedFocus;

var scopeTab_1 = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = scopeTab;



var _tabbable2 = _interopRequireDefault(tabbable_1);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function scopeTab(node, event) {
  var tabbable = (0, _tabbable2.default)(node);

  if (!tabbable.length) {
    // Do nothing, since there are no elements that can receive focus.
    event.preventDefault();
    return;
  }

  var shiftKey = event.shiftKey;
  var head = tabbable[0];
  var tail = tabbable[tabbable.length - 1];

  // proceed with default browser behavior on tab.
  // Focus on last element on shift + tab.
  if (node === document.activeElement) {
    if (!shiftKey) return;
    target = tail;
  }

  var target;
  if (tail === document.activeElement && !shiftKey) {
    target = head;
  }

  if (head === document.activeElement && shiftKey) {
    target = tail;
  }

  if (target) {
    event.preventDefault();
    target.focus();
    return;
  }

  // Safari radio issue.
  //
  // Safari does not move the focus to the radio button,
  // so we need to force it to really walk through all elements.
  //
  // This is very error prone, since we are trying to guess
  // if it is a safari browser from the first occurence between
  // chrome or safari.
  //
  // The chrome user agent contains the first ocurrence
  // as the 'chrome/version' and later the 'safari/version'.
  var checkSafari = /(\bChrome\b|\bSafari\b)\//.exec(navigator.userAgent);
  var isSafariDesktop = checkSafari != null && checkSafari[1] != "Chrome" && /\biPod\b|\biPad\b/g.exec(navigator.userAgent) == null;

  // If we are not in safari desktop, let the browser control
  // the focus
  if (!isSafariDesktop) return;

  var x = tabbable.indexOf(document.activeElement);

  if (x > -1) {
    x += shiftKey ? -1 : 1;
  }

  // If the tabbable element does not exist,
  // focus head/tail based on shiftKey
  if (typeof tabbable[x] === "undefined") {
    event.preventDefault();
    target = shiftKey ? tail : head;
    target.focus();
    return;
  }

  event.preventDefault();

  tabbable[x].focus();
}
module.exports = exports["default"];
});

unwrapExports(scopeTab_1);

/**
 * Copyright 2014-2015, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

var warning = function() {};

var warning_1 = warning;

var exenv = createCommonjsModule(function (module) {
/*!
  Copyright (c) 2015 Jed Watson.
  Based on code that is Copyright 2013-2015, Facebook, Inc.
  All rights reserved.
*/
/* global define */

(function () {

	var canUseDOM = !!(
		typeof window !== 'undefined' &&
		window.document &&
		window.document.createElement
	);

	var ExecutionEnvironment = {

		canUseDOM: canUseDOM,

		canUseWorkers: typeof Worker !== 'undefined',

		canUseEventListeners:
			canUseDOM && !!(window.addEventListener || window.attachEvent),

		canUseViewport: canUseDOM && !!window.screen

	};

	if (module.exports) {
		module.exports = ExecutionEnvironment;
	} else {
		window.ExecutionEnvironment = ExecutionEnvironment;
	}

}());
});

var safeHTMLElement = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.canUseDOM = undefined;



var _exenv2 = _interopRequireDefault(exenv);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var EE = _exenv2.default;

var SafeHTMLElement = EE.canUseDOM ? window.HTMLElement : {};

var canUseDOM = exports.canUseDOM = EE.canUseDOM;

exports.default = SafeHTMLElement;
});

unwrapExports(safeHTMLElement);
var safeHTMLElement_1 = safeHTMLElement.canUseDOM;

var ariaAppHider = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.assertNodeList = assertNodeList;
exports.setElement = setElement;
exports.validateElement = validateElement;
exports.hide = hide;
exports.show = show;
exports.documentNotReadyOrSSRTesting = documentNotReadyOrSSRTesting;
exports.resetForTesting = resetForTesting;



var _warning2 = _interopRequireDefault(warning_1);



function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var globalElement = null;

function assertNodeList(nodeList, selector) {
  if (!nodeList || !nodeList.length) {
    throw new Error("react-modal: No elements were found for selector " + selector + ".");
  }
}

function setElement(element) {
  var useElement = element;
  if (typeof useElement === "string" && safeHTMLElement.canUseDOM) {
    var el = document.querySelectorAll(useElement);
    assertNodeList(el, useElement);
    useElement = "length" in el ? el[0] : el;
  }
  globalElement = useElement || globalElement;
  return globalElement;
}

function validateElement(appElement) {
  if (!appElement && !globalElement) {
    (0, _warning2.default)(false, ["react-modal: App element is not defined.", "Please use `Modal.setAppElement(el)` or set `appElement={el}`.", "This is needed so screen readers don't see main content", "when modal is opened. It is not recommended, but you can opt-out", "by setting `ariaHideApp={false}`."].join(" "));

    return false;
  }

  return true;
}

function hide(appElement) {
  if (validateElement(appElement)) {
    (appElement || globalElement).setAttribute("aria-hidden", "true");
  }
}

function show(appElement) {
  if (validateElement(appElement)) {
    (appElement || globalElement).removeAttribute("aria-hidden");
  }
}

function documentNotReadyOrSSRTesting() {
  globalElement = null;
}

function resetForTesting() {
  globalElement = null;
}
});

unwrapExports(ariaAppHider);
var ariaAppHider_1 = ariaAppHider.assertNodeList;
var ariaAppHider_2 = ariaAppHider.setElement;
var ariaAppHider_3 = ariaAppHider.validateElement;
var ariaAppHider_4 = ariaAppHider.hide;
var ariaAppHider_5 = ariaAppHider.show;
var ariaAppHider_6 = ariaAppHider.documentNotReadyOrSSRTesting;
var ariaAppHider_7 = ariaAppHider.resetForTesting;

var classList = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.dumpClassLists = dumpClassLists;
var htmlClassList = {};
var docBodyClassList = {};

function dumpClassLists() {
}

/**
 * Track the number of reference of a class.
 * @param {object} poll The poll to receive the reference.
 * @param {string} className The class name.
 * @return {string}
 */
var incrementReference = function incrementReference(poll, className) {
  if (!poll[className]) {
    poll[className] = 0;
  }
  poll[className] += 1;
  return className;
};

/**
 * Drop the reference of a class.
 * @param {object} poll The poll to receive the reference.
 * @param {string} className The class name.
 * @return {string}
 */
var decrementReference = function decrementReference(poll, className) {
  if (poll[className]) {
    poll[className] -= 1;
  }
  return className;
};

/**
 * Track a class and add to the given class list.
 * @param {Object} classListRef A class list of an element.
 * @param {Object} poll         The poll to be used.
 * @param {Array}  classes      The list of classes to be tracked.
 */
var trackClass = function trackClass(classListRef, poll, classes) {
  classes.forEach(function (className) {
    incrementReference(poll, className);
    classListRef.add(className);
  });
};

/**
 * Untrack a class and remove from the given class list if the reference
 * reaches 0.
 * @param {Object} classListRef A class list of an element.
 * @param {Object} poll         The poll to be used.
 * @param {Array}  classes      The list of classes to be untracked.
 */
var untrackClass = function untrackClass(classListRef, poll, classes) {
  classes.forEach(function (className) {
    decrementReference(poll, className);
    poll[className] === 0 && classListRef.remove(className);
  });
};

/**
 * Public inferface to add classes to the document.body.
 * @param {string} bodyClass The class string to be added.
 *                           It may contain more then one class
 *                           with ' ' as separator.
 */
var add = exports.add = function add(element, classString) {
  return trackClass(element.classList, element.nodeName.toLowerCase() == "html" ? htmlClassList : docBodyClassList, classString.split(" "));
};

/**
 * Public inferface to remove classes from the document.body.
 * @param {string} bodyClass The class string to be added.
 *                           It may contain more then one class
 *                           with ' ' as separator.
 */
var remove = exports.remove = function remove(element, classString) {
  return untrackClass(element.classList, element.nodeName.toLowerCase() == "html" ? htmlClassList : docBodyClassList, classString.split(" "));
};
});

unwrapExports(classList);
var classList_1 = classList.dumpClassLists;
var classList_2 = classList.add;
var classList_3 = classList.remove;

var ModalPortal_1 = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();



var _react2 = _interopRequireDefault(React__default);



var _propTypes2 = _interopRequireDefault(propTypes);



var focusManager$$1 = _interopRequireWildcard(focusManager);



var _scopeTab2 = _interopRequireDefault(scopeTab_1);



var ariaAppHider$$1 = _interopRequireWildcard(ariaAppHider);



var classList$$1 = _interopRequireWildcard(classList);



var _safeHTMLElement2 = _interopRequireDefault(safeHTMLElement);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// so that our CSS is statically analyzable
var CLASS_NAMES = {
  overlay: "ReactModal__Overlay",
  content: "ReactModal__Content"
};

var TAB_KEY = 9;
var ESC_KEY = 27;

var ariaHiddenInstances = 0;

var ModalPortal = function (_Component) {
  _inherits(ModalPortal, _Component);

  function ModalPortal(props) {
    _classCallCheck(this, ModalPortal);

    var _this = _possibleConstructorReturn(this, (ModalPortal.__proto__ || Object.getPrototypeOf(ModalPortal)).call(this, props));

    _this.setOverlayRef = function (overlay) {
      _this.overlay = overlay;
      _this.props.overlayRef && _this.props.overlayRef(overlay);
    };

    _this.setContentRef = function (content) {
      _this.content = content;
      _this.props.contentRef && _this.props.contentRef(content);
    };

    _this.afterClose = function () {
      var _this$props = _this.props,
          appElement = _this$props.appElement,
          ariaHideApp = _this$props.ariaHideApp,
          htmlOpenClassName = _this$props.htmlOpenClassName,
          bodyOpenClassName = _this$props.bodyOpenClassName;

      // Remove classes.

      bodyOpenClassName && classList$$1.remove(document.body, bodyOpenClassName);

      htmlOpenClassName && classList$$1.remove(document.getElementsByTagName("html")[0], htmlOpenClassName);

      // Reset aria-hidden attribute if all modals have been removed
      if (ariaHideApp && ariaHiddenInstances > 0) {
        ariaHiddenInstances -= 1;

        if (ariaHiddenInstances === 0) {
          ariaAppHider$$1.show(appElement);
        }
      }

      if (_this.props.shouldFocusAfterRender) {
        if (_this.props.shouldReturnFocusAfterClose) {
          focusManager$$1.returnFocus();
          focusManager$$1.teardownScopedFocus();
        } else {
          focusManager$$1.popWithoutFocus();
        }
      }

      if (_this.props.onAfterClose) {
        _this.props.onAfterClose();
      }
    };

    _this.open = function () {
      _this.beforeOpen();
      if (_this.state.afterOpen && _this.state.beforeClose) {
        clearTimeout(_this.closeTimer);
        _this.setState({ beforeClose: false });
      } else {
        if (_this.props.shouldFocusAfterRender) {
          focusManager$$1.setupScopedFocus(_this.node);
          focusManager$$1.markForFocusLater();
        }

        _this.setState({ isOpen: true }, function () {
          _this.setState({ afterOpen: true });

          if (_this.props.isOpen && _this.props.onAfterOpen) {
            _this.props.onAfterOpen();
          }
        });
      }
    };

    _this.close = function () {
      if (_this.props.closeTimeoutMS > 0) {
        _this.closeWithTimeout();
      } else {
        _this.closeWithoutTimeout();
      }
    };

    _this.focusContent = function () {
      return _this.content && !_this.contentHasFocus() && _this.content.focus();
    };

    _this.closeWithTimeout = function () {
      var closesAt = Date.now() + _this.props.closeTimeoutMS;
      _this.setState({ beforeClose: true, closesAt: closesAt }, function () {
        _this.closeTimer = setTimeout(_this.closeWithoutTimeout, _this.state.closesAt - Date.now());
      });
    };

    _this.closeWithoutTimeout = function () {
      _this.setState({
        beforeClose: false,
        isOpen: false,
        afterOpen: false,
        closesAt: null
      }, _this.afterClose);
    };

    _this.handleKeyDown = function (event) {
      if (event.keyCode === TAB_KEY) {
        (0, _scopeTab2.default)(_this.content, event);
      }

      if (_this.props.shouldCloseOnEsc && event.keyCode === ESC_KEY) {
        event.stopPropagation();
        _this.requestClose(event);
      }
    };

    _this.handleOverlayOnClick = function (event) {
      if (_this.shouldClose === null) {
        _this.shouldClose = true;
      }

      if (_this.shouldClose && _this.props.shouldCloseOnOverlayClick) {
        if (_this.ownerHandlesClose()) {
          _this.requestClose(event);
        } else {
          _this.focusContent();
        }
      }
      _this.shouldClose = null;
    };

    _this.handleContentOnMouseUp = function () {
      _this.shouldClose = false;
    };

    _this.handleOverlayOnMouseDown = function (event) {
      if (!_this.props.shouldCloseOnOverlayClick && event.target == _this.overlay) {
        event.preventDefault();
      }
    };

    _this.handleContentOnClick = function () {
      _this.shouldClose = false;
    };

    _this.handleContentOnMouseDown = function () {
      _this.shouldClose = false;
    };

    _this.requestClose = function (event) {
      return _this.ownerHandlesClose() && _this.props.onRequestClose(event);
    };

    _this.ownerHandlesClose = function () {
      return _this.props.onRequestClose;
    };

    _this.shouldBeClosed = function () {
      return !_this.state.isOpen && !_this.state.beforeClose;
    };

    _this.contentHasFocus = function () {
      return document.activeElement === _this.content || _this.content.contains(document.activeElement);
    };

    _this.buildClassName = function (which, additional) {
      var classNames = (typeof additional === "undefined" ? "undefined" : _typeof(additional)) === "object" ? additional : {
        base: CLASS_NAMES[which],
        afterOpen: CLASS_NAMES[which] + "--after-open",
        beforeClose: CLASS_NAMES[which] + "--before-close"
      };
      var className = classNames.base;
      if (_this.state.afterOpen) {
        className = className + " " + classNames.afterOpen;
      }
      if (_this.state.beforeClose) {
        className = className + " " + classNames.beforeClose;
      }
      return typeof additional === "string" && additional ? className + " " + additional : className;
    };

    _this.attributesFromObject = function (prefix, items) {
      return Object.keys(items).reduce(function (acc, name) {
        acc[prefix + "-" + name] = items[name];
        return acc;
      }, {});
    };

    _this.state = {
      afterOpen: false,
      beforeClose: false
    };

    _this.shouldClose = null;
    _this.moveFromContentToOverlay = null;
    return _this;
  }

  _createClass(ModalPortal, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.isOpen) {
        this.open();
      }
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps, prevState) {

      if (this.props.isOpen && !prevProps.isOpen) {
        this.open();
      } else if (!this.props.isOpen && prevProps.isOpen) {
        this.close();
      }

      // Focus only needs to be set once when the modal is being opened
      if (this.props.shouldFocusAfterRender && this.state.isOpen && !prevState.isOpen) {
        this.focusContent();
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      this.afterClose();
      clearTimeout(this.closeTimer);
    }
  }, {
    key: "beforeOpen",
    value: function beforeOpen() {
      var _props = this.props,
          appElement = _props.appElement,
          ariaHideApp = _props.ariaHideApp,
          htmlOpenClassName = _props.htmlOpenClassName,
          bodyOpenClassName = _props.bodyOpenClassName;

      // Add classes.

      bodyOpenClassName && classList$$1.add(document.body, bodyOpenClassName);

      htmlOpenClassName && classList$$1.add(document.getElementsByTagName("html")[0], htmlOpenClassName);

      if (ariaHideApp) {
        ariaHiddenInstances += 1;
        ariaAppHider$$1.hide(appElement);
      }
    }

    // Don't steal focus from inner elements

  }, {
    key: "render",
    value: function render$$1() {
      var _props2 = this.props,
          className = _props2.className,
          overlayClassName = _props2.overlayClassName,
          defaultStyles = _props2.defaultStyles;

      var contentStyles = className ? {} : defaultStyles.content;
      var overlayStyles = overlayClassName ? {} : defaultStyles.overlay;

      return this.shouldBeClosed() ? null : _react2.default.createElement(
        "div",
        {
          ref: this.setOverlayRef,
          className: this.buildClassName("overlay", overlayClassName),
          style: _extends({}, overlayStyles, this.props.style.overlay),
          onClick: this.handleOverlayOnClick,
          onMouseDown: this.handleOverlayOnMouseDown
        },
        _react2.default.createElement(
          "div",
          _extends({
            ref: this.setContentRef,
            style: _extends({}, contentStyles, this.props.style.content),
            className: this.buildClassName("content", className),
            tabIndex: "-1",
            onKeyDown: this.handleKeyDown,
            onMouseDown: this.handleContentOnMouseDown,
            onMouseUp: this.handleContentOnMouseUp,
            onClick: this.handleContentOnClick,
            role: this.props.role,
            "aria-label": this.props.contentLabel
          }, this.attributesFromObject("aria", this.props.aria || {}), this.attributesFromObject("data", this.props.data || {}), {
            "data-testid": this.props.testId
          }),
          this.props.children
        )
      );
    }
  }]);

  return ModalPortal;
}(React__default.Component);

ModalPortal.defaultProps = {
  style: {
    overlay: {},
    content: {}
  },
  defaultStyles: {}
};
ModalPortal.propTypes = {
  isOpen: _propTypes2.default.bool.isRequired,
  defaultStyles: _propTypes2.default.shape({
    content: _propTypes2.default.object,
    overlay: _propTypes2.default.object
  }),
  style: _propTypes2.default.shape({
    content: _propTypes2.default.object,
    overlay: _propTypes2.default.object
  }),
  className: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]),
  overlayClassName: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]),
  bodyOpenClassName: _propTypes2.default.string,
  htmlOpenClassName: _propTypes2.default.string,
  ariaHideApp: _propTypes2.default.bool,
  appElement: _propTypes2.default.instanceOf(_safeHTMLElement2.default),
  onAfterOpen: _propTypes2.default.func,
  onAfterClose: _propTypes2.default.func,
  onRequestClose: _propTypes2.default.func,
  closeTimeoutMS: _propTypes2.default.number,
  shouldFocusAfterRender: _propTypes2.default.bool,
  shouldCloseOnOverlayClick: _propTypes2.default.bool,
  shouldReturnFocusAfterClose: _propTypes2.default.bool,
  role: _propTypes2.default.string,
  contentLabel: _propTypes2.default.string,
  aria: _propTypes2.default.object,
  data: _propTypes2.default.object,
  children: _propTypes2.default.node,
  shouldCloseOnEsc: _propTypes2.default.bool,
  overlayRef: _propTypes2.default.func,
  contentRef: _propTypes2.default.func,
  testId: _propTypes2.default.string
};
exports.default = ModalPortal;
module.exports = exports["default"];
});

unwrapExports(ModalPortal_1);

/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

function componentWillMount() {
  // Call this.constructor.gDSFP to support sub-classes.
  var state = this.constructor.getDerivedStateFromProps(this.props, this.state);
  if (state !== null && state !== undefined) {
    this.setState(state);
  }
}

function componentWillReceiveProps(nextProps) {
  // Call this.constructor.gDSFP to support sub-classes.
  // Use the setState() updater to ensure state isn't stale in certain edge cases.
  function updater(prevState) {
    var state = this.constructor.getDerivedStateFromProps(nextProps, prevState);
    return state !== null && state !== undefined ? state : null;
  }
  // Binding "this" is important for shallow renderer support.
  this.setState(updater.bind(this));
}

function componentWillUpdate(nextProps, nextState) {
  try {
    var prevProps = this.props;
    var prevState = this.state;
    this.props = nextProps;
    this.state = nextState;
    this.__reactInternalSnapshotFlag = true;
    this.__reactInternalSnapshot = this.getSnapshotBeforeUpdate(
      prevProps,
      prevState
    );
  } finally {
    this.props = prevProps;
    this.state = prevState;
  }
}

// React may warn about cWM/cWRP/cWU methods being deprecated.
// Add a flag to suppress these warnings for this special case.
componentWillMount.__suppressDeprecationWarning = true;
componentWillReceiveProps.__suppressDeprecationWarning = true;
componentWillUpdate.__suppressDeprecationWarning = true;

function polyfill(Component$$1) {
  var prototype = Component$$1.prototype;

  if (!prototype || !prototype.isReactComponent) {
    throw new Error('Can only polyfill class components');
  }

  if (
    typeof Component$$1.getDerivedStateFromProps !== 'function' &&
    typeof prototype.getSnapshotBeforeUpdate !== 'function'
  ) {
    return Component$$1;
  }

  // If new component APIs are defined, "unsafe" lifecycles won't be called.
  // Error if any of these lifecycles are present,
  // Because they would work differently between older and newer (16.3+) versions of React.
  var foundWillMountName = null;
  var foundWillReceivePropsName = null;
  var foundWillUpdateName = null;
  if (typeof prototype.componentWillMount === 'function') {
    foundWillMountName = 'componentWillMount';
  } else if (typeof prototype.UNSAFE_componentWillMount === 'function') {
    foundWillMountName = 'UNSAFE_componentWillMount';
  }
  if (typeof prototype.componentWillReceiveProps === 'function') {
    foundWillReceivePropsName = 'componentWillReceiveProps';
  } else if (typeof prototype.UNSAFE_componentWillReceiveProps === 'function') {
    foundWillReceivePropsName = 'UNSAFE_componentWillReceiveProps';
  }
  if (typeof prototype.componentWillUpdate === 'function') {
    foundWillUpdateName = 'componentWillUpdate';
  } else if (typeof prototype.UNSAFE_componentWillUpdate === 'function') {
    foundWillUpdateName = 'UNSAFE_componentWillUpdate';
  }
  if (
    foundWillMountName !== null ||
    foundWillReceivePropsName !== null ||
    foundWillUpdateName !== null
  ) {
    var componentName = Component$$1.displayName || Component$$1.name;
    var newApiName =
      typeof Component$$1.getDerivedStateFromProps === 'function'
        ? 'getDerivedStateFromProps()'
        : 'getSnapshotBeforeUpdate()';

    throw Error(
      'Unsafe legacy lifecycles will not be called for components using new component APIs.\n\n' +
        componentName +
        ' uses ' +
        newApiName +
        ' but also contains the following legacy lifecycles:' +
        (foundWillMountName !== null ? '\n  ' + foundWillMountName : '') +
        (foundWillReceivePropsName !== null
          ? '\n  ' + foundWillReceivePropsName
          : '') +
        (foundWillUpdateName !== null ? '\n  ' + foundWillUpdateName : '') +
        '\n\nThe above lifecycles should be removed. Learn more about this warning here:\n' +
        'https://fb.me/react-async-component-lifecycle-hooks'
    );
  }

  // React <= 16.2 does not support static getDerivedStateFromProps.
  // As a workaround, use cWM and cWRP to invoke the new static lifecycle.
  // Newer versions of React will ignore these lifecycles if gDSFP exists.
  if (typeof Component$$1.getDerivedStateFromProps === 'function') {
    prototype.componentWillMount = componentWillMount;
    prototype.componentWillReceiveProps = componentWillReceiveProps;
  }

  // React <= 16.2 does not support getSnapshotBeforeUpdate.
  // As a workaround, use cWU to invoke the new lifecycle.
  // Newer versions of React will ignore that lifecycle if gSBU exists.
  if (typeof prototype.getSnapshotBeforeUpdate === 'function') {
    if (typeof prototype.componentDidUpdate !== 'function') {
      throw new Error(
        'Cannot polyfill getSnapshotBeforeUpdate() for components that do not define componentDidUpdate() on the prototype'
      );
    }

    prototype.componentWillUpdate = componentWillUpdate;

    var componentDidUpdate = prototype.componentDidUpdate;

    prototype.componentDidUpdate = function componentDidUpdatePolyfill(
      prevProps,
      prevState,
      maybeSnapshot
    ) {
      // 16.3+ will not execute our will-update method;
      // It will pass a snapshot value to did-update though.
      // Older versions will require our polyfilled will-update value.
      // We need to handle both cases, but can't just check for the presence of "maybeSnapshot",
      // Because for <= 15.x versions this might be a "prevContext" object.
      // We also can't just check "__reactInternalSnapshot",
      // Because get-snapshot might return a falsy value.
      // So check for the explicit __reactInternalSnapshotFlag flag to determine behavior.
      var snapshot = this.__reactInternalSnapshotFlag
        ? this.__reactInternalSnapshot
        : maybeSnapshot;

      componentDidUpdate.call(this, prevProps, prevState, snapshot);
    };
  }

  return Component$$1;
}

var reactLifecyclesCompat_es = /*#__PURE__*/Object.freeze({
  polyfill: polyfill
});

var Modal_1 = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.bodyOpenClassName = exports.portalClassName = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();



var _react2 = _interopRequireDefault(React__default);



var _reactDom2 = _interopRequireDefault(ReactDOM__default);



var _propTypes2 = _interopRequireDefault(propTypes);



var _ModalPortal2 = _interopRequireDefault(ModalPortal_1);



var ariaAppHider$$1 = _interopRequireWildcard(ariaAppHider);



var _safeHTMLElement2 = _interopRequireDefault(safeHTMLElement);



function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var portalClassName = exports.portalClassName = "ReactModalPortal";
var bodyOpenClassName = exports.bodyOpenClassName = "ReactModal__Body--open";

var isReact16 = _reactDom2.default.createPortal !== undefined;

var getCreatePortal = function getCreatePortal() {
  return isReact16 ? _reactDom2.default.createPortal : _reactDom2.default.unstable_renderSubtreeIntoContainer;
};

function getParentElement(parentSelector) {
  return parentSelector();
}

var Modal = function (_Component) {
  _inherits(Modal, _Component);

  function Modal() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Modal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Modal.__proto__ || Object.getPrototypeOf(Modal)).call.apply(_ref, [this].concat(args))), _this), _this.removePortal = function () {
      !isReact16 && _reactDom2.default.unmountComponentAtNode(_this.node);
      var parent = getParentElement(_this.props.parentSelector);
      parent.removeChild(_this.node);
    }, _this.portalRef = function (ref) {
      _this.portal = ref;
    }, _this.renderPortal = function (props) {
      var createPortal = getCreatePortal();
      var portal = createPortal(_this, _react2.default.createElement(_ModalPortal2.default, _extends({ defaultStyles: Modal.defaultStyles }, props)), _this.node);
      _this.portalRef(portal);
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Modal, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (!safeHTMLElement.canUseDOM) return;

      if (!isReact16) {
        this.node = document.createElement("div");
      }
      this.node.className = this.props.portalClassName;

      var parent = getParentElement(this.props.parentSelector);
      parent.appendChild(this.node);

      !isReact16 && this.renderPortal(this.props);
    }
  }, {
    key: "getSnapshotBeforeUpdate",
    value: function getSnapshotBeforeUpdate(prevProps) {
      var prevParent = getParentElement(prevProps.parentSelector);
      var nextParent = getParentElement(this.props.parentSelector);
      return { prevParent: prevParent, nextParent: nextParent };
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps, _, snapshot) {
      if (!safeHTMLElement.canUseDOM) return;
      var _props = this.props,
          isOpen = _props.isOpen,
          portalClassName = _props.portalClassName;


      if (prevProps.portalClassName !== portalClassName) {
        this.node.className = portalClassName;
      }

      var prevParent = snapshot.prevParent,
          nextParent = snapshot.nextParent;

      if (nextParent !== prevParent) {
        prevParent.removeChild(this.node);
        nextParent.appendChild(this.node);
      }

      // Stop unnecessary renders if modal is remaining closed
      if (!prevProps.isOpen && !isOpen) return;

      !isReact16 && this.renderPortal(this.props);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (!safeHTMLElement.canUseDOM || !this.node || !this.portal) return;

      var state = this.portal.state;
      var now = Date.now();
      var closesAt = state.isOpen && this.props.closeTimeoutMS && (state.closesAt || now + this.props.closeTimeoutMS);

      if (closesAt) {
        if (!state.beforeClose) {
          this.portal.closeWithTimeout();
        }

        setTimeout(this.removePortal, closesAt - now);
      } else {
        this.removePortal();
      }
    }
  }, {
    key: "render",
    value: function render$$1() {
      if (!safeHTMLElement.canUseDOM || !isReact16) {
        return null;
      }

      if (!this.node && isReact16) {
        this.node = document.createElement("div");
      }

      var createPortal = getCreatePortal();
      return createPortal(_react2.default.createElement(_ModalPortal2.default, _extends({
        ref: this.portalRef,
        defaultStyles: Modal.defaultStyles
      }, this.props)), this.node);
    }
  }], [{
    key: "setAppElement",
    value: function setAppElement(element) {
      ariaAppHider$$1.setElement(element);
    }

    /* eslint-disable react/no-unused-prop-types */

    /* eslint-enable react/no-unused-prop-types */

  }]);

  return Modal;
}(React__default.Component);

Modal.propTypes = {
  isOpen: _propTypes2.default.bool.isRequired,
  style: _propTypes2.default.shape({
    content: _propTypes2.default.object,
    overlay: _propTypes2.default.object
  }),
  portalClassName: _propTypes2.default.string,
  bodyOpenClassName: _propTypes2.default.string,
  htmlOpenClassName: _propTypes2.default.string,
  className: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.shape({
    base: _propTypes2.default.string.isRequired,
    afterOpen: _propTypes2.default.string.isRequired,
    beforeClose: _propTypes2.default.string.isRequired
  })]),
  overlayClassName: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.shape({
    base: _propTypes2.default.string.isRequired,
    afterOpen: _propTypes2.default.string.isRequired,
    beforeClose: _propTypes2.default.string.isRequired
  })]),
  appElement: _propTypes2.default.instanceOf(_safeHTMLElement2.default),
  onAfterOpen: _propTypes2.default.func,
  onRequestClose: _propTypes2.default.func,
  closeTimeoutMS: _propTypes2.default.number,
  ariaHideApp: _propTypes2.default.bool,
  shouldFocusAfterRender: _propTypes2.default.bool,
  shouldCloseOnOverlayClick: _propTypes2.default.bool,
  shouldReturnFocusAfterClose: _propTypes2.default.bool,
  parentSelector: _propTypes2.default.func,
  aria: _propTypes2.default.object,
  data: _propTypes2.default.object,
  role: _propTypes2.default.string,
  contentLabel: _propTypes2.default.string,
  shouldCloseOnEsc: _propTypes2.default.bool,
  overlayRef: _propTypes2.default.func,
  contentRef: _propTypes2.default.func
};
Modal.defaultProps = {
  isOpen: false,
  portalClassName: portalClassName,
  bodyOpenClassName: bodyOpenClassName,
  role: "dialog",
  ariaHideApp: true,
  closeTimeoutMS: 0,
  shouldFocusAfterRender: true,
  shouldCloseOnEsc: true,
  shouldCloseOnOverlayClick: true,
  shouldReturnFocusAfterClose: true,
  parentSelector: function parentSelector() {
    return document.body;
  }
};
Modal.defaultStyles = {
  overlay: {
    position: "fixed",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: "rgba(255, 255, 255, 0.75)"
  },
  content: {
    position: "absolute",
    top: "40px",
    left: "40px",
    right: "40px",
    bottom: "40px",
    border: "1px solid #ccc",
    background: "#fff",
    overflow: "auto",
    WebkitOverflowScrolling: "touch",
    borderRadius: "4px",
    outline: "none",
    padding: "20px"
  }
};


(0, reactLifecyclesCompat_es.polyfill)(Modal);

exports.default = Modal;
});

unwrapExports(Modal_1);
var Modal_2 = Modal_1.bodyOpenClassName;
var Modal_3 = Modal_1.portalClassName;

var lib = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});



var _Modal2 = _interopRequireDefault(Modal_1);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _Modal2.default;
module.exports = exports["default"];
});

var Modal$1 = unwrapExports(lib);

var css$1 = ".FilterWallpaper_w_filter_container__J3RTI {\n  border: 1px solid #ccc;\n  box-sizing: border-box;\n  padding: 15px;\n}\n\n.FilterWallpaper_w_filter_container__J3RTI div{\n  float: left;\n}\n\n.FilterWallpaper_w_filter_container_mobile__1ZSlj {\n}\n\n.FilterWallpaper_modal__1cw_H {\n  position: absolute;\n  top: 0px;\n  left: 0px;\n  right: 0px;\n  bottom: 0px;\n  background-color: white;\n}\n\n.FilterWallpaper_modal_overlay__344_R {\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background-color: white;\n}\n\n.FilterWallpaper_modal_container__qtSYf {\n  margin: 0 auto;\n  width: 500px;\n  font-size: 30px;\n}\n\n.FilterWallpaper_modal_header__2jKXW {\n  padding: 20px 30px;\n  display: flex;\n  border-bottom: 1px solid #ccc;\n  border-width: 0 2px 2px 0;\n}\n\n.FilterWallpaper_modal_header_close__1Ggbj {\n  flex-grow: 0;\n}\n\n.FilterWallpaper_modal_header_text__1WDDm {\n  flex-grow: 1;\n  text-align: center;\n  line-height: 48px;\n}\n\n.FilterWallpaper_modal_item__3xvTJ {\n  padding: 20px 30px;\n  display: flex;\n  border-bottom: 1px solid #ccc;\n  border-width: 0 2px 2px 0;\n}\n\n.FilterWallpaper_modal_item_checked__1lEpe {\n  flex-grow: 0;\n}\n\n.FilterWallpaper_modal_item_text__2wBtp {\n  flex-grow: 1;\n}\n\n.FilterWallpaper_label_header__2zM-S {\n  font-weight: bold;\n  display: block;\n  margin-bottom: 10px;\n  font-size: 13px;\n}\n\ninput[type=checkbox] {\n  position: relative;\n  cursor: pointer;\n  margin: 0;\n}\ninput[type=checkbox]:before {\n  content: \"\";\n  display: block;\n  position: absolute;\n  width: 14px;\n  height: 14px;\n  top: 0;\n  left: 0;\n  border: 1px solid #c0c0c0;\n  border-radius: 3px;\n  background-color: white;\n}\ninput[type=checkbox]:checked:after {\n  content: \"\";\n  display: block;\n  width: 3px;\n  height: 7px;\n  border: solid #4dd0e1;\n  border-width: 0 2px 2px 0;\n  transform: rotate(45deg);\n  position: absolute;\n  top: 2px;\n  left: 6px;\n}\ninput[type=checkbox] + label {\n  margin-left: 8px;\n  vertical-align: middle;\n  font-weight: bold;\n  font-size: 13px;\n}\n\n.FilterWallpaper_checkboxWrapper__2sT4F {\n  margin-left: 8px;\n  vertical-align: middle;\n  font-weight: bold;\n  font-size: 13px;\n  position: relative;\n}\n\n";
var styles$1 = { "w_filter_container": "FilterWallpaper_w_filter_container__J3RTI", "w_filter_container_mobile": "FilterWallpaper_w_filter_container_mobile__1ZSlj", "modal": "FilterWallpaper_modal__1cw_H", "modal_overlay": "FilterWallpaper_modal_overlay__344_R", "modal_container": "FilterWallpaper_modal_container__qtSYf", "modal_header": "FilterWallpaper_modal_header__2jKXW", "modal_header_close": "FilterWallpaper_modal_header_close__1Ggbj", "modal_header_text": "FilterWallpaper_modal_header_text__1WDDm", "modal_item": "FilterWallpaper_modal_item__3xvTJ", "modal_item_checked": "FilterWallpaper_modal_item_checked__1lEpe", "modal_item_text": "FilterWallpaper_modal_item_text__2wBtp", "label_header": "FilterWallpaper_label_header__2zM-S", "checkboxWrapper": "FilterWallpaper_checkboxWrapper__2sT4F" };
styleInject(css$1);

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

var defineProperty = function (obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
};

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var get = function get(object, property, receiver) {
  if (object === null) object = Function.prototype;
  var desc = Object.getOwnPropertyDescriptor(object, property);

  if (desc === undefined) {
    var parent = Object.getPrototypeOf(object);

    if (parent === null) {
      return undefined;
    } else {
      return get(parent, property, receiver);
    }
  } else if ("value" in desc) {
    return desc.value;
  } else {
    var getter = desc.get;

    if (getter === undefined) {
      return undefined;
    }

    return getter.call(receiver);
  }
};

var inherits = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
};

var possibleConstructorReturn = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
};

var toConsumableArray = function (arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

    return arr2;
  } else {
    return Array.from(arr);
  }
};

var DataSourceState = {
  DEFAULT: 'DEFAULT',
  IN_PROGRESS: 'IN_PROGRESS',
  PARTIALLY_LOADED: 'PARTIALLY_LOADED',
  ALL_LOADED: 'ALL_LOADED'
};

var DataSource = function () {
  function DataSource(config) {
    classCallCheck(this, DataSource);
    this.result = [];
    this.state = DataSourceState.DEFAULT;

    this.config = config;
    this.url = config.url;
    this.pagination = config.pagination;
    this.data = config.data;
  }

  createClass(DataSource, [{
    key: 'getItems',
    value: function getItems() {
      var _this = this;

      var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var more = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      if (this.state === DataSourceState.IN_PROGRESS) {
        return Promise.reject(new Error(this.state));
      }
      this.state = DataSourceState.IN_PROGRESS;
      var data = this.config.data;
      if (data) {
        return new Promise(function (resolve, reject) {
          var result = more ? data.slice(0, params.offset + params.limit) : data;
          resolve(result);
        });
      }
      return this.get(this.config.transformParams ? this.config.transformParams(params) : params).then(this.config.transform ? this.config.transform : function (response) {
        return response;
      }).then(function (response) {
        _this.result = _this.result.concat(response);
        _this.state = more ? DataSourceState.PARTIALLY_LOADED : DataSourceState.ALL_LOADED;
        return _this.result;
      });
    }
  }, {
    key: 'loadMore',
    value: function loadMore() {
      var _this2 = this;

      if (this.state === DataSourceState.ALL_LOADED) {
        return Promise.reject(new Error(this.state));
      }
      return this.getItems({
        offset: this.pagination.offset,
        limit: this.pagination.limit
      }, true).then(function (response) {
        _this2.pagination.offset = _this2.pagination.offset + _this2.pagination.limit;
        _this2.state = _this2.pagination.offset === response.length ? DataSourceState.PARTIALLY_LOADED : DataSourceState.ALL_LOADED;
        return response;
      });
    }
  }, {
    key: 'get',
    value: function get$$1() {
      var searchParams = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

      var tempUrl = this.url;
      if (params.length) {
        params.forEach(function (value, index) {
          tempUrl = tempUrl.replace(new RegExp("\\{" + index + "\\}", "g"), value);
        });
      }
      var url = new URL(tempUrl);
      Object.keys(searchParams).forEach(function (key) {
        return url.searchParams.append(key, searchParams[key]);
      });
      return fetch(url).then(function (response) {
        return response.json();
      });
    }
  }, {
    key: 'post',
    value: function post(data) {
      return fetch(this.url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      }).then(function (response) {
        return response.json();
      });
    }
  }]);
  return DataSource;
}();

var filterImg = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJMAAAB3CAIAAACIUuXWAAAAA3NCSVQICAjb4U/gAAAAEHRFWHRTb2Z0d2FyZQBTaHV0dGVyY4LQCQAABppJREFUeNrtnU9s01Ycx58d23Hs1EnTNIT+IaMtpUWUCWkMBkUMNolNaNoRpE2atsu4wrQdxmkHDtuEdpq0G+w0pN0mtCFN7B9CA8EmNKS2/G9R/2Rt06T5HyexdwAaJ23SLNBnO/1+Tm1q9J794f38e3/D6LpOgA3hCCHHro7gQdiL83u2sXgKNgXmYA7AHIA5mAMwB2AO5gDMAZgDMAdzAOYAzMEcgDkAcwDmYA7AHIA5mAP2g6Nf5LDf814o2MI7KJcbzqpf35u6m8ygzTWCzDk+7Omgr40QEhSF470diJYN4hd4nmXMutuAU0C0XB9ZAENaq/9Py2t6LF9Yp+Yuzy9OpnNrWsR2jzzkkRv4hy9I4icD3T6Br3HN3WTm87FHyUJx3Zm7FonfiCbWtAieZRoz905oQ21thJAtbtebQd/3k3PoFVgFnmUGWqQ62zT6cxaiT3bVmUn1ul2CGTkXMpSVGVDKGty34+F0UVv6tUcWDwd9T54gw/S5XSPxNMxZw5whVEbVwk/hBeNfR+L8kjlCyKAi0zeHaLlyZ2CrwdxoIlVxwVwuH1HzJXP1vRFhbs3ZLImio/RkxlZqT6OGD7e0uBwMA3PmM6jI1SSVdCZKHzpZtkcWYc5aL7lkoTiZydVuc8szGpgzAaZcw1g8veKxI1OZXNww9EX/VWdybnmiv0tb4/NY/m9fq0tyujnHilFxecB82ac8/nlri8QQoq8fcw6GcTDWanMVrWc0nqp25Wi8ZE7mHJskcSKdbVpzeU0z0UqhjgOXjKEyp2lTWbXaYMr9VLY8r5Ga2dxMVr2XzPS5XaaYuzK/WEebk41J47ldA/XnNRfLO+xNZU4n5PToxMGA18PTLno6o16ej9W+JuAUWoUGKzZAN0kx4T2XKWo/ziwQS+LmGk+2ZY7qEg30CsoYT2cfNLrE6JfZ6DrqFVgNTSefjUzsbVOU8mAekp172zxLv/4wHUkZ5sF1QmayuRsLCZgzk5ym/TpX+Trc26YYzf3878JcLm9uPREt7QrM2RUrRkuZc7Q7eZ4xf3BF1fR5NZ8yY2mXzcy91Nrydqe/z+2yzoiYTsjdRHrF6QKYexK1P9i88fUNrVZ7QAwh/S1Svxmz3vZ4zx3tDlhQGzKUVeh0Od/qaIMM+0XLgwEva8hHxhLpi+GFTFEzvWIuB/tG0DdgvVBpFXPGhVazufzp0Ym8ZpXvv/grmjjzYl/AySNaroBiGKu9nUhbRxshJK/ptxNpC7Y5S5gzhkrNet82o1nyC3AwhoIMpe7u0e425UC7NyQ5RZZ9mgiUouU+v2en1z2dVa9F4pdmo6pJkVNgmdcCrbvblA5RkJZNvH2xo1fX9aymTaRzv8/FrkXienObkxzsif6uIY+7VoUYRuE5hecGWqTDQd+Xtx9NZVTqvRTh462bgqJQI+ckhEjE4RP4nV73rUDyqzuTabrJMEuztZ3s766trYKgKJwaDCl0jwNQeMepwVANbcsZ8rhP9ndTHrGjZ25/u7eBTYI+gT/aHaD5RI52B1bdqrqc7R55f7u3OaPloUDZjf2zmKy2Q1zhuVfalKU9FsN+z7nxMJ2uAs8yw/7SDGpR1/+MxONVtvF3Sc4dhhByKOD9Yy7WhOZ65NJKvesL8TN3JmtcPJ7Kvhva8PhnJ8t2is5xKksZO0Wnky3Foe8ezV6YidS4/qP+rl1PF8sab7CpoqVxS+695CoaHqTKlvFIHKV6VhRUUY3lGG+E8p5j9OfsCszBHIA5AHMwB2AO2K8n/ix8OhiiM0fGMDD3fGvJMIRBM0O0hDmAaLkKV+YX51Uau578Ar/PMFcAc8/KpdkoncPstimSXcwhWuI9B2AOwBzMAZgD9ja36oHyPFNWsSKtJcQVBVVUo4EbaQZzUbW09u1Au3dj9aWoMuc40uEzfhJRKR0+UlHQkQ5fjTOhNorCAcMaS+MNUoDRdf3Y1REKJR3v7Xi1fC1ptSWUHFs5ZE9zX1ZFM9IJKVQpveLK3+Zi39yfplPJ83u20RtDuTAdGfZ7OIOU+kONiUGJqa/0gq5fmI40Z7SczOTOPgxbcSfaM6MTcvZhmPLJG1THLS/NRuP5wvubgw0s3LcsC2r+7MPw9WiCcrm0R5yvRxN/x5JDHjkkiSbGwOdCXtMn0tlbi6miGZtaTZgrKOr6zVjyZiyJPhl64jAHYA7AHIA5mAMwB2AO5gDMAZgDMAdzAOYAzMEcgDkAcwDmYA7AHIA5mAN2gNF1HU/BjvwHIX/uvru10/AAAAAASUVORK5CYII=";

var closeImg = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAIAAADYYG7QAAAAA3NCSVQICAjb4U/gAAAAEHRFWHRTb2Z0d2FyZQBTaHV0dGVyY4LQCQAAAydJREFUWMO92MlO20AYB/DPo2aRgDZxWJyEQxegtOqlLUWteA2gOQF9iYSMz3aTpwDBodDAc5RDoZcKNYgul4rEW2lySqzYPcRJoPEy3vK/WBp/Y/0O488zpnRdh140TaMoiqIoGFV0Xdd1HSHUH7nTvXy7/L53eHRevYhEI2+WXr7LrSUTiVApyvX17oePn07P1Lb6ZGF+M7e2OPcIAChd16uXP4pcSVXVfvXM1CTPbk9PpkLSCJKEuXJdlPojkUjkPbv9eO4hAoD9yvFNDQDURQlzJUGSR6MBAFVV9yvHAIAA4Lx6MTwtJJMgycOabroMBADxeMx0cuAmQZIxVzLVAEA8HjdAK8uvrB5RFyXMlQMx2WsAYGV5yQBtrK/OZtLWJtG/yVEzm0lvvl01QBPjYxzOZ9NMSCZnTTrN4/z42JgBAgA6keDZgqNJdG8i0XBsvt/2Bi2SxFR0aSLU0DeaMLp5m04keByYyeYNt9L8DwIAOhmMqacRrQqyaWZYYwJyYZJlPxqeLdBmn0tkOoHEhC1Mog+NJYjEVBNMTKIkF31o7EAeTEQabKcxth8OG5c/15gv/76qWRUw01M8WwAdiDRJh22WM4jQpOvgX0MKIjHZhFzjsIZcradANC5AAxPDhKdxB+qaOJbU5EHjGgQAKTKTN40XEABoWqejdexrOp2OpmkeHu4aJMoK5so1QbQvqwlikStLshIuSJQVzJUcNT2T4MGEQtJ4NqHwNN5MRCDJSZNlmAwzE4gJkWiKtqs4yzAcW+DZQiAmRKYRbDX5VDKRSiaJTIriHUSsSfZ6prMJO5lQUBpC01XdwYS8aTLMzLAmEBPypuHZgqnGvwkFrvFpugWSFAXzAWgGJuzahG5puPJVPRiNYaJdm1B4Gm8mBACNZjMkjStTo9k0QDsHlfA05Kadg4oBOvl8ZqfBfjWEppPTMwPUarftNHQAGhJTq9U2QM8WF0ag6Zs4C1OXYfyFjUWjI9B0M2lmikWjG+urg6P05c9fe4dHX6sXsVj09YvnW7n1e3cnIMz8bTR2Dyonp19a7fbThfmt3Nrcg/suzvYjyz9+zM++/HzJZQAAAABJRU5ErkJggg==";

var checkedImg = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAfCAIAAAAEKiicAAAAA3NCSVQICAjb4U/gAAAAEHRFWHRTb2Z0d2FyZQBTaHV0dGVyY4LQCQAAAvFJREFUSMfFlk9o01Acx9/L36bdmrZr085N+yeiDlHQg4IHETx48bLLQCiCFwXBgyAMtHMb61QmCB68KIggiCBMQQXxIqjMi+DBgwdpt45tmjT9l25d2ySNh9X9yfLadLT6OyW/93755P37fh/UdR10PjQd4LD+THQa9kYqTouypGhBGxkNuI6zDOzoKB8t5l5LxfVXCEAs7Osg8smv/LQoG5J7GapTyGe/C8+FwvY8BgHWCd4LQTblAQBYHG8/8lVafvo7j2o97XG0GflWKj5eQvIOddHnAmw7ke8zyw8Xc6jWAQc9EuZICNuG/JBbebCQRW3FfXZqNOyzYXCLFKi6/jlfmi8rXhI/5XbYW1nmT/nS/fkMiscz1FiEW/9g/ZAU1dqNhDBXVtayHhIfD3NBhrTC+1IoTaUkDQEM2shJ3u8kNgaA/T22uXUeACCraCNJcWFTBhVf5dW7qQyK10+TEzy3mbeBnCmsGnrnVS2WFJcqagPet2L59pykIsSklybiPOcicKMaAAB0AMq12vaarKLFEoJQNad+Xy7fmksrCB5HEfEI5yFxEwFaU9sBB21aKSlaLCFKimbI/1ipTMymKzVznpfEJ3nOR5nbVH1iz/e6SAhNewhVNZYQspuoP0vV8dl0GcFzE3ic9/sppC3WkQfs9HDIS5hDwVJFjSXEgqoBAJKr1dGkWNJq5hJKYHGe20U3suEtTjJTKE2lpBp6u1/qc99JSbJqzuvGsUneH2p2tIzm9TFfupeSaq2rjwPH4jzHM1TTnkaJOemyX9ndA1vkMRgci/is8EyQa/5yud9jnWfD4M0It99OW+xvLqRnerou9rmt1FMYjIV9Bx209V9EavdZb/eFXlfjYhLC6yHf4S5bS6vQyC4GOWc0wKJaCQiGQ96j3bZWN1oThxrys0N+1vTWdC3oPeZkduCszU0xGmAHfU5DzdU9PSdY+87M3Oql8l1m+aUoS4oWYshowHWk9flsGdnGwMA/j/+A/AOH1xK2vncHNQAAAABJRU5ErkJggg==";

var FilterWallpaper = function (_React$Component) {
  inherits(FilterWallpaper, _React$Component);

  function FilterWallpaper(props) {
    classCallCheck(this, FilterWallpaper);

    var _this = possibleConstructorReturn(this, (FilterWallpaper.__proto__ || Object.getPrototypeOf(FilterWallpaper)).call(this, props));

    _this.state = { isAllSelect: true };
    return _this;
  }

  createClass(FilterWallpaper, [{
    key: 'handlerFilterChange',
    value: function handlerFilterChange(selectedItem) {
      selectedItem.isSelect = !selectedItem.isSelect;
      var currentData = this.props.dataSource.map(function (item) {
        return item.id === selectedItem.id ? selectedItem : item;
      });
      this.setState({
        isAllSelect: !currentData.find(function (data) {
          return !data.isSelect;
        })
      });
      this.props.onChange(currentData);
    }
  }, {
    key: 'handlerAllFilterChange',
    value: function handlerAllFilterChange() {
      var isAllSelect = !this.state.isAllSelect;
      this.setState({
        isAllSelect: isAllSelect
      });
      this.props.onChange(this.props.dataSource.map(function (item) {
        item.isSelect = isAllSelect;
        return item;
      }));
    }
  }, {
    key: 'createFilterItems',
    value: function createFilterItems() {
      var _this2 = this;

      return (this.props.dataSource || []).map(function (item) {
        return createElement(
          'div',
          { key: item.id },
          createElement(
            'label',
            { className: styles$1.checkboxWrapper },
            createElement('input', { type: 'checkbox',
              checked: item.isSelect,
              onChange: function onChange() {
                return _this2.handlerFilterChange(item);
              } }),
            createElement('span', null),
            item.title
          )
        );
      });
    }
  }, {
    key: 'render',
    value: function render$$1() {
      var _this3 = this;

      return createElement(
        'div',
        { className: styles$1.w_filter_container, style: this.props.style },
        createElement(
          'label',
          { className: styles$1.label_header },
          this.props.properties.header
        ),
        createElement(
          'div',
          { key: 'all' },
          createElement(
            'label',
            { className: styles$1.checkboxWrapper },
            createElement('input', { type: 'checkbox',
              checked: this.state.isAllSelect,
              onChange: function onChange() {
                return _this3.handlerAllFilterChange();
              } }),
            createElement('span', null),
            '\u0412\u0441\u0435'
          )
        ),
        this.createFilterItems()
      );
    }
  }]);
  return FilterWallpaper;
}(Component);

var FilterWallpaperMobile = function (_FilterWallpaper) {
  inherits(FilterWallpaperMobile, _FilterWallpaper);

  function FilterWallpaperMobile(props) {
    classCallCheck(this, FilterWallpaperMobile);

    var _this4 = possibleConstructorReturn(this, (FilterWallpaperMobile.__proto__ || Object.getPrototypeOf(FilterWallpaperMobile)).call(this, props));

    _this4.openModal = function () {
      _this4.setState({ showModal: true });
    };

    _this4.closeModal = function () {
      _this4.setState({ showModal: false });
    };

    _this4.createFilterItems = function () {
      return (_this4.props.dataSource || []).map(function (item) {
        return createElement(
          'div',
          { className: styles$1.modal_item, key: item.id, onClick: function onClick() {
              return _this4.handlerFilterChange(item);
            } },
          createElement(
            'div',
            { className: styles$1.modal_item_text },
            item.title
          ),
          createElement(
            'div',
            { className: styles$1.modal_item_checked },
            createElement('img', { style: { visibility: item.isSelect ? 'visible' : 'hidden' }, src: checkedImg })
          )
        );
      });
    };

    _this4.state = _extends({}, _this4.state, { showModal: false });
    return _this4;
  }

  createClass(FilterWallpaperMobile, [{
    key: 'render',
    value: function render$$1() {
      var _this5 = this;

      return createElement(
        'div',
        { className: styles$1.w_filter_container_mobile + ' ' + this.props.className, style: this.props.style },
        createElement('img', {
          src: filterImg,
          onClick: this.openModal,
          width: this.props.properties.image.width,
          height: this.props.properties.image.height
        }),
        createElement(
          Modal$1,
          {
            isOpen: this.state.showModal,
            onRequestClose: this.handleCloseModal,
            className: styles$1.modal,
            overlayClassName: styles$1.modal_overlay
          },
          createElement(
            'div',
            { className: styles$1.modal_container },
            createElement(
              'div',
              { className: styles$1.modal_header },
              createElement(
                'div',
                { className: styles$1.modal_header_close, onClick: this.closeModal },
                createElement('img', { src: closeImg })
              ),
              createElement(
                'div',
                { className: styles$1.modal_header_text },
                this.props.properties.header
              )
            ),
            createElement(
              'div',
              { className: styles$1.modal_item, key: 'all', onClick: function onClick() {
                  return _this5.handlerAllFilterChange();
                } },
              createElement(
                'div',
                { className: styles$1.modal_item_text },
                '\u0412\u0441\u0435'
              ),
              createElement(
                'div',
                { className: styles$1.modal_item_checked },
                createElement('img', { style: { visibility: this.state.isAllSelect ? 'visible' : 'hidden' }, src: checkedImg })
              )
            ),
            this.createFilterItems()
          )
        )
      );
    }
  }]);
  return FilterWallpaperMobile;
}(FilterWallpaper);

var css$2 = ".PreviewResult_w_preview_container__2pAin {\n  border: 1px solid #ccc;\n  box-sizing: border-box;\n  height: 500px;\n  position: relative;\n}\n\n.PreviewResult_preview_image__3r8RE {\n  width: 100%;\n  height: 100%;\n  object-fit: contain;\n}\n\n.PreviewResult_loader_container__1HWSO {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  background-color: rgba(0, 0, 0, 0.7);\n}\n\n.PreviewResult_loader_container__1HWSO > div {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  width: 200px;\n  height: 86px;\n  margin: auto;\n}\n\n.PreviewResult_full_screen_image__2ZphS {\n  position: absolute;\n  top: 8px;\n  right: 16px;\n}\n";
var styles$2 = { "w_preview_container": "PreviewResult_w_preview_container__2pAin", "preview_image": "PreviewResult_preview_image__3r8RE", "loader_container": "PreviewResult_loader_container__1HWSO", "full_screen_image": "PreviewResult_full_screen_image__2ZphS" };
styleInject(css$2);

var global$1 = (typeof global !== "undefined" ? global :
            typeof self !== "undefined" ? self :
            typeof window !== "undefined" ? window : {});

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _extends$1() {
  _extends$1 = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends$1.apply(this, arguments);
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};
    var ownKeys = Object.keys(source);

    if (typeof Object.getOwnPropertySymbols === 'function') {
      ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) {
        return Object.getOwnPropertyDescriptor(source, sym).enumerable;
      }));
    }

    ownKeys.forEach(function (key) {
      _defineProperty(target, key, source[key]);
    });
  }

  return target;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _possibleConstructorReturn(self, call) {
  if (call && (typeof call === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _slicedToArray(arr, i) {
  return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest();
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

    return arr2;
  }
}

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

function _iterableToArrayLimit(arr, i) {
  var _arr = [];
  var _n = true;
  var _d = false;
  var _e = undefined;

  try {
    for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) _i["return"]();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance");
}

/**
 * Placeholder for future translate functionality
 */
function translate(str) {
  var replaceStrings = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

  if (!str) {
    return '';
  }

  var translated = str;

  if (replaceStrings) {
    Object.keys(replaceStrings).forEach(function (placeholder) {
      translated = translated.replace(placeholder, replaceStrings[placeholder]);
    });
  }

  return translated;
}
function getWindowWidth() {
  return typeof global$1.window !== 'undefined' ? global$1.window.innerWidth : 0;
}
function getWindowHeight() {
  return typeof global$1.window !== 'undefined' ? global$1.window.innerHeight : 0;
} // Get the highest window context that isn't cross-origin
// (When in an iframe)

function getHighestSafeWindowContext() {
  var self = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : global$1.window.self;

  // If we reached the top level, return self
  if (self === global$1.window.top) {
    return self;
  }

  var getOrigin = function getOrigin(href) {
    return href.match(/(.*\/\/.*?)(\/|$)/)[1];
  }; // If parent is the same origin, we can move up one context
  // Reference: https://stackoverflow.com/a/21965342/1601953


  if (getOrigin(self.location.href) === getOrigin(self.document.referrer)) {
    return getHighestSafeWindowContext(self.parent);
  } // If a different origin, we consider the current level
  // as the top reachable one


  return self;
}

// Min image zoom level
var MIN_ZOOM_LEVEL = 0; // Max image zoom level

var MAX_ZOOM_LEVEL = 300; // Size ratio between previous and next zoom levels

var ZOOM_RATIO = 1.007; // How much to increase/decrease the zoom level when the zoom buttons are clicked

var ZOOM_BUTTON_INCREMENT_SIZE = 100; // Used to judge the amount of horizontal scroll needed to initiate a image move

var WHEEL_MOVE_X_THRESHOLD = 200; // Used to judge the amount of vertical scroll needed to initiate a zoom action

var WHEEL_MOVE_Y_THRESHOLD = 1;
var KEYS = {
  ESC: 27,
  LEFT_ARROW: 37,
  RIGHT_ARROW: 39
}; // Actions

var ACTION_NONE = 0;
var ACTION_MOVE = 1;
var ACTION_SWIPE = 2;
var ACTION_PINCH = 3;

var SOURCE_ANY = 0;
var SOURCE_MOUSE = 1;
var SOURCE_TOUCH = 2;
var SOURCE_POINTER = 3; // Minimal swipe distance

var MIN_SWIPE_DISTANCE = 200;

var ReactImageLightbox =
/*#__PURE__*/
function (_Component) {
  _inherits(ReactImageLightbox, _Component);

  _createClass(ReactImageLightbox, null, [{
    key: "isTargetMatchImage",
    value: function isTargetMatchImage(target) {
      return target && /ril-image-current/.test(target.className);
    }
  }, {
    key: "parseMouseEvent",
    value: function parseMouseEvent(mouseEvent) {
      return {
        id: 'mouse',
        source: SOURCE_MOUSE,
        x: parseInt(mouseEvent.clientX, 10),
        y: parseInt(mouseEvent.clientY, 10)
      };
    }
  }, {
    key: "parseTouchPointer",
    value: function parseTouchPointer(touchPointer) {
      return {
        id: touchPointer.identifier,
        source: SOURCE_TOUCH,
        x: parseInt(touchPointer.clientX, 10),
        y: parseInt(touchPointer.clientY, 10)
      };
    }
  }, {
    key: "parsePointerEvent",
    value: function parsePointerEvent(pointerEvent) {
      return {
        id: pointerEvent.pointerId,
        source: SOURCE_POINTER,
        x: parseInt(pointerEvent.clientX, 10),
        y: parseInt(pointerEvent.clientY, 10)
      };
    } // Request to transition to the previous image

  }, {
    key: "getTransform",
    value: function getTransform(_ref) {
      var _ref$x = _ref.x,
          x = _ref$x === void 0 ? 0 : _ref$x,
          _ref$y = _ref.y,
          y = _ref$y === void 0 ? 0 : _ref$y,
          _ref$zoom = _ref.zoom,
          zoom = _ref$zoom === void 0 ? 1 : _ref$zoom,
          width = _ref.width,
          targetWidth = _ref.targetWidth;
      var nextX = x;
      var windowWidth = getWindowWidth();

      if (width > windowWidth) {
        nextX += (windowWidth - width) / 2;
      }

      var scaleFactor = zoom * (targetWidth / width);
      return {
        transform: "translate3d(".concat(nextX, "px,").concat(y, "px,0) scale3d(").concat(scaleFactor, ",").concat(scaleFactor, ",1)")
      };
    }
  }]);

  function ReactImageLightbox(props) {
    var _this;

    _classCallCheck(this, ReactImageLightbox);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ReactImageLightbox).call(this, props));
    _this.state = {
      //-----------------------------
      // Animation
      //-----------------------------
      // Lightbox is closing
      // When Lightbox is mounted, if animation is enabled it will open with the reverse of the closing animation
      isClosing: !props.animationDisabled,
      // Component parts should animate (e.g., when images are moving, or image is being zoomed)
      shouldAnimate: false,
      //-----------------------------
      // Zoom settings
      //-----------------------------
      // Zoom level of image
      zoomLevel: MIN_ZOOM_LEVEL,
      //-----------------------------
      // Image position settings
      //-----------------------------
      // Horizontal offset from center
      offsetX: 0,
      // Vertical offset from center
      offsetY: 0,
      // image load error for srcType
      loadErrorStatus: {}
    }; // Refs

    _this.outerEl = React__default.createRef();
    _this.zoomInBtn = React__default.createRef();
    _this.zoomOutBtn = React__default.createRef();
    _this.caption = React__default.createRef();
    _this.closeIfClickInner = _this.closeIfClickInner.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleImageDoubleClick = _this.handleImageDoubleClick.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleImageMouseWheel = _this.handleImageMouseWheel.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleKeyInput = _this.handleKeyInput.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleMouseUp = _this.handleMouseUp.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleMouseDown = _this.handleMouseDown.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleMouseMove = _this.handleMouseMove.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleOuterMousewheel = _this.handleOuterMousewheel.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleTouchStart = _this.handleTouchStart.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleTouchMove = _this.handleTouchMove.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleTouchEnd = _this.handleTouchEnd.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handlePointerEvent = _this.handlePointerEvent.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleCaptionMousewheel = _this.handleCaptionMousewheel.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleWindowResize = _this.handleWindowResize.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleZoomInButtonClick = _this.handleZoomInButtonClick.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleZoomOutButtonClick = _this.handleZoomOutButtonClick.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.requestClose = _this.requestClose.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.requestMoveNext = _this.requestMoveNext.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.requestMovePrev = _this.requestMovePrev.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    return _this;
  }

  _createClass(ReactImageLightbox, [{
    key: "componentWillMount",
    value: function componentWillMount() {
      // Timeouts - always clear it before umount
      this.timeouts = []; // Current action

      this.currentAction = ACTION_NONE; // Events source

      this.eventsSource = SOURCE_ANY; // Empty pointers list

      this.pointerList = []; // Prevent inner close

      this.preventInnerClose = false;
      this.preventInnerCloseTimeout = null; // Used to disable animation when changing props.mainSrc|nextSrc|prevSrc

      this.keyPressed = false; // Used to store load state / dimensions of images

      this.imageCache = {}; // Time the last keydown event was called (used in keyboard action rate limiting)

      this.lastKeyDownTime = 0; // Used for debouncing window resize event

      this.resizeTimeout = null; // Used to determine when actions are triggered by the scroll wheel

      this.wheelActionTimeout = null;
      this.resetScrollTimeout = null;
      this.scrollX = 0;
      this.scrollY = 0; // Used in panning zoomed images

      this.moveStartX = 0;
      this.moveStartY = 0;
      this.moveStartOffsetX = 0;
      this.moveStartOffsetY = 0; // Used to swipe

      this.swipeStartX = 0;
      this.swipeStartY = 0;
      this.swipeEndX = 0;
      this.swipeEndY = 0; // Used to pinch

      this.pinchTouchList = null;
      this.pinchDistance = 0; // Used to differentiate between images with identical src

      this.keyCounter = 0; // Used to detect a move when all src's remain unchanged (four or more of the same image in a row)

      this.moveRequested = false;

      if (!this.props.animationDisabled) {
        // Make opening animation play
        this.setState({
          isClosing: false
        });
      }
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      // Prevents cross-origin errors when using a cross-origin iframe
      this.windowContext = getHighestSafeWindowContext();
      this.listeners = {
        resize: this.handleWindowResize,
        mouseup: this.handleMouseUp,
        touchend: this.handleTouchEnd,
        touchcancel: this.handleTouchEnd,
        pointerdown: this.handlePointerEvent,
        pointermove: this.handlePointerEvent,
        pointerup: this.handlePointerEvent,
        pointercancel: this.handlePointerEvent
      };
      Object.keys(this.listeners).forEach(function (type) {
        _this2.windowContext.addEventListener(type, _this2.listeners[type]);
      });
      this.loadAllImages();
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      var _this3 = this;

      // Iterate through the source types for prevProps and nextProps to
      //  determine if any of the sources changed
      var sourcesChanged = false;
      var prevSrcDict = {};
      var nextSrcDict = {};
      this.getSrcTypes().forEach(function (srcType) {
        if (_this3.props[srcType.name] !== nextProps[srcType.name]) {
          sourcesChanged = true;
          prevSrcDict[_this3.props[srcType.name]] = true;
          nextSrcDict[nextProps[srcType.name]] = true;
        }
      });

      if (sourcesChanged || this.moveRequested) {
        // Reset the loaded state for images not rendered next
        Object.keys(prevSrcDict).forEach(function (prevSrc) {
          if (!(prevSrc in nextSrcDict) && prevSrc in _this3.imageCache) {
            _this3.imageCache[prevSrc].loaded = false;
          }
        });
        this.moveRequested = false; // Load any new images

        this.loadAllImages(nextProps);
      }
    }
  }, {
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate() {
      // Wait for move...
      return !this.moveRequested;
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      var _this4 = this;

      this.didUnmount = true;
      Object.keys(this.listeners).forEach(function (type) {
        _this4.windowContext.removeEventListener(type, _this4.listeners[type]);
      });
      this.timeouts.forEach(function (tid) {
        return clearTimeout(tid);
      });
    }
  }, {
    key: "setTimeout",
    value: function (_setTimeout) {
      function setTimeout(_x, _x2) {
        return _setTimeout.apply(this, arguments);
      }

      setTimeout.toString = function () {
        return _setTimeout.toString();
      };

      return setTimeout;
    }(function (func, time) {
      var _this5 = this;

      var id = setTimeout(function () {
        _this5.timeouts = _this5.timeouts.filter(function (tid) {
          return tid !== id;
        });
        func();
      }, time);
      this.timeouts.push(id);
      return id;
    })
  }, {
    key: "setPreventInnerClose",
    value: function setPreventInnerClose() {
      var _this6 = this;

      if (this.preventInnerCloseTimeout) {
        this.clearTimeout(this.preventInnerCloseTimeout);
      }

      this.preventInnerClose = true;
      this.preventInnerCloseTimeout = this.setTimeout(function () {
        _this6.preventInnerClose = false;
        _this6.preventInnerCloseTimeout = null;
      }, 100);
    } // Get info for the best suited image to display with the given srcType

  }, {
    key: "getBestImageForType",
    value: function getBestImageForType(srcType) {
      var imageSrc = this.props[srcType];
      var fitSizes = {};

      if (this.isImageLoaded(imageSrc)) {
        // Use full-size image if available
        fitSizes = this.getFitSizes(this.imageCache[imageSrc].width, this.imageCache[imageSrc].height);
      } else if (this.isImageLoaded(this.props["".concat(srcType, "Thumbnail")])) {
        // Fall back to using thumbnail if the image has not been loaded
        imageSrc = this.props["".concat(srcType, "Thumbnail")];
        fitSizes = this.getFitSizes(this.imageCache[imageSrc].width, this.imageCache[imageSrc].height, true);
      } else {
        return null;
      }

      return {
        src: imageSrc,
        height: this.imageCache[imageSrc].height,
        width: this.imageCache[imageSrc].width,
        targetHeight: fitSizes.height,
        targetWidth: fitSizes.width
      };
    } // Get sizing for when an image is larger than the window

  }, {
    key: "getFitSizes",
    value: function getFitSizes(width, height, stretch) {
      var boxSize = this.getLightboxRect();
      var maxHeight = boxSize.height - this.props.imagePadding * 2;
      var maxWidth = boxSize.width - this.props.imagePadding * 2;

      if (!stretch) {
        maxHeight = Math.min(maxHeight, height);
        maxWidth = Math.min(maxWidth, width);
      }

      var maxRatio = maxWidth / maxHeight;
      var srcRatio = width / height;

      if (maxRatio > srcRatio) {
        // height is the constraining dimension of the photo
        return {
          width: width * maxHeight / height,
          height: maxHeight
        };
      }

      return {
        width: maxWidth,
        height: height * maxWidth / width
      };
    }
  }, {
    key: "getMaxOffsets",
    value: function getMaxOffsets() {
      var zoomLevel = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.state.zoomLevel;
      var currentImageInfo = this.getBestImageForType('mainSrc');

      if (currentImageInfo === null) {
        return {
          maxX: 0,
          minX: 0,
          maxY: 0,
          minY: 0
        };
      }

      var boxSize = this.getLightboxRect();
      var zoomMultiplier = this.getZoomMultiplier(zoomLevel);
      var maxX = 0;

      if (zoomMultiplier * currentImageInfo.width - boxSize.width < 0) {
        // if there is still blank space in the X dimension, don't limit except to the opposite edge
        maxX = (boxSize.width - zoomMultiplier * currentImageInfo.width) / 2;
      } else {
        maxX = (zoomMultiplier * currentImageInfo.width - boxSize.width) / 2;
      }

      var maxY = 0;

      if (zoomMultiplier * currentImageInfo.height - boxSize.height < 0) {
        // if there is still blank space in the Y dimension, don't limit except to the opposite edge
        maxY = (boxSize.height - zoomMultiplier * currentImageInfo.height) / 2;
      } else {
        maxY = (zoomMultiplier * currentImageInfo.height - boxSize.height) / 2;
      }

      return {
        maxX: maxX,
        maxY: maxY,
        minX: -1 * maxX,
        minY: -1 * maxY
      };
    } // Get image src types

  }, {
    key: "getSrcTypes",
    value: function getSrcTypes() {
      return [{
        name: 'mainSrc',
        keyEnding: "i".concat(this.keyCounter)
      }, {
        name: 'mainSrcThumbnail',
        keyEnding: "t".concat(this.keyCounter)
      }, {
        name: 'nextSrc',
        keyEnding: "i".concat(this.keyCounter + 1)
      }, {
        name: 'nextSrcThumbnail',
        keyEnding: "t".concat(this.keyCounter + 1)
      }, {
        name: 'prevSrc',
        keyEnding: "i".concat(this.keyCounter - 1)
      }, {
        name: 'prevSrcThumbnail',
        keyEnding: "t".concat(this.keyCounter - 1)
      }];
    }
    /**
     * Get sizing when the image is scaled
     */

  }, {
    key: "getZoomMultiplier",
    value: function getZoomMultiplier() {
      var zoomLevel = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.state.zoomLevel;
      return Math.pow(ZOOM_RATIO, zoomLevel);
    }
    /**
     * Get the size of the lightbox in pixels
     */

  }, {
    key: "getLightboxRect",
    value: function getLightboxRect() {
      if (this.outerEl.current) {
        return this.outerEl.current.getBoundingClientRect();
      }

      return {
        width: getWindowWidth(),
        height: getWindowHeight(),
        top: 0,
        right: 0,
        bottom: 0,
        left: 0
      };
    }
  }, {
    key: "clearTimeout",
    value: function (_clearTimeout) {
      function clearTimeout(_x3) {
        return _clearTimeout.apply(this, arguments);
      }

      clearTimeout.toString = function () {
        return _clearTimeout.toString();
      };

      return clearTimeout;
    }(function (id) {
      this.timeouts = this.timeouts.filter(function (tid) {
        return tid !== id;
      });
      clearTimeout(id);
    }) // Change zoom level

  }, {
    key: "changeZoom",
    value: function changeZoom(zoomLevel, clientX, clientY) {
      // Ignore if zoom disabled
      if (!this.props.enableZoom) {
        return;
      } // Constrain zoom level to the set bounds


      var nextZoomLevel = Math.max(MIN_ZOOM_LEVEL, Math.min(MAX_ZOOM_LEVEL, zoomLevel)); // Ignore requests that don't change the zoom level

      if (nextZoomLevel === this.state.zoomLevel) {
        return;
      }

      if (nextZoomLevel === MIN_ZOOM_LEVEL) {
        // Snap back to center if zoomed all the way out
        this.setState({
          zoomLevel: nextZoomLevel,
          offsetX: 0,
          offsetY: 0
        });
        return;
      }

      var imageBaseSize = this.getBestImageForType('mainSrc');

      if (imageBaseSize === null) {
        return;
      }

      var currentZoomMultiplier = this.getZoomMultiplier();
      var nextZoomMultiplier = this.getZoomMultiplier(nextZoomLevel); // Default to the center of the image to zoom when no mouse position specified

      var boxRect = this.getLightboxRect();
      var pointerX = typeof clientX !== 'undefined' ? clientX - boxRect.left : boxRect.width / 2;
      var pointerY = typeof clientY !== 'undefined' ? clientY - boxRect.top : boxRect.height / 2;
      var currentImageOffsetX = (boxRect.width - imageBaseSize.width * currentZoomMultiplier) / 2;
      var currentImageOffsetY = (boxRect.height - imageBaseSize.height * currentZoomMultiplier) / 2;
      var currentImageRealOffsetX = currentImageOffsetX - this.state.offsetX;
      var currentImageRealOffsetY = currentImageOffsetY - this.state.offsetY;
      var currentPointerXRelativeToImage = (pointerX - currentImageRealOffsetX) / currentZoomMultiplier;
      var currentPointerYRelativeToImage = (pointerY - currentImageRealOffsetY) / currentZoomMultiplier;
      var nextImageRealOffsetX = pointerX - currentPointerXRelativeToImage * nextZoomMultiplier;
      var nextImageRealOffsetY = pointerY - currentPointerYRelativeToImage * nextZoomMultiplier;
      var nextImageOffsetX = (boxRect.width - imageBaseSize.width * nextZoomMultiplier) / 2;
      var nextImageOffsetY = (boxRect.height - imageBaseSize.height * nextZoomMultiplier) / 2;
      var nextOffsetX = nextImageOffsetX - nextImageRealOffsetX;
      var nextOffsetY = nextImageOffsetY - nextImageRealOffsetY; // When zooming out, limit the offset so things don't get left askew

      if (this.currentAction !== ACTION_PINCH) {
        var maxOffsets = this.getMaxOffsets();

        if (this.state.zoomLevel > nextZoomLevel) {
          nextOffsetX = Math.max(maxOffsets.minX, Math.min(maxOffsets.maxX, nextOffsetX));
          nextOffsetY = Math.max(maxOffsets.minY, Math.min(maxOffsets.maxY, nextOffsetY));
        }
      }

      this.setState({
        zoomLevel: nextZoomLevel,
        offsetX: nextOffsetX,
        offsetY: nextOffsetY
      });
    }
  }, {
    key: "closeIfClickInner",
    value: function closeIfClickInner(event) {
      if (!this.preventInnerClose && event.target.className.search(/\bril-inner\b/) > -1) {
        this.requestClose(event);
      }
    }
    /**
     * Handle user keyboard actions
     */

  }, {
    key: "handleKeyInput",
    value: function handleKeyInput(event) {
      event.stopPropagation(); // Ignore key input during animations

      if (this.isAnimating()) {
        return;
      } // Allow slightly faster navigation through the images when user presses keys repeatedly


      if (event.type === 'keyup') {
        this.lastKeyDownTime -= this.props.keyRepeatKeyupBonus;
        return;
      }

      var keyCode = event.which || event.keyCode; // Ignore key presses that happen too close to each other (when rapid fire key pressing or holding down the key)
      // But allow it if it's a lightbox closing action

      var currentTime = new Date();

      if (currentTime.getTime() - this.lastKeyDownTime < this.props.keyRepeatLimit && keyCode !== KEYS.ESC) {
        return;
      }

      this.lastKeyDownTime = currentTime.getTime();

      switch (keyCode) {
        // ESC key closes the lightbox
        case KEYS.ESC:
          event.preventDefault();
          this.requestClose(event);
          break;
        // Left arrow key moves to previous image

        case KEYS.LEFT_ARROW:
          if (!this.props.prevSrc) {
            return;
          }

          event.preventDefault();
          this.keyPressed = true;
          this.requestMovePrev(event);
          break;
        // Right arrow key moves to next image

        case KEYS.RIGHT_ARROW:
          if (!this.props.nextSrc) {
            return;
          }

          event.preventDefault();
          this.keyPressed = true;
          this.requestMoveNext(event);
          break;

        default:
      }
    }
    /**
     * Handle a mouse wheel event over the lightbox container
     */

  }, {
    key: "handleOuterMousewheel",
    value: function handleOuterMousewheel(event) {
      var _this7 = this;

      // Prevent scrolling of the background
      event.preventDefault();
      event.stopPropagation();
      var xThreshold = WHEEL_MOVE_X_THRESHOLD;
      var actionDelay = 0;
      var imageMoveDelay = 500;
      this.clearTimeout(this.resetScrollTimeout);
      this.resetScrollTimeout = this.setTimeout(function () {
        _this7.scrollX = 0;
        _this7.scrollY = 0;
      }, 300); // Prevent rapid-fire zoom behavior

      if (this.wheelActionTimeout !== null || this.isAnimating()) {
        return;
      }

      if (Math.abs(event.deltaY) < Math.abs(event.deltaX)) {
        // handle horizontal scrolls with image moves
        this.scrollY = 0;
        this.scrollX += event.deltaX;
        var bigLeapX = xThreshold / 2; // If the scroll amount has accumulated sufficiently, or a large leap was taken

        if (this.scrollX >= xThreshold || event.deltaX >= bigLeapX) {
          // Scroll right moves to next
          this.requestMoveNext(event);
          actionDelay = imageMoveDelay;
          this.scrollX = 0;
        } else if (this.scrollX <= -1 * xThreshold || event.deltaX <= -1 * bigLeapX) {
          // Scroll left moves to previous
          this.requestMovePrev(event);
          actionDelay = imageMoveDelay;
          this.scrollX = 0;
        }
      } // Allow successive actions after the set delay


      if (actionDelay !== 0) {
        this.wheelActionTimeout = this.setTimeout(function () {
          _this7.wheelActionTimeout = null;
        }, actionDelay);
      }
    }
  }, {
    key: "handleImageMouseWheel",
    value: function handleImageMouseWheel(event) {
      event.preventDefault();
      var yThreshold = WHEEL_MOVE_Y_THRESHOLD;

      if (Math.abs(event.deltaY) >= Math.abs(event.deltaX)) {
        event.stopPropagation(); // If the vertical scroll amount was large enough, perform a zoom

        if (Math.abs(event.deltaY) < yThreshold) {
          return;
        }

        this.scrollX = 0;
        this.scrollY += event.deltaY;
        this.changeZoom(this.state.zoomLevel - event.deltaY, event.clientX, event.clientY);
      }
    }
    /**
     * Handle a double click on the current image
     */

  }, {
    key: "handleImageDoubleClick",
    value: function handleImageDoubleClick(event) {
      if (this.state.zoomLevel > MIN_ZOOM_LEVEL) {
        // A double click when zoomed in zooms all the way out
        this.changeZoom(MIN_ZOOM_LEVEL, event.clientX, event.clientY);
      } else {
        // A double click when zoomed all the way out zooms in
        this.changeZoom(this.state.zoomLevel + ZOOM_BUTTON_INCREMENT_SIZE, event.clientX, event.clientY);
      }
    }
  }, {
    key: "shouldHandleEvent",
    value: function shouldHandleEvent(source) {
      if (this.eventsSource === source) {
        return true;
      }

      if (this.eventsSource === SOURCE_ANY) {
        this.eventsSource = source;
        return true;
      }

      switch (source) {
        case SOURCE_MOUSE:
          return false;

        case SOURCE_TOUCH:
          this.eventsSource = SOURCE_TOUCH;
          this.filterPointersBySource();
          return true;

        case SOURCE_POINTER:
          if (this.eventsSource === SOURCE_MOUSE) {
            this.eventsSource = SOURCE_POINTER;
            this.filterPointersBySource();
            return true;
          }

          return false;

        default:
          return false;
      }
    }
  }, {
    key: "addPointer",
    value: function addPointer(pointer) {
      this.pointerList.push(pointer);
    }
  }, {
    key: "removePointer",
    value: function removePointer(pointer) {
      this.pointerList = this.pointerList.filter(function (_ref2) {
        var id = _ref2.id;
        return id !== pointer.id;
      });
    }
  }, {
    key: "filterPointersBySource",
    value: function filterPointersBySource() {
      var _this8 = this;

      this.pointerList = this.pointerList.filter(function (_ref3) {
        var source = _ref3.source;
        return source === _this8.eventsSource;
      });
    }
  }, {
    key: "handleMouseDown",
    value: function handleMouseDown(event) {
      if (this.shouldHandleEvent(SOURCE_MOUSE) && ReactImageLightbox.isTargetMatchImage(event.target)) {
        this.addPointer(ReactImageLightbox.parseMouseEvent(event));
        this.multiPointerStart(event);
      }
    }
  }, {
    key: "handleMouseMove",
    value: function handleMouseMove(event) {
      if (this.shouldHandleEvent(SOURCE_MOUSE)) {
        this.multiPointerMove(event, [ReactImageLightbox.parseMouseEvent(event)]);
      }
    }
  }, {
    key: "handleMouseUp",
    value: function handleMouseUp(event) {
      if (this.shouldHandleEvent(SOURCE_MOUSE)) {
        this.removePointer(ReactImageLightbox.parseMouseEvent(event));
        this.multiPointerEnd(event);
      }
    }
  }, {
    key: "handlePointerEvent",
    value: function handlePointerEvent(event) {
      if (this.shouldHandleEvent(SOURCE_POINTER)) {
        switch (event.type) {
          case 'pointerdown':
            if (ReactImageLightbox.isTargetMatchImage(event.target)) {
              this.addPointer(ReactImageLightbox.parsePointerEvent(event));
              this.multiPointerStart(event);
            }

            break;

          case 'pointermove':
            this.multiPointerMove(event, [ReactImageLightbox.parsePointerEvent(event)]);
            break;

          case 'pointerup':
          case 'pointercancel':
            this.removePointer(ReactImageLightbox.parsePointerEvent(event));
            this.multiPointerEnd(event);
            break;

          default:
            break;
        }
      }
    }
  }, {
    key: "handleTouchStart",
    value: function handleTouchStart(event) {
      var _this9 = this;

      if (this.shouldHandleEvent(SOURCE_TOUCH) && ReactImageLightbox.isTargetMatchImage(event.target)) {
        [].forEach.call(event.changedTouches, function (eventTouch) {
          return _this9.addPointer(ReactImageLightbox.parseTouchPointer(eventTouch));
        });
        this.multiPointerStart(event);
      }
    }
  }, {
    key: "handleTouchMove",
    value: function handleTouchMove(event) {
      if (this.shouldHandleEvent(SOURCE_TOUCH)) {
        this.multiPointerMove(event, [].map.call(event.changedTouches, function (eventTouch) {
          return ReactImageLightbox.parseTouchPointer(eventTouch);
        }));
      }
    }
  }, {
    key: "handleTouchEnd",
    value: function handleTouchEnd(event) {
      var _this10 = this;

      if (this.shouldHandleEvent(SOURCE_TOUCH)) {
        [].map.call(event.changedTouches, function (touch) {
          return _this10.removePointer(ReactImageLightbox.parseTouchPointer(touch));
        });
        this.multiPointerEnd(event);
      }
    }
  }, {
    key: "decideMoveOrSwipe",
    value: function decideMoveOrSwipe(pointer) {
      if (this.state.zoomLevel <= MIN_ZOOM_LEVEL) {
        this.handleSwipeStart(pointer);
      } else {
        this.handleMoveStart(pointer);
      }
    }
  }, {
    key: "multiPointerStart",
    value: function multiPointerStart(event) {
      this.handleEnd(null);

      switch (this.pointerList.length) {
        case 1:
          {
            event.preventDefault();
            this.decideMoveOrSwipe(this.pointerList[0]);
            break;
          }

        case 2:
          {
            event.preventDefault();
            this.handlePinchStart(this.pointerList);
            break;
          }

        default:
          break;
      }
    }
  }, {
    key: "multiPointerMove",
    value: function multiPointerMove(event, pointerList) {
      switch (this.currentAction) {
        case ACTION_MOVE:
          {
            event.preventDefault();
            this.handleMove(pointerList[0]);
            break;
          }

        case ACTION_SWIPE:
          {
            event.preventDefault();
            this.handleSwipe(pointerList[0]);
            break;
          }

        case ACTION_PINCH:
          {
            event.preventDefault();
            this.handlePinch(pointerList);
            break;
          }

        default:
          break;
      }
    }
  }, {
    key: "multiPointerEnd",
    value: function multiPointerEnd(event) {
      if (this.currentAction !== ACTION_NONE) {
        this.setPreventInnerClose();
        this.handleEnd(event);
      }

      switch (this.pointerList.length) {
        case 0:
          {
            this.eventsSource = SOURCE_ANY;
            break;
          }

        case 1:
          {
            event.preventDefault();
            this.decideMoveOrSwipe(this.pointerList[0]);
            break;
          }

        case 2:
          {
            event.preventDefault();
            this.handlePinchStart(this.pointerList);
            break;
          }

        default:
          break;
      }
    }
  }, {
    key: "handleEnd",
    value: function handleEnd(event) {
      switch (this.currentAction) {
        case ACTION_MOVE:
          this.handleMoveEnd(event);
          break;

        case ACTION_SWIPE:
          this.handleSwipeEnd(event);
          break;

        case ACTION_PINCH:
          this.handlePinchEnd(event);
          break;

        default:
          break;
      }
    } // Handle move start over the lightbox container
    // This happens:
    // - On a mouseDown event
    // - On a touchstart event

  }, {
    key: "handleMoveStart",
    value: function handleMoveStart(_ref4) {
      var clientX = _ref4.x,
          clientY = _ref4.y;

      if (!this.props.enableZoom) {
        return;
      }

      this.currentAction = ACTION_MOVE;
      this.moveStartX = clientX;
      this.moveStartY = clientY;
      this.moveStartOffsetX = this.state.offsetX;
      this.moveStartOffsetY = this.state.offsetY;
    } // Handle dragging over the lightbox container
    // This happens:
    // - After a mouseDown and before a mouseUp event
    // - After a touchstart and before a touchend event

  }, {
    key: "handleMove",
    value: function handleMove(_ref5) {
      var clientX = _ref5.x,
          clientY = _ref5.y;
      var newOffsetX = this.moveStartX - clientX + this.moveStartOffsetX;
      var newOffsetY = this.moveStartY - clientY + this.moveStartOffsetY;

      if (this.state.offsetX !== newOffsetX || this.state.offsetY !== newOffsetY) {
        this.setState({
          offsetX: newOffsetX,
          offsetY: newOffsetY
        });
      }
    }
  }, {
    key: "handleMoveEnd",
    value: function handleMoveEnd() {
      var _this11 = this;

      this.currentAction = ACTION_NONE;
      this.moveStartX = 0;
      this.moveStartY = 0;
      this.moveStartOffsetX = 0;
      this.moveStartOffsetY = 0; // Snap image back into frame if outside max offset range

      var maxOffsets = this.getMaxOffsets();
      var nextOffsetX = Math.max(maxOffsets.minX, Math.min(maxOffsets.maxX, this.state.offsetX));
      var nextOffsetY = Math.max(maxOffsets.minY, Math.min(maxOffsets.maxY, this.state.offsetY));

      if (nextOffsetX !== this.state.offsetX || nextOffsetY !== this.state.offsetY) {
        this.setState({
          offsetX: nextOffsetX,
          offsetY: nextOffsetY,
          shouldAnimate: true
        });
        this.setTimeout(function () {
          _this11.setState({
            shouldAnimate: false
          });
        }, this.props.animationDuration);
      }
    }
  }, {
    key: "handleSwipeStart",
    value: function handleSwipeStart(_ref6) {
      var clientX = _ref6.x,
          clientY = _ref6.y;
      this.currentAction = ACTION_SWIPE;
      this.swipeStartX = clientX;
      this.swipeStartY = clientY;
      this.swipeEndX = clientX;
      this.swipeEndY = clientY;
    }
  }, {
    key: "handleSwipe",
    value: function handleSwipe(_ref7) {
      var clientX = _ref7.x,
          clientY = _ref7.y;
      this.swipeEndX = clientX;
      this.swipeEndY = clientY;
    }
  }, {
    key: "handleSwipeEnd",
    value: function handleSwipeEnd(event) {
      var xDiff = this.swipeEndX - this.swipeStartX;
      var xDiffAbs = Math.abs(xDiff);
      var yDiffAbs = Math.abs(this.swipeEndY - this.swipeStartY);
      this.currentAction = ACTION_NONE;
      this.swipeStartX = 0;
      this.swipeStartY = 0;
      this.swipeEndX = 0;
      this.swipeEndY = 0;

      if (!event || this.isAnimating() || xDiffAbs < yDiffAbs * 1.5) {
        return;
      }

      if (xDiffAbs < MIN_SWIPE_DISTANCE) {
        var boxRect = this.getLightboxRect();

        if (xDiffAbs < boxRect.width / 4) {
          return;
        }
      }

      if (xDiff > 0 && this.props.prevSrc) {
        event.preventDefault();
        this.requestMovePrev();
      } else if (xDiff < 0 && this.props.nextSrc) {
        event.preventDefault();
        this.requestMoveNext();
      }
    }
  }, {
    key: "calculatePinchDistance",
    value: function calculatePinchDistance() {
      var _ref8 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.pinchTouchList,
          _ref9 = _slicedToArray(_ref8, 2),
          a = _ref9[0],
          b = _ref9[1];

      return Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2));
    }
  }, {
    key: "calculatePinchCenter",
    value: function calculatePinchCenter() {
      var _ref10 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.pinchTouchList,
          _ref11 = _slicedToArray(_ref10, 2),
          a = _ref11[0],
          b = _ref11[1];

      return {
        x: a.x - (a.x - b.x) / 2,
        y: a.y - (a.y - b.y) / 2
      };
    }
  }, {
    key: "handlePinchStart",
    value: function handlePinchStart(pointerList) {
      if (!this.props.enableZoom) {
        return;
      }

      this.currentAction = ACTION_PINCH;
      this.pinchTouchList = pointerList.map(function (_ref12) {
        var id = _ref12.id,
            x = _ref12.x,
            y = _ref12.y;
        return {
          id: id,
          x: x,
          y: y
        };
      });
      this.pinchDistance = this.calculatePinchDistance();
    }
  }, {
    key: "handlePinch",
    value: function handlePinch(pointerList) {
      this.pinchTouchList = this.pinchTouchList.map(function (oldPointer) {
        for (var i = 0; i < pointerList.length; i += 1) {
          if (pointerList[i].id === oldPointer.id) {
            return pointerList[i];
          }
        }

        return oldPointer;
      });
      var newDistance = this.calculatePinchDistance();
      var zoomLevel = this.state.zoomLevel + newDistance - this.pinchDistance;
      this.pinchDistance = newDistance;

      var _this$calculatePinchC = this.calculatePinchCenter(this.pinchTouchList),
          clientX = _this$calculatePinchC.x,
          clientY = _this$calculatePinchC.y;

      this.changeZoom(zoomLevel, clientX, clientY);
    }
  }, {
    key: "handlePinchEnd",
    value: function handlePinchEnd() {
      this.currentAction = ACTION_NONE;
      this.pinchTouchList = null;
      this.pinchDistance = 0;
    } // Handle the window resize event

  }, {
    key: "handleWindowResize",
    value: function handleWindowResize() {
      this.clearTimeout(this.resizeTimeout);
      this.resizeTimeout = this.setTimeout(this.forceUpdate.bind(this), 100);
    }
  }, {
    key: "handleZoomInButtonClick",
    value: function handleZoomInButtonClick() {
      var nextZoomLevel = this.state.zoomLevel + ZOOM_BUTTON_INCREMENT_SIZE;
      this.changeZoom(nextZoomLevel);

      if (nextZoomLevel === MAX_ZOOM_LEVEL) {
        this.zoomOutBtn.current.focus();
      }
    }
  }, {
    key: "handleZoomOutButtonClick",
    value: function handleZoomOutButtonClick() {
      var nextZoomLevel = this.state.zoomLevel - ZOOM_BUTTON_INCREMENT_SIZE;
      this.changeZoom(nextZoomLevel);

      if (nextZoomLevel === MIN_ZOOM_LEVEL) {
        this.zoomInBtn.current.focus();
      }
    }
  }, {
    key: "handleCaptionMousewheel",
    value: function handleCaptionMousewheel(event) {
      event.stopPropagation();

      if (!this.caption.current) {
        return;
      }

      var _this$caption$current = this.caption.current.getBoundingClientRect(),
          height = _this$caption$current.height;

      var _this$caption$current2 = this.caption.current,
          scrollHeight = _this$caption$current2.scrollHeight,
          scrollTop = _this$caption$current2.scrollTop;

      if (event.deltaY > 0 && height + scrollTop >= scrollHeight || event.deltaY < 0 && scrollTop <= 0) {
        event.preventDefault();
      }
    } // Detach key and mouse input events

  }, {
    key: "isAnimating",
    value: function isAnimating() {
      return this.state.shouldAnimate || this.state.isClosing;
    } // Check if image is loaded

  }, {
    key: "isImageLoaded",
    value: function isImageLoaded(imageSrc) {
      return imageSrc && imageSrc in this.imageCache && this.imageCache[imageSrc].loaded;
    } // Load image from src and call callback with image width and height on load

  }, {
    key: "loadImage",
    value: function loadImage(srcType, imageSrc, done) {
      var _this12 = this;

      // Return the image info if it is already cached
      if (this.isImageLoaded(imageSrc)) {
        this.setTimeout(function () {
          done();
        }, 1);
        return;
      }

      var inMemoryImage = new global$1.Image();

      if (this.props.imageCrossOrigin) {
        inMemoryImage.crossOrigin = this.props.imageCrossOrigin;
      }

      inMemoryImage.onerror = function (errorEvent) {
        _this12.props.onImageLoadError(imageSrc, srcType, errorEvent); // failed to load so set the state loadErrorStatus


        _this12.setState(function (prevState) {
          return {
            loadErrorStatus: _objectSpread({}, prevState.loadErrorStatus, _defineProperty({}, srcType, true))
          };
        });

        done(errorEvent);
      };

      inMemoryImage.onload = function () {
        _this12.props.onImageLoad(imageSrc, srcType, inMemoryImage);

        _this12.imageCache[imageSrc] = {
          loaded: true,
          width: inMemoryImage.width,
          height: inMemoryImage.height
        };
        done();
      };

      inMemoryImage.src = imageSrc;
    } // Load all images and their thumbnails

  }, {
    key: "loadAllImages",
    value: function loadAllImages() {
      var _this13 = this;

      var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.props;

      var generateLoadDoneCallback = function generateLoadDoneCallback(srcType, imageSrc) {
        return function (err) {
          // Give up showing image on error
          if (err) {
            return;
          } // Don't rerender if the src is not the same as when the load started
          // or if the component has unmounted


          if (_this13.props[srcType] !== imageSrc || _this13.didUnmount) {
            return;
          } // Force rerender with the new image


          _this13.forceUpdate();
        };
      }; // Load the images


      this.getSrcTypes().forEach(function (srcType) {
        var type = srcType.name; // there is no error when we try to load it initially

        if (props[type] && _this13.state.loadErrorStatus[type]) {
          _this13.setState(function (prevState) {
            return {
              loadErrorStatus: _objectSpread({}, prevState.loadErrorStatus, _defineProperty({}, type, false))
            };
          });
        } // Load unloaded images


        if (props[type] && !_this13.isImageLoaded(props[type])) {
          _this13.loadImage(type, props[type], generateLoadDoneCallback(type, props[type]));
        }
      });
    } // Request that the lightbox be closed

  }, {
    key: "requestClose",
    value: function requestClose(event) {
      var _this14 = this;

      // Call the parent close request
      var closeLightbox = function closeLightbox() {
        return _this14.props.onCloseRequest(event);
      };

      if (this.props.animationDisabled || event.type === 'keydown' && !this.props.animationOnKeyInput) {
        // No animation
        closeLightbox();
        return;
      } // With animation
      // Start closing animation


      this.setState({
        isClosing: true
      }); // Perform the actual closing at the end of the animation

      this.setTimeout(closeLightbox, this.props.animationDuration);
    }
  }, {
    key: "requestMove",
    value: function requestMove(direction, event) {
      var _this15 = this;

      // Reset the zoom level on image move
      var nextState = {
        zoomLevel: MIN_ZOOM_LEVEL,
        offsetX: 0,
        offsetY: 0
      }; // Enable animated states

      if (!this.props.animationDisabled && (!this.keyPressed || this.props.animationOnKeyInput)) {
        nextState.shouldAnimate = true;
        this.setTimeout(function () {
          return _this15.setState({
            shouldAnimate: false
          });
        }, this.props.animationDuration);
      }

      this.keyPressed = false;
      this.moveRequested = true;

      if (direction === 'prev') {
        this.keyCounter -= 1;
        this.setState(nextState);
        this.props.onMovePrevRequest(event);
      } else {
        this.keyCounter += 1;
        this.setState(nextState);
        this.props.onMoveNextRequest(event);
      }
    } // Request to transition to the next image

  }, {
    key: "requestMoveNext",
    value: function requestMoveNext(event) {
      this.requestMove('next', event);
    } // Request to transition to the previous image

  }, {
    key: "requestMovePrev",
    value: function requestMovePrev(event) {
      this.requestMove('prev', event);
    }
  }, {
    key: "render",
    value: function render$$1() {
      var _this16 = this;

      var _this$props = this.props,
          animationDisabled = _this$props.animationDisabled,
          animationDuration = _this$props.animationDuration,
          clickOutsideToClose = _this$props.clickOutsideToClose,
          discourageDownloads = _this$props.discourageDownloads,
          enableZoom = _this$props.enableZoom,
          imageTitle = _this$props.imageTitle,
          nextSrc = _this$props.nextSrc,
          prevSrc = _this$props.prevSrc,
          toolbarButtons = _this$props.toolbarButtons,
          reactModalStyle = _this$props.reactModalStyle,
          _onAfterOpen = _this$props.onAfterOpen,
          imageCrossOrigin = _this$props.imageCrossOrigin,
          reactModalProps = _this$props.reactModalProps;
      var _this$state = this.state,
          zoomLevel = _this$state.zoomLevel,
          offsetX = _this$state.offsetX,
          offsetY = _this$state.offsetY,
          isClosing = _this$state.isClosing,
          loadErrorStatus = _this$state.loadErrorStatus;
      var boxSize = this.getLightboxRect();
      var transitionStyle = {}; // Transition settings for sliding animations

      if (!animationDisabled && this.isAnimating()) {
        transitionStyle = _objectSpread({}, transitionStyle, {
          transition: "transform ".concat(animationDuration, "ms")
        });
      } // Key endings to differentiate between images with the same src


      var keyEndings = {};
      this.getSrcTypes().forEach(function (_ref13) {
        var name = _ref13.name,
            keyEnding = _ref13.keyEnding;
        keyEndings[name] = keyEnding;
      }); // Images to be displayed

      var images = [];

      var addImage = function addImage(srcType, imageClass, transforms) {
        // Ignore types that have no source defined for their full size image
        if (!_this16.props[srcType]) {
          return;
        }

        var bestImageInfo = _this16.getBestImageForType(srcType);

        var imageStyle = _objectSpread({}, transitionStyle, ReactImageLightbox.getTransform(_objectSpread({}, transforms, bestImageInfo)));

        if (zoomLevel > MIN_ZOOM_LEVEL) {
          imageStyle.cursor = 'move';
        } // support IE 9 and 11


        var hasTrueValue = function hasTrueValue(object) {
          return Object.keys(object).some(function (key) {
            return object[key];
          });
        }; // when error on one of the loads then push custom error stuff


        if (bestImageInfo === null && hasTrueValue(loadErrorStatus)) {
          images.push(React__default.createElement("div", {
            className: "".concat(imageClass, " ril__image ril-errored"),
            style: imageStyle,
            key: _this16.props[srcType] + keyEndings[srcType]
          }, React__default.createElement("div", {
            className: "ril__errorContainer"
          }, _this16.props.imageLoadErrorMessage)));
          return;
        }

        if (bestImageInfo === null) {
          var loadingIcon = React__default.createElement("div", {
            className: "ril-loading-circle ril__loadingCircle ril__loadingContainer__icon"
          }, _toConsumableArray(new Array(12)).map(function (_, index) {
            return React__default.createElement("div", {
              // eslint-disable-next-line react/no-array-index-key
              key: index,
              className: "ril-loading-circle-point ril__loadingCirclePoint"
            });
          })); // Fall back to loading icon if the thumbnail has not been loaded

          images.push(React__default.createElement("div", {
            className: "".concat(imageClass, " ril__image ril-not-loaded"),
            style: imageStyle,
            key: _this16.props[srcType] + keyEndings[srcType]
          }, React__default.createElement("div", {
            className: "ril__loadingContainer"
          }, loadingIcon)));
          return;
        }

        var imageSrc = bestImageInfo.src;

        if (discourageDownloads) {
          imageStyle.backgroundImage = "url('".concat(imageSrc, "')");
          images.push(React__default.createElement("div", {
            className: "".concat(imageClass, " ril__image ril__imageDiscourager"),
            onDoubleClick: _this16.handleImageDoubleClick,
            onWheel: _this16.handleImageMouseWheel,
            style: imageStyle,
            key: imageSrc + keyEndings[srcType]
          }, React__default.createElement("div", {
            className: "ril-download-blocker ril__downloadBlocker"
          })));
        } else {
          images.push(React__default.createElement("img", _extends$1({}, imageCrossOrigin ? {
            crossOrigin: imageCrossOrigin
          } : {}, {
            className: "".concat(imageClass, " ril__image"),
            onDoubleClick: _this16.handleImageDoubleClick,
            onWheel: _this16.handleImageMouseWheel,
            onDragStart: function onDragStart(e) {
              return e.preventDefault();
            },
            style: imageStyle,
            src: imageSrc,
            key: imageSrc + keyEndings[srcType],
            alt: typeof imageTitle === 'string' ? imageTitle : translate('Image'),
            draggable: false
          })));
        }
      };

      var zoomMultiplier = this.getZoomMultiplier(); // Next Image (displayed on the right)

      addImage('nextSrc', 'ril-image-next ril__imageNext', {
        x: boxSize.width
      }); // Main Image

      addImage('mainSrc', 'ril-image-current', {
        x: -1 * offsetX,
        y: -1 * offsetY,
        zoom: zoomMultiplier
      }); // Previous Image (displayed on the left)

      addImage('prevSrc', 'ril-image-prev ril__imagePrev', {
        x: -1 * boxSize.width
      });
      var modalStyle = {
        overlay: _objectSpread({
          zIndex: 1000,
          backgroundColor: 'transparent'
        }, reactModalStyle.overlay),
        content: _objectSpread({
          backgroundColor: 'transparent',
          overflow: 'hidden',
          // Needed, otherwise keyboard shortcuts scroll the page
          border: 'none',
          borderRadius: 0,
          padding: 0,
          top: 0,
          left: 0,
          right: 0,
          bottom: 0
        }, reactModalStyle.content)
      };
      return React__default.createElement(Modal$1, _extends$1({
        isOpen: true,
        onRequestClose: clickOutsideToClose ? this.requestClose : undefined,
        onAfterOpen: function onAfterOpen() {
          // Focus on the div with key handlers
          if (_this16.outerEl.current) {
            _this16.outerEl.current.focus();
          }

          _onAfterOpen();
        },
        style: modalStyle,
        contentLabel: translate('Lightbox'),
        appElement: typeof global$1.window !== 'undefined' ? global$1.window.document.body : undefined
      }, reactModalProps), React__default.createElement("div", {
        // eslint-disable-line jsx-a11y/no-static-element-interactions
        // Floating modal with closing animations
        className: "ril-outer ril__outer ril__outerAnimating ".concat(this.props.wrapperClassName, " ").concat(isClosing ? 'ril-closing ril__outerClosing' : ''),
        style: {
          transition: "opacity ".concat(animationDuration, "ms"),
          animationDuration: "".concat(animationDuration, "ms"),
          animationDirection: isClosing ? 'normal' : 'reverse'
        },
        ref: this.outerEl,
        onWheel: this.handleOuterMousewheel,
        onMouseMove: this.handleMouseMove,
        onMouseDown: this.handleMouseDown,
        onTouchStart: this.handleTouchStart,
        onTouchMove: this.handleTouchMove,
        tabIndex: "-1" // Enables key handlers on div
        ,
        onKeyDown: this.handleKeyInput,
        onKeyUp: this.handleKeyInput
      }, React__default.createElement("div", {
        // eslint-disable-line jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events
        // Image holder
        className: "ril-inner ril__inner",
        onClick: clickOutsideToClose ? this.closeIfClickInner : undefined
      }, images), prevSrc && React__default.createElement("button", {
        // Move to previous image button
        type: "button",
        className: "ril-prev-button ril__navButtons ril__navButtonPrev",
        key: "prev",
        "aria-label": this.props.prevLabel,
        onClick: !this.isAnimating() ? this.requestMovePrev : undefined // Ignore clicks during animation

      }), nextSrc && React__default.createElement("button", {
        // Move to next image button
        type: "button",
        className: "ril-next-button ril__navButtons ril__navButtonNext",
        key: "next",
        "aria-label": this.props.nextLabel,
        onClick: !this.isAnimating() ? this.requestMoveNext : undefined // Ignore clicks during animation

      }), React__default.createElement("div", {
        // Lightbox toolbar
        className: "ril-toolbar ril__toolbar"
      }, React__default.createElement("ul", {
        className: "ril-toolbar-left ril__toolbarSide ril__toolbarLeftSide"
      }, React__default.createElement("li", {
        className: "ril-toolbar__item ril__toolbarItem"
      }, React__default.createElement("span", {
        className: "ril-toolbar__item__child ril__toolbarItemChild"
      }, imageTitle))), React__default.createElement("ul", {
        className: "ril-toolbar-right ril__toolbarSide ril__toolbarRightSide"
      }, toolbarButtons && toolbarButtons.map(function (button, i) {
        return React__default.createElement("li", {
          key: "button_".concat(i + 1),
          className: "ril-toolbar__item ril__toolbarItem"
        }, button);
      }), enableZoom && React__default.createElement("li", {
        className: "ril-toolbar__item ril__toolbarItem"
      }, React__default.createElement("button", {
        // Lightbox zoom in button
        type: "button",
        key: "zoom-in",
        "aria-label": this.props.zoomInLabel,
        className: ['ril-zoom-in', 'ril__toolbarItemChild', 'ril__builtinButton', 'ril__zoomInButton'].concat(_toConsumableArray(zoomLevel === MAX_ZOOM_LEVEL ? ['ril__builtinButtonDisabled'] : [])).join(' '),
        ref: this.zoomInBtn,
        disabled: this.isAnimating() || zoomLevel === MAX_ZOOM_LEVEL,
        onClick: !this.isAnimating() && zoomLevel !== MAX_ZOOM_LEVEL ? this.handleZoomInButtonClick : undefined
      })), enableZoom && React__default.createElement("li", {
        className: "ril-toolbar__item ril__toolbarItem"
      }, React__default.createElement("button", {
        // Lightbox zoom out button
        type: "button",
        key: "zoom-out",
        "aria-label": this.props.zoomOutLabel,
        className: ['ril-zoom-out', 'ril__toolbarItemChild', 'ril__builtinButton', 'ril__zoomOutButton'].concat(_toConsumableArray(zoomLevel === MIN_ZOOM_LEVEL ? ['ril__builtinButtonDisabled'] : [])).join(' '),
        ref: this.zoomOutBtn,
        disabled: this.isAnimating() || zoomLevel === MIN_ZOOM_LEVEL,
        onClick: !this.isAnimating() && zoomLevel !== MIN_ZOOM_LEVEL ? this.handleZoomOutButtonClick : undefined
      })), React__default.createElement("li", {
        className: "ril-toolbar__item ril__toolbarItem"
      }, React__default.createElement("button", {
        // Lightbox close button
        type: "button",
        key: "close",
        "aria-label": this.props.closeLabel,
        className: "ril-close ril-toolbar__item__child ril__toolbarItemChild ril__builtinButton ril__closeButton",
        onClick: !this.isAnimating() ? this.requestClose : undefined // Ignore clicks during animation

      })))), this.props.imageCaption && // eslint-disable-next-line jsx-a11y/no-static-element-interactions
      React__default.createElement("div", {
        // Image caption
        onWheel: this.handleCaptionMousewheel,
        onMouseDown: function onMouseDown(event) {
          return event.stopPropagation();
        },
        className: "ril-caption ril__caption",
        ref: this.caption
      }, React__default.createElement("div", {
        className: "ril-caption-content ril__captionContent"
      }, this.props.imageCaption))));
    }
  }]);

  return ReactImageLightbox;
}(Component);

ReactImageLightbox.propTypes = {
  //-----------------------------
  // Image sources
  //-----------------------------
  // Main display image url
  mainSrc: propTypes.string.isRequired,
  // eslint-disable-line react/no-unused-prop-types
  // Previous display image url (displayed to the left)
  // If left undefined, movePrev actions will not be performed, and the button not displayed
  prevSrc: propTypes.string,
  // Next display image url (displayed to the right)
  // If left undefined, moveNext actions will not be performed, and the button not displayed
  nextSrc: propTypes.string,
  //-----------------------------
  // Image thumbnail sources
  //-----------------------------
  // Thumbnail image url corresponding to props.mainSrc
  mainSrcThumbnail: propTypes.string,
  // eslint-disable-line react/no-unused-prop-types
  // Thumbnail image url corresponding to props.prevSrc
  prevSrcThumbnail: propTypes.string,
  // eslint-disable-line react/no-unused-prop-types
  // Thumbnail image url corresponding to props.nextSrc
  nextSrcThumbnail: propTypes.string,
  // eslint-disable-line react/no-unused-prop-types
  //-----------------------------
  // Event Handlers
  //-----------------------------
  // Close window event
  // Should change the parent state such that the lightbox is not rendered
  onCloseRequest: propTypes.func.isRequired,
  // Move to previous image event
  // Should change the parent state such that props.prevSrc becomes props.mainSrc,
  //  props.mainSrc becomes props.nextSrc, etc.
  onMovePrevRequest: propTypes.func,
  // Move to next image event
  // Should change the parent state such that props.nextSrc becomes props.mainSrc,
  //  props.mainSrc becomes props.prevSrc, etc.
  onMoveNextRequest: propTypes.func,
  // Called when an image fails to load
  // (imageSrc: string, srcType: string, errorEvent: object): void
  onImageLoadError: propTypes.func,
  // Called when image successfully loads
  onImageLoad: propTypes.func,
  // Open window event
  onAfterOpen: propTypes.func,
  //-----------------------------
  // Download discouragement settings
  //-----------------------------
  // Enable download discouragement (prevents [right-click -> Save Image As...])
  discourageDownloads: propTypes.bool,
  //-----------------------------
  // Animation settings
  //-----------------------------
  // Disable all animation
  animationDisabled: propTypes.bool,
  // Disable animation on actions performed with keyboard shortcuts
  animationOnKeyInput: propTypes.bool,
  // Animation duration (ms)
  animationDuration: propTypes.number,
  //-----------------------------
  // Keyboard shortcut settings
  //-----------------------------
  // Required interval of time (ms) between key actions
  // (prevents excessively fast navigation of images)
  keyRepeatLimit: propTypes.number,
  // Amount of time (ms) restored after each keyup
  // (makes rapid key presses slightly faster than holding down the key to navigate images)
  keyRepeatKeyupBonus: propTypes.number,
  //-----------------------------
  // Image info
  //-----------------------------
  // Image title
  imageTitle: propTypes.node,
  // Image caption
  imageCaption: propTypes.node,
  // Optional crossOrigin attribute
  imageCrossOrigin: propTypes.string,
  //-----------------------------
  // Lightbox style
  //-----------------------------
  // Set z-index style, etc., for the parent react-modal (format: https://github.com/reactjs/react-modal#styles )
  reactModalStyle: propTypes.shape({}),
  // Padding (px) between the edge of the window and the lightbox
  imagePadding: propTypes.number,
  wrapperClassName: propTypes.string,
  //-----------------------------
  // Other
  //-----------------------------
  // Array of custom toolbar buttons
  toolbarButtons: propTypes.arrayOf(propTypes.node),
  // When true, clicks outside of the image close the lightbox
  clickOutsideToClose: propTypes.bool,
  // Set to false to disable zoom functionality and hide zoom buttons
  enableZoom: propTypes.bool,
  // Override props set on react-modal (https://github.com/reactjs/react-modal)
  reactModalProps: propTypes.shape({}),
  // Aria-labels
  nextLabel: propTypes.string,
  prevLabel: propTypes.string,
  zoomInLabel: propTypes.string,
  zoomOutLabel: propTypes.string,
  closeLabel: propTypes.string,
  imageLoadErrorMessage: propTypes.node
};
ReactImageLightbox.defaultProps = {
  imageTitle: null,
  imageCaption: null,
  toolbarButtons: null,
  reactModalProps: {},
  animationDisabled: false,
  animationDuration: 300,
  animationOnKeyInput: false,
  clickOutsideToClose: true,
  closeLabel: 'Close lightbox',
  discourageDownloads: false,
  enableZoom: true,
  imagePadding: 10,
  imageCrossOrigin: null,
  keyRepeatKeyupBonus: 40,
  keyRepeatLimit: 180,
  mainSrcThumbnail: null,
  nextLabel: 'Next image',
  nextSrc: null,
  nextSrcThumbnail: null,
  onAfterOpen: function onAfterOpen() {},
  onImageLoadError: function onImageLoadError() {},
  onImageLoad: function onImageLoad() {},
  onMoveNextRequest: function onMoveNextRequest() {},
  onMovePrevRequest: function onMovePrevRequest() {},
  prevLabel: 'Previous image',
  prevSrc: null,
  prevSrcThumbnail: null,
  reactModalStyle: {},
  wrapperClassName: '',
  zoomInLabel: 'Zoom in',
  zoomOutLabel: 'Zoom out',
  imageLoadErrorMessage: 'This image failed to load'
};

var FullScreenIcon = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAQAAADZc7J/AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAADdcAAA3XAUIom3gAAAAHdElNRQfiBRAIDipEin0mAAAAoklEQVRIx+1VwQmAMAy8SAfSCTqDi2idRNNFnMEJ6ghuUh9qrdqHUkER73UJyQUSuBBbBEBV0WwzWtk6VJkgEs8LCMd6dF7eHCoN2Isk0omQWyKX6vxcblC+ZQcfEBBUzcxcaaMWQ+zoH18CaTUzU3Tn27RENjHhrJJxQcDmvx/cKLC68sbWqd0fVUube6Gz9fUvpEsKADAcjpoth3vbDqIFRu8AJCdWHLSTAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE4LTA1LTE2VDA4OjE0OjQyKzAyOjAwAj0tXQAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxOC0wNS0xNlQwODoxNDo0MiswMjowMHNgleEAAAAZdEVYdFNvZnR3YXJlAHd3dy5pbmtzY2FwZS5vcmeb7jwaAAAAAElFTkSuQmCC";

var css$3 = "@keyframes closeWindow {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n  }\n}\n\n.ril__outer {\n  background-color: rgba(0, 0, 0, 0.85);\n  outline: none;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  z-index: 1000;\n  width: 100%;\n  height: 100%;\n  -ms-content-zooming: none;\n  -ms-user-select: none;\n  -ms-touch-select: none;\n  touch-action: none;\n}\n\n.ril__outerClosing {\n  opacity: 0;\n}\n\n.ril__inner {\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n}\n\n.ril__image,\n.ril__imagePrev,\n.ril__imageNext {\n  position: absolute;\n  top: 0;\n  right: 0;\n  bottom: 0;\n  left: 0;\n  margin: auto;\n  max-width: none;\n  -ms-content-zooming: none;\n  -ms-user-select: none;\n  -ms-touch-select: none;\n  touch-action: none;\n}\n\n.ril__imageDiscourager {\n  background-repeat: no-repeat;\n  background-position: center;\n  background-size: contain;\n}\n\n.ril__navButtons {\n  border: none;\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  width: 20px;\n  height: 34px;\n  padding: 40px 30px;\n  margin: auto;\n  cursor: pointer;\n  opacity: 0.7;\n}\n.ril__navButtons:hover {\n  opacity: 1;\n}\n.ril__navButtons:active {\n  opacity: 0.7;\n}\n\n.ril__navButtonPrev {\n  left: 0;\n  background: rgba(0, 0, 0, 0.2)\n    url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgd2lkdGg9IjIwIiBoZWlnaHQ9IjM0Ij48cGF0aCBkPSJtIDE5LDMgLTIsLTIgLTE2LDE2IDE2LDE2IDEsLTEgLTE1LC0xNSAxNSwtMTUgeiIgZmlsbD0iI0ZGRiIvPjwvc3ZnPg==')\n    no-repeat center;\n}\n\n.ril__navButtonNext {\n  right: 0;\n  background: rgba(0, 0, 0, 0.2)\n    url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgd2lkdGg9IjIwIiBoZWlnaHQ9IjM0Ij48cGF0aCBkPSJtIDEsMyAyLC0yIDE2LDE2IC0xNiwxNiAtMSwtMSAxNSwtMTUgLTE1LC0xNSB6IiBmaWxsPSIjRkZGIi8+PC9zdmc+')\n    no-repeat center;\n}\n\n.ril__downloadBlocker {\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background-image: url('data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7');\n  background-size: cover;\n}\n\n.ril__caption,\n.ril__toolbar {\n  background-color: rgba(0, 0, 0, 0.5);\n  position: absolute;\n  left: 0;\n  right: 0;\n  display: flex;\n  justify-content: space-between;\n}\n\n.ril__caption {\n  bottom: 0;\n  max-height: 150px;\n  overflow: auto;\n}\n\n.ril__captionContent {\n  padding: 10px 20px;\n  color: #fff;\n}\n\n.ril__toolbar {\n  top: 0;\n  height: 50px;\n}\n\n.ril__toolbarSide {\n  height: 50px;\n  margin: 0;\n}\n\n.ril__toolbarLeftSide {\n  padding-left: 20px;\n  padding-right: 0;\n  flex: 0 1 auto;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n\n.ril__toolbarRightSide {\n  padding-left: 0;\n  padding-right: 20px;\n  flex: 0 0 auto;\n}\n\n.ril__toolbarItem {\n  display: inline-block;\n  line-height: 50px;\n  padding: 0;\n  color: #fff;\n  font-size: 120%;\n  max-width: 100%;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n}\n\n.ril__toolbarItemChild {\n  vertical-align: middle;\n}\n\n.ril__builtinButton {\n  width: 40px;\n  height: 35px;\n  cursor: pointer;\n  border: none;\n  opacity: 0.7;\n}\n.ril__builtinButton:hover {\n  opacity: 1;\n}\n.ril__builtinButton:active {\n  outline: none;\n}\n\n.ril__builtinButtonDisabled {\n  cursor: default;\n  opacity: 0.5;\n}\n.ril__builtinButtonDisabled:hover {\n  opacity: 0.5;\n}\n\n.ril__closeButton {\n  background: url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgd2lkdGg9IjIwIiBoZWlnaHQ9IjIwIj48cGF0aCBkPSJtIDEsMyAxLjI1LC0xLjI1IDcuNSw3LjUgNy41LC03LjUgMS4yNSwxLjI1IC03LjUsNy41IDcuNSw3LjUgLTEuMjUsMS4yNSAtNy41LC03LjUgLTcuNSw3LjUgLTEuMjUsLTEuMjUgNy41LC03LjUgLTcuNSwtNy41IHoiIGZpbGw9IiNGRkYiLz48L3N2Zz4=')\n    no-repeat center;\n}\n\n.ril__zoomInButton {\n  background: url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCI+PGcgc3Ryb2tlPSIjZmZmIiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCI+PHBhdGggZD0iTTEgMTlsNi02Ii8+PHBhdGggZD0iTTkgOGg2Ii8+PHBhdGggZD0iTTEyIDV2NiIvPjwvZz48Y2lyY2xlIGN4PSIxMiIgY3k9IjgiIHI9IjciIGZpbGw9Im5vbmUiIHN0cm9rZT0iI2ZmZiIgc3Ryb2tlLXdpZHRoPSIyIi8+PC9zdmc+')\n    no-repeat center;\n}\n\n.ril__zoomOutButton {\n  background: url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCI+PGcgc3Ryb2tlPSIjZmZmIiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCI+PHBhdGggZD0iTTEgMTlsNi02Ii8+PHBhdGggZD0iTTkgOGg2Ii8+PC9nPjxjaXJjbGUgY3g9IjEyIiBjeT0iOCIgcj0iNyIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjZmZmIiBzdHJva2Utd2lkdGg9IjIiLz48L3N2Zz4=')\n    no-repeat center;\n}\n\n.ril__outerAnimating {\n  animation-name: closeWindow;\n}\n\n@keyframes pointFade {\n  0%,\n  19.999%,\n  100% {\n    opacity: 0;\n  }\n  20% {\n    opacity: 1;\n  }\n}\n\n.ril__loadingCircle {\n  width: 60px;\n  height: 60px;\n  position: relative;\n}\n\n.ril__loadingCirclePoint {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  left: 0;\n  top: 0;\n}\n.ril__loadingCirclePoint::before {\n  content: '';\n  display: block;\n  margin: 0 auto;\n  width: 11%;\n  height: 30%;\n  background-color: #fff;\n  border-radius: 30%;\n  animation: pointFade 800ms infinite ease-in-out both;\n}\n.ril__loadingCirclePoint:nth-of-type(1) {\n  transform: rotate(0deg);\n}\n.ril__loadingCirclePoint:nth-of-type(7) {\n  transform: rotate(180deg);\n}\n.ril__loadingCirclePoint:nth-of-type(1)::before,\n.ril__loadingCirclePoint:nth-of-type(7)::before {\n  animation-delay: -800ms;\n}\n.ril__loadingCirclePoint:nth-of-type(2) {\n  transform: rotate(30deg);\n}\n.ril__loadingCirclePoint:nth-of-type(8) {\n  transform: rotate(210deg);\n}\n.ril__loadingCirclePoint:nth-of-type(2)::before,\n.ril__loadingCirclePoint:nth-of-type(8)::before {\n  animation-delay: -666ms;\n}\n.ril__loadingCirclePoint:nth-of-type(3) {\n  transform: rotate(60deg);\n}\n.ril__loadingCirclePoint:nth-of-type(9) {\n  transform: rotate(240deg);\n}\n.ril__loadingCirclePoint:nth-of-type(3)::before,\n.ril__loadingCirclePoint:nth-of-type(9)::before {\n  animation-delay: -533ms;\n}\n.ril__loadingCirclePoint:nth-of-type(4) {\n  transform: rotate(90deg);\n}\n.ril__loadingCirclePoint:nth-of-type(10) {\n  transform: rotate(270deg);\n}\n.ril__loadingCirclePoint:nth-of-type(4)::before,\n.ril__loadingCirclePoint:nth-of-type(10)::before {\n  animation-delay: -400ms;\n}\n.ril__loadingCirclePoint:nth-of-type(5) {\n  transform: rotate(120deg);\n}\n.ril__loadingCirclePoint:nth-of-type(11) {\n  transform: rotate(300deg);\n}\n.ril__loadingCirclePoint:nth-of-type(5)::before,\n.ril__loadingCirclePoint:nth-of-type(11)::before {\n  animation-delay: -266ms;\n}\n.ril__loadingCirclePoint:nth-of-type(6) {\n  transform: rotate(150deg);\n}\n.ril__loadingCirclePoint:nth-of-type(12) {\n  transform: rotate(330deg);\n}\n.ril__loadingCirclePoint:nth-of-type(6)::before,\n.ril__loadingCirclePoint:nth-of-type(12)::before {\n  animation-delay: -133ms;\n}\n.ril__loadingCirclePoint:nth-of-type(7) {\n  transform: rotate(180deg);\n}\n.ril__loadingCirclePoint:nth-of-type(13) {\n  transform: rotate(360deg);\n}\n.ril__loadingCirclePoint:nth-of-type(7)::before,\n.ril__loadingCirclePoint:nth-of-type(13)::before {\n  animation-delay: 0ms;\n}\n\n.ril__loadingContainer {\n  position: absolute;\n  top: 0;\n  right: 0;\n  bottom: 0;\n  left: 0;\n}\n.ril__imagePrev .ril__loadingContainer,\n.ril__imageNext .ril__loadingContainer {\n  display: none;\n}\n\n.ril__errorContainer {\n  position: absolute;\n  top: 0;\n  right: 0;\n  bottom: 0;\n  left: 0;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  color: #fff;\n}\n.ril__imagePrev .ril__errorContainer,\n.ril__imageNext .ril__errorContainer {\n  display: none;\n}\n\n.ril__loadingContainer__icon {\n  color: #fff;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translateX(-50%) translateY(-50%);\n}\n";
styleInject(css$3);

var Audio = createCommonjsModule(function (module, exports) {
(function (global, factory) {
  {
    factory(exports, React__default);
  }
})(commonjsGlobal, function (exports, _react) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Audio = undefined;

  var _react2 = _interopRequireDefault(_react);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  var Audio = exports.Audio = function Audio(svg) {
    return _react2.default.createElement(
      "svg",
      {
        height: svg.height,
        width: svg.width,
        fill: svg.color,
        viewBox: "0 0 55 80",
        xmlns: "http://www.w3.org/2000/svg"
      },
      _react2.default.createElement(
        "g",
        { transform: "matrix(1 0 0 -1 0 80)" },
        _react2.default.createElement(
          "rect",
          { width: "10", height: "20", rx: "3" },
          _react2.default.createElement("animate", {
            attributeName: "height",
            begin: "0s",
            dur: "4.3s",
            values: "20;45;57;80;64;32;66;45;64;23;66;13;64;56;34;34;2;23;76;79;20",
            calcMode: "linear",
            repeatCount: "indefinite"
          })
        ),
        _react2.default.createElement(
          "rect",
          { x: "15", width: "10", height: "80", rx: "3" },
          _react2.default.createElement("animate", {
            attributeName: "height",
            begin: "0s",
            dur: "2s",
            values: "80;55;33;5;75;23;73;33;12;14;60;80",
            calcMode: "linear",
            repeatCount: "indefinite"
          })
        ),
        _react2.default.createElement(
          "rect",
          { x: "30", width: "10", height: "50", rx: "3" },
          _react2.default.createElement("animate", {
            attributeName: "height",
            begin: "0s",
            dur: "1.4s",
            values: "50;34;78;23;56;23;34;76;80;54;21;50",
            calcMode: "linear",
            repeatCount: "indefinite"
          })
        ),
        _react2.default.createElement(
          "rect",
          { x: "45", width: "10", height: "30", rx: "3" },
          _react2.default.createElement("animate", {
            attributeName: "height",
            begin: "0s",
            dur: "2s",
            values: "30;45;13;80;56;72;45;76;34;23;67;30",
            calcMode: "linear",
            repeatCount: "indefinite"
          })
        )
      )
    );
  };
});
});

unwrapExports(Audio);

var BallTriangle = createCommonjsModule(function (module, exports) {
(function (global, factory) {
  {
    factory(exports, React__default);
  }
})(commonjsGlobal, function (exports, _react) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.BallTriangle = undefined;

  var _react2 = _interopRequireDefault(_react);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  var BallTriangle = exports.BallTriangle = function BallTriangle(svg) {
    return _react2.default.createElement(
      "svg",
      {
        height: svg.height,
        width: svg.width,
        stroke: svg.color,
        viewBox: "0 0 57 57",
        xmlns: "http://www.w3.org/2000/svg"
      },
      _react2.default.createElement(
        "g",
        { fill: "none", fillRule: "evenodd" },
        _react2.default.createElement(
          "g",
          { transform: "translate(1 1)", strokeWidth: "2" },
          _react2.default.createElement(
            "circle",
            { cx: "5", cy: "50", r: "5" },
            _react2.default.createElement("animate", {
              attributeName: "cy",
              begin: "0s",
              dur: "2.2s",
              values: "50;5;50;50",
              calcMode: "linear",
              repeatCount: "indefinite"
            }),
            _react2.default.createElement("animate", {
              attributeName: "cx",
              begin: "0s",
              dur: "2.2s",
              values: "5;27;49;5",
              calcMode: "linear",
              repeatCount: "indefinite"
            })
          ),
          _react2.default.createElement(
            "circle",
            { cx: "27", cy: "5", r: "5" },
            _react2.default.createElement("animate", {
              attributeName: "cy",
              begin: "0s",
              dur: "2.2s",
              from: "5",
              to: "5",
              values: "5;50;50;5",
              calcMode: "linear",
              repeatCount: "indefinite"
            }),
            _react2.default.createElement("animate", {
              attributeName: "cx",
              begin: "0s",
              dur: "2.2s",
              from: "27",
              to: "27",
              values: "27;49;5;27",
              calcMode: "linear",
              repeatCount: "indefinite"
            })
          ),
          _react2.default.createElement(
            "circle",
            { cx: "49", cy: "50", r: "5" },
            _react2.default.createElement("animate", {
              attributeName: "cy",
              begin: "0s",
              dur: "2.2s",
              values: "50;50;5;50",
              calcMode: "linear",
              repeatCount: "indefinite"
            }),
            _react2.default.createElement("animate", {
              attributeName: "cx",
              from: "49",
              to: "49",
              begin: "0s",
              dur: "2.2s",
              values: "49;5;27;49",
              calcMode: "linear",
              repeatCount: "indefinite"
            })
          )
        )
      )
    );
  };
});
});

unwrapExports(BallTriangle);

var Bars = createCommonjsModule(function (module, exports) {
(function (global, factory) {
  {
    factory(exports, React__default);
  }
})(commonjsGlobal, function (exports, _react) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Bars = undefined;

  var _react2 = _interopRequireDefault(_react);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  var Bars = exports.Bars = function Bars(svg) {
    return _react2.default.createElement(
      "svg",
      {
        width: svg.height,
        height: svg.width,
        fill: svg.color,
        viewBox: "0 0 135 140",
        xmlns: "http://www.w3.org/2000/svg"
      },
      _react2.default.createElement(
        "rect",
        { y: "10", width: "15", height: "120", rx: "6" },
        _react2.default.createElement("animate", {
          attributeName: "height",
          begin: "0.5s",
          dur: "1s",
          values: "120;110;100;90;80;70;60;50;40;140;120",
          calcMode: "linear",
          repeatCount: "indefinite"
        }),
        _react2.default.createElement("animate", {
          attributeName: "y",
          begin: "0.5s",
          dur: "1s",
          values: "10;15;20;25;30;35;40;45;50;0;10",
          calcMode: "linear",
          repeatCount: "indefinite"
        })
      ),
      _react2.default.createElement(
        "rect",
        { x: "30", y: "10", width: "15", height: "120", rx: "6" },
        _react2.default.createElement("animate", {
          attributeName: "height",
          begin: "0.25s",
          dur: "1s",
          values: "120;110;100;90;80;70;60;50;40;140;120",
          calcMode: "linear",
          repeatCount: "indefinite"
        }),
        _react2.default.createElement("animate", {
          attributeName: "y",
          begin: "0.25s",
          dur: "1s",
          values: "10;15;20;25;30;35;40;45;50;0;10",
          calcMode: "linear",
          repeatCount: "indefinite"
        })
      ),
      _react2.default.createElement(
        "rect",
        { x: "60", width: "15", height: "140", rx: "6" },
        _react2.default.createElement("animate", {
          attributeName: "height",
          begin: "0s",
          dur: "1s",
          values: "120;110;100;90;80;70;60;50;40;140;120",
          calcMode: "linear",
          repeatCount: "indefinite"
        }),
        _react2.default.createElement("animate", {
          attributeName: "y",
          begin: "0s",
          dur: "1s",
          values: "10;15;20;25;30;35;40;45;50;0;10",
          calcMode: "linear",
          repeatCount: "indefinite"
        })
      ),
      _react2.default.createElement(
        "rect",
        { x: "90", y: "10", width: "15", height: "120", rx: "6" },
        _react2.default.createElement("animate", {
          attributeName: "height",
          begin: "0.25s",
          dur: "1s",
          values: "120;110;100;90;80;70;60;50;40;140;120",
          calcMode: "linear",
          repeatCount: "indefinite"
        }),
        _react2.default.createElement("animate", {
          attributeName: "y",
          begin: "0.25s",
          dur: "1s",
          values: "10;15;20;25;30;35;40;45;50;0;10",
          calcMode: "linear",
          repeatCount: "indefinite"
        })
      ),
      _react2.default.createElement(
        "rect",
        { x: "120", y: "10", width: "15", height: "120", rx: "6" },
        _react2.default.createElement("animate", {
          attributeName: "height",
          begin: "0.5s",
          dur: "1s",
          values: "120;110;100;90;80;70;60;50;40;140;120",
          calcMode: "linear",
          repeatCount: "indefinite"
        }),
        _react2.default.createElement("animate", {
          attributeName: "y",
          begin: "0.5s",
          dur: "1s",
          values: "10;15;20;25;30;35;40;45;50;0;10",
          calcMode: "linear",
          repeatCount: "indefinite"
        })
      )
    );
  };
});
});

unwrapExports(Bars);

var Circles = createCommonjsModule(function (module, exports) {
(function (global, factory) {
  {
    factory(exports, React__default);
  }
})(commonjsGlobal, function (exports, _react) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Circles = undefined;

  var _react2 = _interopRequireDefault(_react);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  var Circles = exports.Circles = function Circles(svg) {
    return _react2.default.createElement(
      "svg",
      {
        width: svg.width,
        height: svg.height,
        viewBox: "0 0 135 135",
        xmlns: "http://www.w3.org/2000/svg",
        fill: svg.color
      },
      _react2.default.createElement(
        "path",
        { d: "M67.447 58c5.523 0 10-4.477 10-10s-4.477-10-10-10-10 4.477-10 10 4.477 10 10 10zm9.448 9.447c0 5.523 4.477 10 10 10 5.522 0 10-4.477 10-10s-4.478-10-10-10c-5.523 0-10 4.477-10 10zm-9.448 9.448c-5.523 0-10 4.477-10 10 0 5.522 4.477 10 10 10s10-4.478 10-10c0-5.523-4.477-10-10-10zM58 67.447c0-5.523-4.477-10-10-10s-10 4.477-10 10 4.477 10 10 10 10-4.477 10-10z" },
        _react2.default.createElement("animateTransform", {
          attributeName: "transform",
          type: "rotate",
          from: "0 67 67",
          to: "-360 67 67",
          dur: "2.5s",
          repeatCount: "indefinite"
        })
      ),
      _react2.default.createElement(
        "path",
        { d: "M28.19 40.31c6.627 0 12-5.374 12-12 0-6.628-5.373-12-12-12-6.628 0-12 5.372-12 12 0 6.626 5.372 12 12 12zm30.72-19.825c4.686 4.687 12.284 4.687 16.97 0 4.686-4.686 4.686-12.284 0-16.97-4.686-4.687-12.284-4.687-16.97 0-4.687 4.686-4.687 12.284 0 16.97zm35.74 7.705c0 6.627 5.37 12 12 12 6.626 0 12-5.373 12-12 0-6.628-5.374-12-12-12-6.63 0-12 5.372-12 12zm19.822 30.72c-4.686 4.686-4.686 12.284 0 16.97 4.687 4.686 12.285 4.686 16.97 0 4.687-4.686 4.687-12.284 0-16.97-4.685-4.687-12.283-4.687-16.97 0zm-7.704 35.74c-6.627 0-12 5.37-12 12 0 6.626 5.373 12 12 12s12-5.374 12-12c0-6.63-5.373-12-12-12zm-30.72 19.822c-4.686-4.686-12.284-4.686-16.97 0-4.686 4.687-4.686 12.285 0 16.97 4.686 4.687 12.284 4.687 16.97 0 4.687-4.685 4.687-12.283 0-16.97zm-35.74-7.704c0-6.627-5.372-12-12-12-6.626 0-12 5.373-12 12s5.374 12 12 12c6.628 0 12-5.373 12-12zm-19.823-30.72c4.687-4.686 4.687-12.284 0-16.97-4.686-4.686-12.284-4.686-16.97 0-4.687 4.686-4.687 12.284 0 16.97 4.686 4.687 12.284 4.687 16.97 0z" },
        _react2.default.createElement("animateTransform", {
          attributeName: "transform",
          type: "rotate",
          from: "0 67 67",
          to: "360 67 67",
          dur: "8s",
          repeatCount: "indefinite"
        })
      )
    );
  };
});
});

unwrapExports(Circles);

var Grid = createCommonjsModule(function (module, exports) {
(function (global, factory) {
  {
    factory(exports, React__default);
  }
})(commonjsGlobal, function (exports, _react) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Grid = undefined;

  var _react2 = _interopRequireDefault(_react);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  var Grid = exports.Grid = function Grid(svg) {
    return _react2.default.createElement(
      "svg",
      { width: svg.width, height: svg.height, viewBox: "0 0 105 105", fill: svg.color },
      _react2.default.createElement(
        "circle",
        { cx: "12.5", cy: "12.5", r: "12.5" },
        _react2.default.createElement("animate", {
          attributeName: "fill-opacity",
          begin: "0s",
          dur: "1s",
          values: "1;.2;1",
          calcMode: "linear",
          repeatCount: "indefinite"
        })
      ),
      _react2.default.createElement(
        "circle",
        { cx: "12.5", cy: "52.5", r: "12.5" },
        _react2.default.createElement("animate", {
          attributeName: "fill-opacity",
          begin: "100ms",
          dur: "1s",
          values: "1;.2;1",
          calcMode: "linear",
          repeatCount: "indefinite"
        })
      ),
      _react2.default.createElement(
        "circle",
        { cx: "52.5", cy: "12.5", r: "12.5" },
        _react2.default.createElement("animate", {
          attributeName: "fill-opacity",
          begin: "300ms",
          dur: "1s",
          values: "1;.2;1",
          calcMode: "linear",
          repeatCount: "indefinite"
        })
      ),
      _react2.default.createElement(
        "circle",
        { cx: "52.5", cy: "52.5", r: "12.5" },
        _react2.default.createElement("animate", {
          attributeName: "fill-opacity",
          begin: "600ms",
          dur: "1s",
          values: "1;.2;1",
          calcMode: "linear",
          repeatCount: "indefinite"
        })
      ),
      _react2.default.createElement(
        "circle",
        { cx: "92.5", cy: "12.5", r: "12.5" },
        _react2.default.createElement("animate", {
          attributeName: "fill-opacity",
          begin: "800ms",
          dur: "1s",
          values: "1;.2;1",
          calcMode: "linear",
          repeatCount: "indefinite"
        })
      ),
      _react2.default.createElement(
        "circle",
        { cx: "92.5", cy: "52.5", r: "12.5" },
        _react2.default.createElement("animate", {
          attributeName: "fill-opacity",
          begin: "400ms",
          dur: "1s",
          values: "1;.2;1",
          calcMode: "linear",
          repeatCount: "indefinite"
        })
      ),
      _react2.default.createElement(
        "circle",
        { cx: "12.5", cy: "92.5", r: "12.5" },
        _react2.default.createElement("animate", {
          attributeName: "fill-opacity",
          begin: "700ms",
          dur: "1s",
          values: "1;.2;1",
          calcMode: "linear",
          repeatCount: "indefinite"
        })
      ),
      _react2.default.createElement(
        "circle",
        { cx: "52.5", cy: "92.5", r: "12.5" },
        _react2.default.createElement("animate", {
          attributeName: "fill-opacity",
          begin: "500ms",
          dur: "1s",
          values: "1;.2;1",
          calcMode: "linear",
          repeatCount: "indefinite"
        })
      ),
      _react2.default.createElement(
        "circle",
        { cx: "92.5", cy: "92.5", r: "12.5" },
        _react2.default.createElement("animate", {
          attributeName: "fill-opacity",
          begin: "200ms",
          dur: "1s",
          values: "1;.2;1",
          calcMode: "linear",
          repeatCount: "indefinite"
        })
      )
    );
  };
});
});

unwrapExports(Grid);

var Hearts = createCommonjsModule(function (module, exports) {
(function (global, factory) {
  {
    factory(exports, React__default);
  }
})(commonjsGlobal, function (exports, _react) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Hearts = undefined;

  var _react2 = _interopRequireDefault(_react);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  var Hearts = exports.Hearts = function Hearts(svg) {
    return _react2.default.createElement(
      "svg",
      {
        width: svg.width,
        height: svg.height,
        viewBox: "0 0 140 64",
        xmlns: "http://www.w3.org/2000/svg",
        fill: svg.color
      },
      _react2.default.createElement(
        "path",
        {
          d: "M30.262 57.02L7.195 40.723c-5.84-3.976-7.56-12.06-3.842-18.063 3.715-6 11.467-7.65 17.306-3.68l4.52 3.76 2.6-5.274c3.717-6.002 11.47-7.65 17.305-3.68 5.84 3.97 7.56 12.054 3.842 18.062L34.49 56.118c-.897 1.512-2.793 1.915-4.228.9z",
          attributeName: "fill-opacity",
          from: "0",
          to: ".5"
        },
        _react2.default.createElement("animate", {
          attributeName: "fill-opacity",
          begin: "0s",
          dur: "1.4s",
          values: "0.5;1;0.5",
          calcMode: "linear",
          repeatCount: "indefinite"
        })
      ),
      _react2.default.createElement(
        "path",
        {
          d: "M105.512 56.12l-14.44-24.272c-3.716-6.008-1.996-14.093 3.843-18.062 5.835-3.97 13.588-2.322 17.306 3.68l2.6 5.274 4.52-3.76c5.84-3.97 13.592-2.32 17.307 3.68 3.718 6.003 1.998 14.088-3.842 18.064L109.74 57.02c-1.434 1.014-3.33.61-4.228-.9z",
          attributeName: "fill-opacity",
          from: "0",
          to: ".5"
        },
        _react2.default.createElement("animate", {
          attributeName: "fill-opacity",
          begin: "0.7s",
          dur: "1.4s",
          values: "0.5;1;0.5",
          calcMode: "linear",
          repeatCount: "indefinite"
        })
      ),
      _react2.default.createElement("path", { d: "M67.408 57.834l-23.01-24.98c-5.864-6.15-5.864-16.108 0-22.248 5.86-6.14 15.37-6.14 21.234 0L70 16.168l4.368-5.562c5.863-6.14 15.375-6.14 21.235 0 5.863 6.14 5.863 16.098 0 22.247l-23.007 24.98c-1.43 1.556-3.757 1.556-5.188 0z" })
    );
  };
});
});

unwrapExports(Hearts);

var Oval = createCommonjsModule(function (module, exports) {
(function (global, factory) {
  {
    factory(exports, React__default);
  }
})(commonjsGlobal, function (exports, _react) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Oval = undefined;

  var _react2 = _interopRequireDefault(_react);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  var Oval = exports.Oval = function Oval(svg) {
    return _react2.default.createElement(
      "svg",
      {
        width: svg.width,
        height: svg.height,
        viewBox: "0 0 38 38",
        xmlns: "http://www.w3.org/2000/svg",
        stroke: svg.color
      },
      _react2.default.createElement(
        "g",
        { fill: "none", fillRule: "evenodd" },
        _react2.default.createElement(
          "g",
          { transform: "translate(1 1)", strokeWidth: "2" },
          _react2.default.createElement("circle", { strokeOpacity: ".5", cx: "18", cy: "18", r: "18" }),
          _react2.default.createElement(
            "path",
            { d: "M36 18c0-9.94-8.06-18-18-18" },
            _react2.default.createElement("animateTransform", {
              attributeName: "transform",
              type: "rotate",
              from: "0 18 18",
              to: "360 18 18",
              dur: "1s",
              repeatCount: "indefinite"
            })
          )
        )
      )
    );
  };
});
});

unwrapExports(Oval);

var Puff = createCommonjsModule(function (module, exports) {
(function (global, factory) {
  {
    factory(exports, React__default);
  }
})(commonjsGlobal, function (exports, _react) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Puff = undefined;

  var _react2 = _interopRequireDefault(_react);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  var Puff = exports.Puff = function Puff(svg) {
    return _react2.default.createElement(
      "svg",
      {
        width: svg.width,
        height: svg.height,
        viewBox: "0 0 44 44",
        xmlns: "http://www.w3.org/2000/svg",
        stroke: svg.color
      },
      _react2.default.createElement(
        "g",
        { fill: "none", fillRule: "evenodd", strokeWidth: "2" },
        _react2.default.createElement(
          "circle",
          { cx: "22", cy: "22", r: "1" },
          _react2.default.createElement("animate", {
            attributeName: "r",
            begin: "0s",
            dur: "1.8s",
            values: "1; 20",
            calcMode: "spline",
            keyTimes: "0; 1",
            keySplines: "0.165, 0.84, 0.44, 1",
            repeatCount: "indefinite"
          }),
          _react2.default.createElement("animate", {
            attributeName: "strokeOpacity",
            begin: "0s",
            dur: "1.8s",
            values: "1; 0",
            calcMode: "spline",
            keyTimes: "0; 1",
            keySplines: "0.3, 0.61, 0.355, 1",
            repeatCount: "indefinite"
          })
        ),
        _react2.default.createElement(
          "circle",
          { cx: "22", cy: "22", r: "1" },
          _react2.default.createElement("animate", {
            attributeName: "r",
            begin: "-0.9s",
            dur: "1.8s",
            values: "1; 20",
            calcMode: "spline",
            keyTimes: "0; 1",
            keySplines: "0.165, 0.84, 0.44, 1",
            repeatCount: "indefinite"
          }),
          _react2.default.createElement("animate", {
            attributeName: "strokeOpacity",
            begin: "-0.9s",
            dur: "1.8s",
            values: "1; 0",
            calcMode: "spline",
            keyTimes: "0; 1",
            keySplines: "0.3, 0.61, 0.355, 1",
            repeatCount: "indefinite"
          })
        )
      )
    );
  };
});
});

unwrapExports(Puff);

var Rings = createCommonjsModule(function (module, exports) {
(function (global, factory) {
  {
    factory(exports, React__default);
  }
})(commonjsGlobal, function (exports, _react) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Rings = undefined;

  var _react2 = _interopRequireDefault(_react);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  var Rings = exports.Rings = function Rings(svg) {
    return _react2.default.createElement(
      "svg",
      {
        width: svg.width,
        height: svg.height,
        viewBox: "0 0 45 45",
        xmlns: "http://www.w3.org/2000/svg",
        stroke: svg.color
      },
      _react2.default.createElement(
        "g",
        { fill: "none", fillRule: "evenodd", transform: "translate(1 1)", strokeWidth: "2" },
        _react2.default.createElement(
          "circle",
          { cx: "22", cy: "22", r: "6", strokeOpacity: "0" },
          _react2.default.createElement("animate", {
            attributeName: "r",
            begin: "1.5s",
            dur: "3s",
            values: "6;22",
            calcMode: "linear",
            repeatCount: "indefinite"
          }),
          _react2.default.createElement("animate", {
            attributeName: "stroke-opacity",
            begin: "1.5s",
            dur: "3s",
            values: "1;0",
            calcMode: "linear",
            repeatCount: "indefinite"
          }),
          _react2.default.createElement("animate", {
            attributeName: "stroke-width",
            begin: "1.5s",
            dur: "3s",
            values: "2;0",
            calcMode: "linear",
            repeatCount: "indefinite"
          })
        ),
        _react2.default.createElement(
          "circle",
          { cx: "22", cy: "22", r: "6", strokeOpacity: "0" },
          _react2.default.createElement("animate", {
            attributeName: "r",
            begin: "3s",
            dur: "3s",
            values: "6;22",
            calcMode: "linear",
            repeatCount: "indefinite"
          }),
          _react2.default.createElement("animate", {
            attributeName: "strokeOpacity",
            begin: "3s",
            dur: "3s",
            values: "1;0",
            calcMode: "linear",
            repeatCount: "indefinite"
          }),
          _react2.default.createElement("animate", {
            attributeName: "strokeWidth",
            begin: "3s",
            dur: "3s",
            values: "2;0",
            calcMode: "linear",
            repeatCount: "indefinite"
          })
        ),
        _react2.default.createElement(
          "circle",
          { cx: "22", cy: "22", r: "8" },
          _react2.default.createElement("animate", {
            attributeName: "r",
            begin: "0s",
            dur: "1.5s",
            values: "6;1;2;3;4;5;6",
            calcMode: "linear",
            repeatCount: "indefinite"
          })
        )
      )
    );
  };
});
});

unwrapExports(Rings);

var TailSpin = createCommonjsModule(function (module, exports) {
(function (global, factory) {
  {
    factory(exports, React__default);
  }
})(commonjsGlobal, function (exports, _react) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.TailSpin = undefined;

  var _react2 = _interopRequireDefault(_react);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  var TailSpin = exports.TailSpin = function TailSpin(svg) {
    return _react2.default.createElement(
      "svg",
      { width: svg.width, height: svg.height, viewBox: "0 0 38 38", xmlns: "http://www.w3.org/2000/svg" },
      _react2.default.createElement(
        "defs",
        null,
        _react2.default.createElement(
          "linearGradient",
          { x1: "8.042%", y1: "0%", x2: "65.682%", y2: "23.865%", id: "a" },
          _react2.default.createElement("stop", { stopColor: svg.color, stopOpacity: "0", offset: "0%" }),
          _react2.default.createElement("stop", { stopColor: svg.color, stopOpacity: ".631", offset: "63.146%" }),
          _react2.default.createElement("stop", { stopColor: svg.color, offset: "100%" })
        )
      ),
      _react2.default.createElement(
        "g",
        { fill: "none", fillRule: "evenodd" },
        _react2.default.createElement(
          "g",
          { transform: "translate(1 1)" },
          _react2.default.createElement(
            "path",
            { d: "M36 18c0-9.94-8.06-18-18-18", id: "Oval-2", stroke: svg.color, strokeWidth: "2" },
            _react2.default.createElement("animateTransform", {
              attributeName: "transform",
              type: "rotate",
              from: "0 18 18",
              to: "360 18 18",
              dur: "0.9s",
              repeatCount: "indefinite"
            })
          ),
          _react2.default.createElement(
            "circle",
            { fill: "#fff", cx: "36", cy: "18", r: "1" },
            _react2.default.createElement("animateTransform", {
              attributeName: "transform",
              type: "rotate",
              from: "0 18 18",
              to: "360 18 18",
              dur: "0.9s",
              repeatCount: "indefinite"
            })
          )
        )
      )
    );
  };
});
});

unwrapExports(TailSpin);

var ThreeDots = createCommonjsModule(function (module, exports) {
(function (global, factory) {
  {
    factory(exports, React__default);
  }
})(commonjsGlobal, function (exports, _react) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.ThreeDots = undefined;

  var _react2 = _interopRequireDefault(_react);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  var ThreeDots = exports.ThreeDots = function ThreeDots(svg) {
    return _react2.default.createElement(
      "svg",
      {
        width: svg.width,
        height: svg.height,
        viewBox: "0 0 120 30",
        xmlns: "http://www.w3.org/2000/svg",
        fill: svg.color
      },
      _react2.default.createElement(
        "circle",
        { cx: "15", cy: "15", r: "15" },
        _react2.default.createElement("animate", {
          attributeName: "r",
          from: "15",
          to: "15",
          begin: "0s",
          dur: "0.8s",
          values: "15;9;15",
          calcMode: "linear",
          repeatCount: "indefinite"
        }),
        _react2.default.createElement("animate", {
          attributeName: "fillOpacity",
          from: "1",
          to: "1",
          begin: "0s",
          dur: "0.8s",
          values: "1;.5;1",
          calcMode: "linear",
          repeatCount: "indefinite"
        })
      ),
      _react2.default.createElement(
        "circle",
        { cx: "60", cy: "15", r: "9", attributeName: "fillOpacity", from: "1", to: "0.3" },
        _react2.default.createElement("animate", {
          attributeName: "r",
          from: "9",
          to: "9",
          begin: "0s",
          dur: "0.8s",
          values: "9;15;9",
          calcMode: "linear",
          repeatCount: "indefinite"
        }),
        _react2.default.createElement("animate", {
          attributeName: "fillOpacity",
          from: "0.5",
          to: "0.5",
          begin: "0s",
          dur: "0.8s",
          values: ".5;1;.5",
          calcMode: "linear",
          repeatCount: "indefinite"
        })
      ),
      _react2.default.createElement(
        "circle",
        { cx: "105", cy: "15", r: "15" },
        _react2.default.createElement("animate", {
          attributeName: "r",
          from: "15",
          to: "15",
          begin: "0s",
          dur: "0.8s",
          values: "15;9;15",
          calcMode: "linear",
          repeatCount: "indefinite"
        }),
        _react2.default.createElement("animate", {
          attributeName: "fillOpacity",
          from: "1",
          to: "1",
          begin: "0s",
          dur: "0.8s",
          values: "1;.5;1",
          calcMode: "linear",
          repeatCount: "indefinite"
        })
      )
    );
  };
});
});

unwrapExports(ThreeDots);

var Watch = createCommonjsModule(function (module, exports) {
(function (global, factory) {
  {
    factory(exports, React__default);
  }
})(commonjsGlobal, function (exports, _react) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Watch = undefined;

  var _react2 = _interopRequireDefault(_react);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  var Watch = exports.Watch = function Watch(svg) {
    return _react2.default.createElement(
      "svg",
      {
        width: svg.width,
        height: svg.height,
        version: "1.1",
        id: "L2",
        xmlns: "http://www.w3.org/2000/svg",
        x: "0px",
        y: "0px",
        viewBox: "0 0 100 100",
        enableBackground: "new 0 0 100 100",
        xmlSpace: "preserve"
      },
      _react2.default.createElement("circle", {
        fill: "none",
        stroke: svg.color,
        strokeWidth: "4",
        strokeMiterlimit: "10",
        cx: "50",
        cy: "50",
        r: "48"
      }),
      _react2.default.createElement(
        "line",
        {
          fill: "none",
          strokeLinecap: "round",
          stroke: svg.color,
          strokeWidth: "4",
          strokeMiterlimit: "10",
          x1: "50",
          y1: "50",
          x2: "85",
          y2: "50.5"
        },
        _react2.default.createElement("animateTransform", {
          attributeName: "transform",
          dur: "2s",
          type: "rotate",
          from: "0 50 50",
          to: "360 50 50",
          repeatCount: "indefinite"
        })
      ),
      _react2.default.createElement(
        "line",
        {
          fill: "none",
          strokeLinecap: "round",
          stroke: svg.color,
          strokeWidth: "4",
          strokeMiterlimit: "10",
          x1: "50",
          y1: "50",
          x2: "49.5",
          y2: "74"
        },
        _react2.default.createElement("animateTransform", {
          attributeName: "transform",
          dur: "15s",
          type: "rotate",
          from: "0 50 50",
          to: "360 50 50",
          repeatCount: "indefinite"
        })
      )
    );
  };
});
});

unwrapExports(Watch);

var RevolvingDot = createCommonjsModule(function (module, exports) {
(function (global, factory) {
  {
    factory(exports, React__default);
  }
})(commonjsGlobal, function (exports, _react) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.RevolvingDot = undefined;

  var _react2 = _interopRequireDefault(_react);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  var RevolvingDot = exports.RevolvingDot = function RevolvingDot(svg) {
    return _react2.default.createElement(
      "svg",
      {
        version: "1.1",
        width: svg.width,
        height: svg.height,
        id: "L3",
        xmlns: "http://www.w3.org/2000/svg",
        x: "0px",
        y: "0px",
        viewBox: "0 0 100 100",
        enableBackground: "new 0 0 0 0",
        xmlSpace: "preserve"
      },
      _react2.default.createElement("circle", {
        fill: "none",
        stroke: svg.color,
        strokeWidth: "4",
        cx: "50",
        cy: "50",
        r: "44",
        style: { opacity: 0.5 }
      }),
      _react2.default.createElement(
        "circle",
        { fill: svg.color, stroke: svg.color, strokeWidth: "3", cx: "8", cy: "54", r: "6" },
        _react2.default.createElement("animateTransform", {
          attributeName: "transform",
          dur: "2s",
          type: "rotate",
          from: "0 50 48",
          to: "360 50 52",
          repeatCount: "indefinite"
        })
      )
    );
  };
});
});

unwrapExports(RevolvingDot);

var css$4 = ".CradleLoader_react-spinner-loader-swing__DG6F- div {\n  border-radius: 50%;\n  float: left;\n  height: 1em;\n  width: 1em;\n}\n.CradleLoader_react-spinner-loader-swing__DG6F- div:nth-of-type(1) {\n  background: -webkit-linear-gradient(left, #385c78 0%, #325774 100%);\n  background: linear-gradient(to right, #385c78 0%, #325774 100%);\n}\n.CradleLoader_react-spinner-loader-swing__DG6F- div:nth-of-type(2) {\n  background: -webkit-linear-gradient(left, #325774 0%, #47536a 100%);\n  background: linear-gradient(to right, #325774 0%, #47536a 100%);\n}\n.CradleLoader_react-spinner-loader-swing__DG6F- div:nth-of-type(3) {\n  background: -webkit-linear-gradient(left, #4a5369 0%, #6b4d59 100%);\n  background: linear-gradient(to right, #4a5369 0%, #6b4d59 100%);\n}\n.CradleLoader_react-spinner-loader-swing__DG6F- div:nth-of-type(4) {\n  background: -webkit-linear-gradient(left, #744c55 0%, #954646 100%);\n  background: linear-gradient(to right, #744c55 0%, #954646 100%);\n}\n.CradleLoader_react-spinner-loader-swing__DG6F- div:nth-of-type(5) {\n  background: -webkit-linear-gradient(left, #9c4543 0%, #bb4034 100%);\n  background: linear-gradient(to right, #9c4543 0%, #bb4034 100%);\n}\n.CradleLoader_react-spinner-loader-swing__DG6F- div:nth-of-type(6) {\n  background: -webkit-linear-gradient(left, #c33f31 0%, #d83b27 100%);\n  background: linear-gradient(to right, #c33f31 0%, #d83b27 100%);\n}\n.CradleLoader_react-spinner-loader-swing__DG6F- div:nth-of-type(7) {\n  background: -webkit-linear-gradient(left, #da3b26 0%, #db412c 100%);\n  background: linear-gradient(to right, #da3b26 0%, #db412c 100%);\n}\n.CradleLoader_react-spinner-loader-shadow__1xKcx {\n  clear: left;\n  padding-top: 1.5em;\n}\n.CradleLoader_react-spinner-loader-shadow__1xKcx div {\n  -webkit-filter: blur(1px);\n  filter: blur(1px);\n  float: left;\n  width: 1em;\n  height: .25em;\n  border-radius: 50%;\n  background: #e3dbd2;\n}\n.CradleLoader_react-spinner-loader-shadow__1xKcx .CradleLoader_react-spinner-loader-shadow-l__nNXf5 {\n  background: #d5d8d6;\n}\n.CradleLoader_react-spinner-loader-shadow__1xKcx .CradleLoader_react-spinner-loader-shadow-r__2LcRa {\n  background: #eed3ca;\n}\n@-webkit-keyframes CradleLoader_ball-l__tbuZa {\n  0%, 50% {\n    -webkit-transform: rotate(0) translateX(0);\n    transform: rotate(0) translateX(0);\n  }\n  100% {\n    -webkit-transform: rotate(50deg) translateX(-2.5em);\n    transform: rotate(50deg) translateX(-2.5em);\n  }\n}\n@keyframes CradleLoader_ball-l__tbuZa {\n  0%, 50% {\n    -webkit-transform: rotate(0) translate(0);\n    transform: rotate(0) translateX(0);\n  }\n  100% {\n    -webkit-transform: rotate(50deg) translateX(-2.5em);\n    transform: rotate(50deg) translateX(-2.5em);\n  }\n}\n@-webkit-keyframes CradleLoader_ball-r__2sNRI {\n  0% {\n    -webkit-transform: rotate(-50deg) translateX(2.5em);\n    transform: rotate(-50deg) translateX(2.5em);\n  }\n  50%,\n  100% {\n    -webkit-transform: rotate(0) translateX(0);\n    transform: rotate(0) translateX(0);\n  }\n}\n@keyframes CradleLoader_ball-r__2sNRI {\n  0% {\n    -webkit-transform: rotate(-50deg) translateX(2.5em);\n    transform: rotate(-50deg) translateX(2.5em);\n  }\n  50%,\n  100% {\n    -webkit-transform: rotate(0) translateX(0);\n    transform: rotate(0) translateX(0)\n  }\n}\n@-webkit-keyframes CradleLoader_shadow-l-n__35K5l {\n  0%, 50% {\n    opacity: .5;\n    -webkit-transform: translateX(0);\n    transform: translateX(0);\n  }\n  100% {\n    opacity: .125;\n    -webkit-transform: translateX(-1.57em);\n    transform: translateX(-1.75em);\n  }\n}\n@keyframes CradleLoader_shadow-l-n__35K5l {\n  0%, 50% {\n    opacity: .5;\n    -webkit-transform: translateX(0);\n    transform: translateX(0);\n  }\n  100% {\n    opacity: .125;\n    -webkit-transform: translateX(-1.75);\n    transform: translateX(-1.75em);\n  }\n}\n@-webkit-keyframes CradleLoader_shadow-r-n__2bPej {\n  0% {\n    opacity: .125;\n    -webkit-transform: translateX(1.75em);\n    transform: translateX(1.75em);\n  }\n  50%,\n  100% {\n    opacity: .5;\n    -webkit-transform: translateX(0);\n    transform: translateX(0);\n  }\n}\n@keyframes CradleLoader_shadow-r-n__2bPej {\n  0% {\n    opacity: .125;\n    -webkit-transform: translateX(1.75em);\n    transform: translateX(1.75em);\n  }\n  50%,\n  100% {\n    opacity: .5;\n    -webkit-transform: translateX(0);\n    transform: translateX(0);\n  }\n}\n.CradleLoader_react-spinner-loader-swing-l__zwhAB {\n  -webkit-animation: CradleLoader_ball-l__tbuZa .425s ease-in-out infinite alternate;\n  animation: CradleLoader_ball-l__tbuZa .425s ease-in-out infinite alternate;\n}\n.CradleLoader_react-spinner-loader-swing-r__JSTHR {\n  -webkit-animation: CradleLoader_ball-r__2sNRI .425s ease-in-out infinite alternate;\n  animation: CradleLoader_ball-r__2sNRI .425s ease-in-out infinite alternate;\n}\n.CradleLoader_react-spinner-loader-shadow-l__nNXf5 {\n  -webkit-animation: CradleLoader_shadow-l-n__35K5l .425s ease-in-out infinite alternate;\n  animation: CradleLoader_shadow-l-n__35K5l .425s ease-in-out infinite alternate;\n}\n.CradleLoader_react-spinner-loader-shadow-r__2LcRa {\n  -webkit-animation: CradleLoader_shadow-r-n__2bPej .425s ease-in-out infinite alternate;\n  animation: CradleLoader_shadow-r-n__2bPej .425s ease-in-out infinite alternate;\n}\n";
var CradleLoader = {"react-spinner-loader-swing":"CradleLoader_react-spinner-loader-swing__DG6F-","react-spinner-loader-shadow":"CradleLoader_react-spinner-loader-shadow__1xKcx","react-spinner-loader-shadow-l":"CradleLoader_react-spinner-loader-shadow-l__nNXf5","react-spinner-loader-shadow-r":"CradleLoader_react-spinner-loader-shadow-r__2LcRa","react-spinner-loader-swing-l":"CradleLoader_react-spinner-loader-swing-l__zwhAB","ball-l":"CradleLoader_ball-l__tbuZa","react-spinner-loader-swing-r":"CradleLoader_react-spinner-loader-swing-r__JSTHR","ball-r":"CradleLoader_ball-r__2sNRI","shadow-l-n":"CradleLoader_shadow-l-n__35K5l","shadow-r-n":"CradleLoader_shadow-r-n__2bPej"};
styleInject(css$4);

var CradleLoader$1 = /*#__PURE__*/Object.freeze({
  default: CradleLoader
});

var require$$1 = getCjsExportFromNamespace(CradleLoader$1);

var CradleLoader$2 = createCommonjsModule(function (module, exports) {
(function (global, factory) {
  {
    factory(exports, React__default, require$$1);
  }
})(commonjsGlobal, function (exports, _react) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.CradleLoader = undefined;

  var _react2 = _interopRequireDefault(_react);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  var CradleLoader = exports.CradleLoader = function CradleLoader() {
    return _react2.default.createElement(
      "div",
      { "aria-busy": "true", "aria-label": "Loading", role: "progressbar", className: "container" },
      _react2.default.createElement(
        "div",
        { className: "react-spinner-loader-swing" },
        _react2.default.createElement("div", { className: "react-spinner-loader-swing-l" }),
        _react2.default.createElement("div", null),
        _react2.default.createElement("div", null),
        _react2.default.createElement("div", null),
        _react2.default.createElement("div", null),
        _react2.default.createElement("div", null),
        _react2.default.createElement("div", { className: "react-spinner-loader-swing-r" })
      ),
      _react2.default.createElement(
        "div",
        { className: "react-spinner-loader-shadow" },
        _react2.default.createElement("div", { className: "react-spinner-loader-shadow-l" }),
        _react2.default.createElement("div", null),
        _react2.default.createElement("div", null),
        _react2.default.createElement("div", null),
        _react2.default.createElement("div", null),
        _react2.default.createElement("div", null),
        _react2.default.createElement("div", { className: "react-spinner-loader-shadow-r" })
      )
    );
  };
});
});

unwrapExports(CradleLoader$2);

var css$5 = "\n.Triangle_react-spinner-loader-svg__3P5MA svg {\n  -webkit-transform-origin: 50% 65%;\n          transform-origin: 50% 65%;\n}\n\n.Triangle_react-spinner-loader-svg__3P5MA svg polygon {\n  stroke-dasharray: 17;\n  -webkit-animation: Triangle_dash__2hWz_ 2.5s cubic-bezier(0.35, 0.04, 0.63, 0.95) infinite;\n          animation: Triangle_dash__2hWz_ 2.5s cubic-bezier(0.35, 0.04, 0.63, 0.95) infinite;\n}\n\n@-webkit-keyframes Triangle_dash__2hWz_ {\n  to {\n    stroke-dashoffset: 136;\n  }\n}\n\n@keyframes Triangle_dash__2hWz_ {\n  to {\n    stroke-dashoffset: 136;\n  }\n}\n@-webkit-keyframes Triangle_rotate__3ZS_P {\n  100% {\n    -webkit-transform: rotate(360deg);\n            transform: rotate(360deg);\n  }\n}\n@keyframes Triangle_rotate__3ZS_P {\n  100% {\n    -webkit-transform: rotate(360deg);\n            transform: rotate(360deg);\n  }\n}\n";
var Triangle = {"react-spinner-loader-svg":"Triangle_react-spinner-loader-svg__3P5MA","dash":"Triangle_dash__2hWz_","rotate":"Triangle_rotate__3ZS_P"};
styleInject(css$5);

var Triangle$1 = /*#__PURE__*/Object.freeze({
  default: Triangle
});

var require$$1$1 = getCjsExportFromNamespace(Triangle$1);

var Triangle$2 = createCommonjsModule(function (module, exports) {
(function (global, factory) {
  {
    factory(exports, React__default, require$$1$1);
  }
})(commonjsGlobal, function (exports, _react) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Triangle = undefined;

  var _react2 = _interopRequireDefault(_react);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  var Triangle = exports.Triangle = function Triangle(svg) {
    return _react2.default.createElement(
      "div",
      { className: "react-spinner-loader-svg" },
      _react2.default.createElement(
        "svg",
        { id: "triangle", width: svg.height, height: svg.height, viewBox: "-3 -4 39 39" },
        _react2.default.createElement("polygon", { fill: "transparent", stroke: svg.color, strokeWidth: "1", points: "16,0 32,32 0,32" })
      )
    );
  };
});
});

unwrapExports(Triangle$2);

var css$6 = "\n.Plane_react-spinner-loader-svg-calLoader__3U3_Q {\n  width: 230px;\n  height: 230px;\n  transform-origin: 115px 115px;\n  animation: 1.4s linear infinite :local(loader-spin);\n}\n\n.Plane_react-spinner-loader-svg-cal-loader__path__5pfEG {\n\n  animation: 1.4s ease-in-out infinite :local(loader-path);\n}\n\n@keyframes Plane_loader-spin__2Oqbe {\n  to {\n    transform: rotate(360deg);\n  }\n}\n@keyframes Plane_loader-path__2LaO1 {\n  0% {\n    stroke-dasharray: 0, 580, 0, 0, 0, 0, 0, 0, 0;\n  }\n  50% {\n    stroke-dasharray: 0, 450, 10, 30, 10, 30, 10, 30, 10;\n  }\n  100% {\n    stroke-dasharray: 0, 580, 0, 0, 0, 0, 0, 0, 0;\n  }\n}\n";
var Plane = {"react-spinner-loader-svg-calLoader":"Plane_react-spinner-loader-svg-calLoader__3U3_Q","react-spinner-loader-svg-cal-loader__path":"Plane_react-spinner-loader-svg-cal-loader__path__5pfEG","loader-spin":"Plane_loader-spin__2Oqbe","loader-path":"Plane_loader-path__2LaO1"};
styleInject(css$6);

var Plane$1 = /*#__PURE__*/Object.freeze({
  default: Plane
});

var require$$2 = getCjsExportFromNamespace(Plane$1);

var Plane$2 = createCommonjsModule(function (module, exports) {
(function (global, factory) {
  {
    factory(exports, React__default, propTypes, require$$2);
  }
})(commonjsGlobal, function (exports, _react, _propTypes) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Plane = undefined;

  var _react2 = _interopRequireDefault(_react);

  var _propTypes2 = _interopRequireDefault(_propTypes);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  var Plane = exports.Plane = function Plane(props) {
    return _react2.default.createElement(
      "svg",
      { className: "react-spinner-loader-svg-calLoader", xmlns: "http://www.w3.org/2000/svg", width: "230", height: "230" },
      _react2.default.createElement("path", {
        className: "react-spinner-loader-cal-loader__path",
        style: { stroke: props.secondaryColor },
        d: "M86.429 40c63.616-20.04 101.511 25.08 107.265 61.93 6.487 41.54-18.593 76.99-50.6 87.643-59.46 19.791-101.262-23.577-107.142-62.616C29.398 83.441 59.945 48.343 86.43 40z",
        fill: "none",
        stroke: "#0099cc",
        strokeWidth: "4",
        strokeLinecap: "round",
        strokeLinejoin: "round",
        strokeDasharray: "10 10 10 10 10 10 10 432",
        strokeDashoffset: "77"
      }),
      _react2.default.createElement("path", {
        className: "cal-loader__plane",
        style: { fill: props.color },
        d: "M141.493 37.93c-1.087-.927-2.942-2.002-4.32-2.501-2.259-.824-3.252-.955-9.293-1.172-4.017-.146-5.197-.23-5.47-.37-.766-.407-1.526-1.448-7.114-9.773-4.8-7.145-5.344-7.914-6.327-8.976-1.214-1.306-1.396-1.378-3.79-1.473-1.036-.04-2-.043-2.153-.002-.353.1-.87.586-1 .952-.139.399-.076.71.431 2.22.241.72 1.029 3.386 1.742 5.918 1.644 5.844 2.378 8.343 2.863 9.705.206.601.33 1.1.275 1.125-.24.097-10.56 1.066-11.014 1.032a3.532 3.532 0 0 1-1.002-.276l-.487-.246-2.044-2.613c-2.234-2.87-2.228-2.864-3.35-3.309-.717-.287-2.82-.386-3.276-.163-.457.237-.727.644-.737 1.152-.018.39.167.805 1.916 4.373 1.06 2.166 1.964 4.083 1.998 4.27.04.179.004.521-.076.75-.093.228-1.109 2.064-2.269 4.088-1.921 3.34-2.11 3.711-2.123 4.107-.008.25.061.557.168.725.328.512.72.644 1.966.676 1.32.029 2.352-.236 3.05-.762.222-.171 1.275-1.313 2.412-2.611 1.918-2.185 2.048-2.32 2.45-2.505.241-.111.601-.232.82-.271.267-.058 2.213.201 5.912.8 3.036.48 5.525.894 5.518.914 0 .026-.121.306-.27.638-.54 1.198-1.515 3.842-3.35 9.021-1.029 2.913-2.107 5.897-2.4 6.62-.703 1.748-.725 1.833-.594 2.286.137.46.45.833.872 1.012.41.177 3.823.24 4.37.085.852-.25 1.44-.688 2.312-1.724 1.166-1.39 3.169-3.948 6.771-8.661 5.8-7.583 6.561-8.49 7.387-8.702.233-.065 2.828-.056 5.784.011 5.827.138 6.64.09 8.62-.5 2.24-.67 4.035-1.65 5.517-3.016 1.136-1.054 1.135-1.014.207-1.962-.357-.38-.767-.777-.902-.893z",
        fill: "#000033"
      })
    );
  };

  Plane.propTypes = {
    secondaryColor: _propTypes2.default.string,
    color: _propTypes2.default.string
  };

  Plane.defaultProps = {
    secondaryColor: "grey",
    color: "#FFA500"
  };
});
});

unwrapExports(Plane$2);

var MutatingDot = createCommonjsModule(function (module, exports) {
(function (global, factory) {
  {
    factory(exports, React__default);
  }
})(commonjsGlobal, function (exports, _react) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.MutatingDot = undefined;

  var _react2 = _interopRequireDefault(_react);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  var MutatingDot = exports.MutatingDot = function MutatingDot(svg) {
    return _react2.default.createElement(
      "svg",
      { id: "goo-loader", width: svg.width, height: svg.height },
      _react2.default.createElement(
        "filter",
        { id: "fancy-goo" },
        _react2.default.createElement("feGaussianBlur", { "in": "SourceGraphic", stdDeviation: "6", result: "blur" }),
        _react2.default.createElement("feColorMatrix", {
          "in": "blur",
          mode: "matrix",
          values: "1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9",
          result: "goo"
        }),
        _react2.default.createElement("feComposite", { "in": "SourceGraphic", in2: "goo", operator: "atop" })
      ),
      _react2.default.createElement(
        "g",
        { filter: "url(#fancy-goo)" },
        _react2.default.createElement("animateTransform", {
          id: "mainAnim",
          attributeName: "transform",
          attributeType: "XML",
          type: "rotate",
          from: "0 50 50",
          to: "359 50 50",
          dur: "1.2s",
          repeatCount: "indefinite"
        }),
        _react2.default.createElement(
          "circle",
          { cx: "50%", cy: "40", r: "11" },
          _react2.default.createElement("animate", {
            id: "cAnim1",
            attributeType: "XML",
            attributeName: "cy",
            dur: "0.6s",
            begin: "0;cAnim1.end+0.2s",
            calcMode: "spline",
            values: "40;20;40",
            keyTimes: "0;0.3;1",
            keySplines: "0.175, 0.885, 0.320, 1.5; 0.175, 0.885, 0.320, 1.5"
          })
        ),
        _react2.default.createElement(
          "circle",
          { cx: "50%", cy: "60", r: "11" },
          _react2.default.createElement("animate", {
            id: "cAnim2",
            attributeType: "XML",
            attributeName: "cy",
            dur: "0.6s",
            begin: "0.4s;cAnim2.end+0.2s",
            calcMode: "spline",
            values: "60;80;60",
            keyTimes: "0;0.3;1",
            keySplines: "0.175, 0.885, 0.320, 1.5;0.175, 0.885, 0.320, 1.5"
          })
        )
      )
    );
  };
});
});

unwrapExports(MutatingDot);

var dist = createCommonjsModule(function (module, exports) {
(function (global, factory) {
  {
    factory(exports, React__default, propTypes, Audio, BallTriangle, Bars, Circles, Grid, Hearts, Oval, Puff, Rings, TailSpin, ThreeDots, Watch, RevolvingDot, CradleLoader$2, Triangle$2, Plane$2, MutatingDot);
  }
})(commonjsGlobal, function (exports, _react, _propTypes, _Audio, _BallTriangle, _Bars, _Circles, _Grid, _Hearts, _Oval, _Puff, _Rings, _TailSpin, _ThreeDots, _Watch, _RevolvingDot, _CradleLoader, _Triangle, _Plane, _MutatingDot) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  var _react2 = _interopRequireDefault(_react);

  var _propTypes2 = _interopRequireDefault(_propTypes);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  var Loader = function (_React$Component) {
    _inherits(Loader, _React$Component);

    function Loader() {
      var _ref;

      var _temp, _this, _ret;

      _classCallCheck(this, Loader);

      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Loader.__proto__ || Object.getPrototypeOf(Loader)).call.apply(_ref, [this].concat(args))), _this), _this.svgRenderer = function (type) {
        switch (type) {
          case "Audio":
            return _react2.default.createElement(_Audio.Audio, _this.props);
          case "Ball-Triangle":
            return _react2.default.createElement(_BallTriangle.BallTriangle, _this.props);
          case "Bars":
            return _react2.default.createElement(_Bars.Bars, _this.props);
          case "Circles":
            return _react2.default.createElement(_Circles.Circles, _this.props);
          case "Grid":
            return _react2.default.createElement(_Grid.Grid, _this.props);
          case "Hearts":
            return _react2.default.createElement(_Hearts.Hearts, _this.props);
          case "Oval":
            return _react2.default.createElement(_Oval.Oval, _this.props);
          case "Puff":
            return _react2.default.createElement(_Puff.Puff, _this.props);
          case "Rings":
            return _react2.default.createElement(_Rings.Rings, _this.props);
          case "TailSpin":
            return _react2.default.createElement(_TailSpin.TailSpin, _this.props);
          case "ThreeDots":
            return _react2.default.createElement(_ThreeDots.ThreeDots, _this.props);
          case "Watch":
            return _react2.default.createElement(_Watch.Watch, _this.props);
          case "RevolvingDot":
            return _react2.default.createElement(_RevolvingDot.RevolvingDot, _this.props);
          case "CradleLoader":
            return _react2.default.createElement(_CradleLoader.CradleLoader, _this.props);
          case "Triangle":
            return _react2.default.createElement(_Triangle.Triangle, _this.props);
          case "Plane":
            return _react2.default.createElement(_Plane.Plane, _this.props);
          case "MutatingDot":
            return _react2.default.createElement(_MutatingDot.MutatingDot, _this.props);
          default:
            return _react2.default.createElement(
              "div",
              null,
              _react2.default.createElement(
                "span",
                { style: { color: "Green" } },
                "LOADING"
              ),
              _react2.default.createElement(
                "small",
                null,
                _react2.default.createElement(
                  "i",
                  null,
                  _react2.default.createElement("br", null),
                  "Note:No specific svg type exist"
                )
              )
            );
        }
      }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    _createClass(Loader, [{
      key: "render",
      value: function render$$1() {
        var type = this.props.type;

        return _react2.default.createElement(
          "div",
          { className: this.props.style },
          this.svgRenderer(type)
        );
      }
    }]);

    return Loader;
  }(_react2.default.Component);

  Loader.propTypes = {
    color: _propTypes2.default.string,
    type: _propTypes2.default.string,
    height: _propTypes2.default.oneOfType([_propTypes2.default.number, _propTypes2.default.string]),
    width: _propTypes2.default.oneOfType([_propTypes2.default.number, _propTypes2.default.string]),
    style: _propTypes2.default.shape({})
  };
  Loader.defaultProps = {
    color: "#71238",
    type: "Audio",
    height: 80,
    width: 80
  };
  exports.default = Loader;
});
});

unwrapExports(dist);

var reactLoaderSpinner = dist;

var RollerIcon = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAMAAAAMCGV4AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAArlBMVEUAAAAA//8B4fkC4foC4foC4foD4foA4/8E4fsC4foB4PsD4PoA6v8C4PoD4foC4foB4voD4foA2/8C4vsC4fsC4fsC4foA4/YC4foA3/kD4foA4PoAzP8A4PsD4foA4fsB4foB4vsC4foA4/8C4foB4fsC4foA4vkA3v8A4foC4fkA3P8C4voC4foB4fkC4foC4PoC4fsC4fkA3/cC4foA4fkC4voC4PoC4foAAACaNskHAAAAOXRSTlMAAbTf3d7GCUTOr2QMnsmpv8oHe3d42xvgKGYyBTrLO7qtbhKksG0sFzPmFmjwtWfReYggzytx6nCcqdN4AAAAAWJLR0QAiAUdSAAAAAlwSFlzAAAN1wAADdcBQiibeAAAAAd0SU1FB+IMGg0nLxTAVrwAAABzSURBVAjXY2BkYmaBAlY2dgYGDk4GOODi5mHg5UPw+QUEGYSYhUWgQFRMXIKBQZJbCgakGSQYkICMrLAcjM0oL6wgoaikDOOrqKqpM8JVamhqaWoj6dTR1dPVRzbKwNCAAYVvhMY3RuWbmHKi8M3MLSAMADmpCWqrIUfgAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE4LTEyLTI2VDEyOjM5OjQ3KzAxOjAwNLHzvAAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxOC0xMi0yNlQxMjozOTo0NyswMTowMEXsSwAAAAAZdEVYdFNvZnR3YXJlAHd3dy5pbmtzY2FwZS5vcmeb7jwaAAAAAElFTkSuQmCC";

var css$7 = ".Companion_companion__sGKsL {\n  position: absolute;\n  background-color: rgba(0, 0, 0, 0.7);\n  border-radius: 50%;\n  width: 30px;\n  height: 30px;\n  cursor: pointer;\n}\n\n.Companion_companion__sGKsL img {\n  margin: 8px;\n}\n\n.Companion_companionsTitle__1wmyr {\n  margin-bottom: 15px;\n}\n\n.Companion_companionsContainer__1_AOP {\n  max-height: 200px;\n  overflow: auto;\n  padding: 15px 20px;\n  position: relative;\n}\n\n.Companion_companionsContainer__1_AOP img {\n  width: 32px;\n  height: 32px;\n  cursor: pointer;\n}\n\n.Companion_companionsContainer__1_AOP img {\n  margin-right: 10px;\n}\n\n.Companion_companionTooltip__tOGN2 {\n  background-color: #FFF !important;\n  opacity: 1 !important;\n  font-weight: bold;\n  font-size: 13px;\n  color: inherit !important;\n  padding: 0;\n  min-width: 240px;\n  max-width: 240px;\n  pointer-events: all;\n}\n\n.Companion_companionTooltip__tOGN2::after {\n  border-bottom-color: #fff !important;\n}\n\n.Companion_close__2yoc5 {\n  position: absolute;\n  cursor: pointer;\n  right: 15px;\n  top: 15px;\n  width: 16px;\n  height: 16px;\n  opacity: 0.3;\n}\n.Companion_close__2yoc5:hover {\n  opacity: 1;\n}\n.Companion_close__2yoc5:before, .Companion_close__2yoc5:after {\n  position: absolute;\n  left: 15px;\n  content: ' ';\n  height: 16px;\n  width: 2px;\n  background-color: #333;\n}\n.Companion_close__2yoc5:before {\n  transform: rotate(45deg);\n}\n.Companion_close__2yoc5:after {\n  transform: rotate(-45deg);\n}\n";
var styles$3 = { "companion": "Companion_companion__sGKsL", "companionsTitle": "Companion_companionsTitle__1wmyr", "companionsContainer": "Companion_companionsContainer__1_AOP", "companionTooltip": "Companion_companionTooltip__tOGN2", "close": "Companion_close__2yoc5" };
styleInject(css$7);

var classnames = createCommonjsModule(function (module) {
/*!
  Copyright (c) 2017 Jed Watson.
  Licensed under the MIT License (MIT), see
  http://jedwatson.github.io/classnames
*/
/* global define */

(function () {

	var hasOwn = {}.hasOwnProperty;

	function classNames () {
		var classes = [];

		for (var i = 0; i < arguments.length; i++) {
			var arg = arguments[i];
			if (!arg) continue;

			var argType = typeof arg;

			if (argType === 'string' || argType === 'number') {
				classes.push(arg);
			} else if (Array.isArray(arg) && arg.length) {
				var inner = classNames.apply(null, arg);
				if (inner) {
					classes.push(inner);
				}
			} else if (argType === 'object') {
				for (var key in arg) {
					if (hasOwn.call(arg, key) && arg[key]) {
						classes.push(key);
					}
				}
			}
		}

		return classes.join(' ');
	}

	if (module.exports) {
		classNames.default = classNames;
		module.exports = classNames;
	} else {
		window.classNames = classNames;
	}
}());
});

var constant = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {

  GLOBAL: {
    HIDE: '__react_tooltip_hide_event',
    REBUILD: '__react_tooltip_rebuild_event',
    SHOW: '__react_tooltip_show_event'
  }
};
});

unwrapExports(constant);

var staticMethods = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (target) {
  /**
   * Hide all tooltip
   * @trigger ReactTooltip.hide()
   */
  target.hide = function (target) {
    dispatchGlobalEvent(_constant2.default.GLOBAL.HIDE, { target: target });
  };

  /**
   * Rebuild all tooltip
   * @trigger ReactTooltip.rebuild()
   */
  target.rebuild = function () {
    dispatchGlobalEvent(_constant2.default.GLOBAL.REBUILD);
  };

  /**
   * Show specific tooltip
   * @trigger ReactTooltip.show()
   */
  target.show = function (target) {
    dispatchGlobalEvent(_constant2.default.GLOBAL.SHOW, { target: target });
  };

  target.prototype.globalRebuild = function () {
    if (this.mount) {
      this.unbindListener();
      this.bindListener();
    }
  };

  target.prototype.globalShow = function (event) {
    if (this.mount) {
      // Create a fake event, specific show will limit the type to `solid`
      // only `float` type cares e.clientX e.clientY
      var e = { currentTarget: event.detail.target };
      this.showTooltip(e, true);
    }
  };

  target.prototype.globalHide = function (event) {
    if (this.mount) {
      var hasTarget = event && event.detail && event.detail.target && true || false;
      this.hideTooltip({ currentTarget: hasTarget && event.detail.target }, hasTarget);
    }
  };
};



var _constant2 = _interopRequireDefault(constant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var dispatchGlobalEvent = function dispatchGlobalEvent(eventName, opts) {
  // Compatibale with IE
  // @see http://stackoverflow.com/questions/26596123/internet-explorer-9-10-11-event-constructor-doesnt-work
  var event = void 0;

  if (typeof window.CustomEvent === 'function') {
    event = new window.CustomEvent(eventName, { detail: opts });
  } else {
    event = document.createEvent('Event');
    event.initEvent(eventName, false, true);
    event.detail = opts;
  }

  window.dispatchEvent(event);
}; /**
    * Static methods for react-tooltip
    */
});

unwrapExports(staticMethods);

var windowListener = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (target) {
  target.prototype.bindWindowEvents = function (resizeHide) {
    // ReactTooltip.hide
    window.removeEventListener(_constant2.default.GLOBAL.HIDE, this.globalHide);
    window.addEventListener(_constant2.default.GLOBAL.HIDE, this.globalHide, false);

    // ReactTooltip.rebuild
    window.removeEventListener(_constant2.default.GLOBAL.REBUILD, this.globalRebuild);
    window.addEventListener(_constant2.default.GLOBAL.REBUILD, this.globalRebuild, false);

    // ReactTooltip.show
    window.removeEventListener(_constant2.default.GLOBAL.SHOW, this.globalShow);
    window.addEventListener(_constant2.default.GLOBAL.SHOW, this.globalShow, false);

    // Resize
    if (resizeHide) {
      window.removeEventListener('resize', this.onWindowResize);
      window.addEventListener('resize', this.onWindowResize, false);
    }
  };

  target.prototype.unbindWindowEvents = function () {
    window.removeEventListener(_constant2.default.GLOBAL.HIDE, this.globalHide);
    window.removeEventListener(_constant2.default.GLOBAL.REBUILD, this.globalRebuild);
    window.removeEventListener(_constant2.default.GLOBAL.SHOW, this.globalShow);
    window.removeEventListener('resize', this.onWindowResize);
  };

  /**
   * invoked by resize event of window
   */
  target.prototype.onWindowResize = function () {
    if (!this.mount) return;
    this.hideTooltip();
  };
};



var _constant2 = _interopRequireDefault(constant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
});

unwrapExports(windowListener);

var customEvent = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (target) {
  target.prototype.isCustomEvent = function (ele) {
    var event = this.state.event;

    return event || !!ele.getAttribute('data-event');
  };

  /* Bind listener for custom event */
  target.prototype.customBindListener = function (ele) {
    var _this = this;

    var _state = this.state,
        event = _state.event,
        eventOff = _state.eventOff;

    var dataEvent = ele.getAttribute('data-event') || event;
    var dataEventOff = ele.getAttribute('data-event-off') || eventOff;

    dataEvent.split(' ').forEach(function (event) {
      ele.removeEventListener(event, customListeners.get(ele, event));
      var customListener = checkStatus.bind(_this, dataEventOff);
      customListeners.set(ele, event, customListener);
      ele.addEventListener(event, customListener, false);
    });
    if (dataEventOff) {
      dataEventOff.split(' ').forEach(function (event) {
        ele.removeEventListener(event, _this.hideTooltip);
        ele.addEventListener(event, _this.hideTooltip, false);
      });
    }
  };

  /* Unbind listener for custom event */
  target.prototype.customUnbindListener = function (ele) {
    var _state2 = this.state,
        event = _state2.event,
        eventOff = _state2.eventOff;

    var dataEvent = event || ele.getAttribute('data-event');
    var dataEventOff = eventOff || ele.getAttribute('data-event-off');

    ele.removeEventListener(dataEvent, customListeners.get(ele, event));
    if (dataEventOff) ele.removeEventListener(dataEventOff, this.hideTooltip);
  };
};

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Custom events to control showing and hiding of tooltip
 *
 * @attributes
 * - `event` {String}
 * - `eventOff` {String}
 */

var checkStatus = function checkStatus(dataEventOff, e) {
  var show = this.state.show;
  var id = this.props.id;

  var dataIsCapture = e.currentTarget.getAttribute('data-iscapture');
  var isCapture = dataIsCapture && dataIsCapture === 'true' || this.props.isCapture;
  var currentItem = e.currentTarget.getAttribute('currentItem');

  if (!isCapture) e.stopPropagation();
  if (show && currentItem === 'true') {
    if (!dataEventOff) this.hideTooltip(e);
  } else {
    e.currentTarget.setAttribute('currentItem', 'true');
    setUntargetItems(e.currentTarget, this.getTargetArray(id));
    this.showTooltip(e);
  }
};

var setUntargetItems = function setUntargetItems(currentTarget, targetArray) {
  for (var i = 0; i < targetArray.length; i++) {
    if (currentTarget !== targetArray[i]) {
      targetArray[i].setAttribute('currentItem', 'false');
    } else {
      targetArray[i].setAttribute('currentItem', 'true');
    }
  }
};

var customListeners = {
  id: '9b69f92e-d3fe-498b-b1b4-c5e63a51b0cf',
  set: function set(target, event, listener) {
    if (this.id in target) {
      var map = target[this.id];
      map[event] = listener;
    } else {
      // this is workaround for WeakMap, which is not supported in older browsers, such as IE
      Object.defineProperty(target, this.id, {
        configurable: true,
        value: _defineProperty({}, event, listener)
      });
    }
  },
  get: function get(target, event) {
    var map = target[this.id];
    if (map !== undefined) {
      return map[event];
    }
  }
};
});

unwrapExports(customEvent);

var isCapture = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (target) {
  target.prototype.isCapture = function (currentTarget) {
    return currentTarget && currentTarget.getAttribute('data-iscapture') === 'true' || this.props.isCapture || false;
  };
};
});

unwrapExports(isCapture);

var getEffect = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (target) {
  target.prototype.getEffect = function (currentTarget) {
    var dataEffect = currentTarget.getAttribute('data-effect');
    return dataEffect || this.props.effect || 'float';
  };
};
});

unwrapExports(getEffect);

var trackRemoval = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (target) {
  target.prototype.bindRemovalTracker = function () {
    var _this = this;

    var MutationObserver = getMutationObserverClass();
    if (MutationObserver == null) return;

    var observer = new MutationObserver(function (mutations) {
      for (var m1 = 0; m1 < mutations.length; m1++) {
        var mutation = mutations[m1];
        for (var m2 = 0; m2 < mutation.removedNodes.length; m2++) {
          var element = mutation.removedNodes[m2];
          if (element === _this.state.currentTarget) {
            _this.hideTooltip();
            return;
          }
        }
      }
    });

    observer.observe(window.document, { childList: true, subtree: true });

    this.removalTracker = observer;
  };

  target.prototype.unbindRemovalTracker = function () {
    if (this.removalTracker) {
      this.removalTracker.disconnect();
      this.removalTracker = null;
    }
  };
};

/**
 * Tracking target removing from DOM.
 * It's nessesary to hide tooltip when it's target disappears.
 * Otherwise, the tooltip would be shown forever until another target
 * is triggered.
 *
 * If MutationObserver is not available, this feature just doesn't work.
 */

// https://hacks.mozilla.org/2012/05/dom-mutationobserver-reacting-to-dom-changes-without-killing-browser-performance/
var getMutationObserverClass = function getMutationObserverClass() {
  return window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
};
});

unwrapExports(trackRemoval);

var getPosition = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (e, target, node, place, desiredPlace, effect, offset) {
  var _getDimensions = getDimensions(node),
      tipWidth = _getDimensions.width,
      tipHeight = _getDimensions.height;

  var _getDimensions2 = getDimensions(target),
      targetWidth = _getDimensions2.width,
      targetHeight = _getDimensions2.height;

  var _getCurrentOffset = getCurrentOffset(e, target, effect),
      mouseX = _getCurrentOffset.mouseX,
      mouseY = _getCurrentOffset.mouseY;

  var defaultOffset = getDefaultPosition(effect, targetWidth, targetHeight, tipWidth, tipHeight);

  var _calculateOffset = calculateOffset(offset),
      extraOffset_X = _calculateOffset.extraOffset_X,
      extraOffset_Y = _calculateOffset.extraOffset_Y;

  var windowWidth = window.innerWidth;
  var windowHeight = window.innerHeight;

  var _getParent = getParent(node),
      parentTop = _getParent.parentTop,
      parentLeft = _getParent.parentLeft;

  // Get the edge offset of the tooltip


  var getTipOffsetLeft = function getTipOffsetLeft(place) {
    var offset_X = defaultOffset[place].l;
    return mouseX + offset_X + extraOffset_X;
  };
  var getTipOffsetRight = function getTipOffsetRight(place) {
    var offset_X = defaultOffset[place].r;
    return mouseX + offset_X + extraOffset_X;
  };
  var getTipOffsetTop = function getTipOffsetTop(place) {
    var offset_Y = defaultOffset[place].t;
    return mouseY + offset_Y + extraOffset_Y;
  };
  var getTipOffsetBottom = function getTipOffsetBottom(place) {
    var offset_Y = defaultOffset[place].b;
    return mouseY + offset_Y + extraOffset_Y;
  };

  //
  // Functions to test whether the tooltip's sides are inside
  // the client window for a given orientation p
  //
  //  _____________
  // |             | <-- Right side
  // | p = 'left'  |\
  // |             |/  |\
  // |_____________|   |_\  <-- Mouse
  //      / \           |
  //       |
  //       |
  //  Bottom side
  //
  var outsideLeft = function outsideLeft(p) {
    return getTipOffsetLeft(p) < 0;
  };
  var outsideRight = function outsideRight(p) {
    return getTipOffsetRight(p) > windowWidth;
  };
  var outsideTop = function outsideTop(p) {
    return getTipOffsetTop(p) < 0;
  };
  var outsideBottom = function outsideBottom(p) {
    return getTipOffsetBottom(p) > windowHeight;
  };

  // Check whether the tooltip with orientation p is completely inside the client window
  var outside = function outside(p) {
    return outsideLeft(p) || outsideRight(p) || outsideTop(p) || outsideBottom(p);
  };
  var inside = function inside(p) {
    return !outside(p);
  };

  var placesList = ['top', 'bottom', 'left', 'right'];
  var insideList = [];
  for (var i = 0; i < 4; i++) {
    var p = placesList[i];
    if (inside(p)) {
      insideList.push(p);
    }
  }

  var isNewState = false;
  var newPlace = void 0;
  if (inside(desiredPlace) && desiredPlace !== place) {
    isNewState = true;
    newPlace = desiredPlace;
  } else if (insideList.length > 0 && outside(desiredPlace) && outside(place)) {
    isNewState = true;
    newPlace = insideList[0];
  }

  if (isNewState) {
    return {
      isNewState: true,
      newState: { place: newPlace }
    };
  }

  return {
    isNewState: false,
    position: {
      left: parseInt(getTipOffsetLeft(place) - parentLeft, 10),
      top: parseInt(getTipOffsetTop(place) - parentTop, 10)
    }
  };
};

var getDimensions = function getDimensions(node) {
  var _node$getBoundingClie = node.getBoundingClientRect(),
      height = _node$getBoundingClie.height,
      width = _node$getBoundingClie.width;

  return {
    height: parseInt(height, 10),
    width: parseInt(width, 10)
  };
};

// Get current mouse offset
/**
 * Calculate the position of tooltip
 *
 * @params
 * - `e` {Event} the event of current mouse
 * - `target` {Element} the currentTarget of the event
 * - `node` {DOM} the react-tooltip object
 * - `place` {String} top / right / bottom / left
 * - `effect` {String} float / solid
 * - `offset` {Object} the offset to default position
 *
 * @return {Object}
 * - `isNewState` {Bool} required
 * - `newState` {Object}
 * - `position` {Object} {left: {Number}, top: {Number}}
 */
var getCurrentOffset = function getCurrentOffset(e, currentTarget, effect) {
  var boundingClientRect = currentTarget.getBoundingClientRect();
  var targetTop = boundingClientRect.top;
  var targetLeft = boundingClientRect.left;

  var _getDimensions3 = getDimensions(currentTarget),
      targetWidth = _getDimensions3.width,
      targetHeight = _getDimensions3.height;

  if (effect === 'float') {
    return {
      mouseX: e.clientX,
      mouseY: e.clientY
    };
  }
  return {
    mouseX: targetLeft + targetWidth / 2,
    mouseY: targetTop + targetHeight / 2
  };
};

// List all possibility of tooltip final offset
// This is useful in judging if it is necessary for tooltip to switch position when out of window
var getDefaultPosition = function getDefaultPosition(effect, targetWidth, targetHeight, tipWidth, tipHeight) {
  var top = void 0;
  var right = void 0;
  var bottom = void 0;
  var left = void 0;
  var disToMouse = 3;
  var triangleHeight = 2;
  var cursorHeight = 12; // Optimize for float bottom only, cause the cursor will hide the tooltip

  if (effect === 'float') {
    top = {
      l: -(tipWidth / 2),
      r: tipWidth / 2,
      t: -(tipHeight + disToMouse + triangleHeight),
      b: -disToMouse
    };
    bottom = {
      l: -(tipWidth / 2),
      r: tipWidth / 2,
      t: disToMouse + cursorHeight,
      b: tipHeight + disToMouse + triangleHeight + cursorHeight
    };
    left = {
      l: -(tipWidth + disToMouse + triangleHeight),
      r: -disToMouse,
      t: -(tipHeight / 2),
      b: tipHeight / 2
    };
    right = {
      l: disToMouse,
      r: tipWidth + disToMouse + triangleHeight,
      t: -(tipHeight / 2),
      b: tipHeight / 2
    };
  } else if (effect === 'solid') {
    top = {
      l: -(tipWidth / 2),
      r: tipWidth / 2,
      t: -(targetHeight / 2 + tipHeight + triangleHeight),
      b: -(targetHeight / 2)
    };
    bottom = {
      l: -(tipWidth / 2),
      r: tipWidth / 2,
      t: targetHeight / 2,
      b: targetHeight / 2 + tipHeight + triangleHeight
    };
    left = {
      l: -(tipWidth + targetWidth / 2 + triangleHeight),
      r: -(targetWidth / 2),
      t: -(tipHeight / 2),
      b: tipHeight / 2
    };
    right = {
      l: targetWidth / 2,
      r: tipWidth + targetWidth / 2 + triangleHeight,
      t: -(tipHeight / 2),
      b: tipHeight / 2
    };
  }

  return { top: top, bottom: bottom, left: left, right: right };
};

// Consider additional offset into position calculation
var calculateOffset = function calculateOffset(offset) {
  var extraOffset_X = 0;
  var extraOffset_Y = 0;

  if (Object.prototype.toString.apply(offset) === '[object String]') {
    offset = JSON.parse(offset.toString().replace(/\'/g, '\"'));
  }
  for (var key in offset) {
    if (key === 'top') {
      extraOffset_Y -= parseInt(offset[key], 10);
    } else if (key === 'bottom') {
      extraOffset_Y += parseInt(offset[key], 10);
    } else if (key === 'left') {
      extraOffset_X -= parseInt(offset[key], 10);
    } else if (key === 'right') {
      extraOffset_X += parseInt(offset[key], 10);
    }
  }

  return { extraOffset_X: extraOffset_X, extraOffset_Y: extraOffset_Y };
};

// Get the offset of the parent elements
var getParent = function getParent(currentTarget) {
  var currentParent = currentTarget;
  while (currentParent) {
    if (window.getComputedStyle(currentParent).getPropertyValue('transform') !== 'none') break;
    currentParent = currentParent.parentElement;
  }

  var parentTop = currentParent && currentParent.getBoundingClientRect().top || 0;
  var parentLeft = currentParent && currentParent.getBoundingClientRect().left || 0;

  return { parentTop: parentTop, parentLeft: parentLeft };
};
});

unwrapExports(getPosition);

var getTipContent = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (tip, children, getContent, multiline) {
  if (children) return children;
  if (getContent !== undefined && getContent !== null) return getContent; // getContent can be 0, '', etc.
  if (getContent === null) return null; // Tip not exist and childern is null or undefined

  var regexp = /<br\s*\/?>/;
  if (!multiline || multiline === 'false' || !regexp.test(tip)) {
    // No trim(), so that user can keep their input
    return tip;
  }

  // Multiline tooltip content
  return tip.split(regexp).map(function (d, i) {
    return _react2.default.createElement(
      'span',
      { key: i, className: 'multi-line' },
      d
    );
  });
};



var _react2 = _interopRequireDefault(React__default);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
});

unwrapExports(getTipContent);

var aria = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.parseAria = parseAria;
/**
 * Support aria- and role in ReactTooltip
 *
 * @params props {Object}
 * @return {Object}
 */
function parseAria(props) {
  var ariaObj = {};
  Object.keys(props).filter(function (prop) {
    // aria-xxx and role is acceptable
    return (/(^aria-\w+$|^role$)/.test(prop)
    );
  }).forEach(function (prop) {
    ariaObj[prop] = props[prop];
  });

  return ariaObj;
}
});

unwrapExports(aria);
var aria_1 = aria.parseAria;

var nodeListToArray = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (nodeList) {
  var length = nodeList.length;
  if (nodeList.hasOwnProperty) {
    return Array.prototype.slice.call(nodeList);
  }
  return new Array(length).fill().map(function (index) {
    return nodeList[index];
  });
};
});

unwrapExports(nodeListToArray);

var style$1 = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = '.__react_component_tooltip{border-radius:3px;display:inline-block;font-size:13px;left:-999em;opacity:0;padding:8px 21px;position:fixed;pointer-events:none;transition:opacity 0.3s ease-out;top:-999em;visibility:hidden;z-index:999}.__react_component_tooltip.allow_hover{pointer-events:auto}.__react_component_tooltip:before,.__react_component_tooltip:after{content:"";width:0;height:0;position:absolute}.__react_component_tooltip.show{opacity:0.9;margin-top:0px;margin-left:0px;visibility:visible}.__react_component_tooltip.type-dark{color:#fff;background-color:#222}.__react_component_tooltip.type-dark.place-top:after{border-top-color:#222;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-dark.place-bottom:after{border-bottom-color:#222;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-dark.place-left:after{border-left-color:#222;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-dark.place-right:after{border-right-color:#222;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-dark.border{border:1px solid #fff}.__react_component_tooltip.type-dark.border.place-top:before{border-top:8px solid #fff}.__react_component_tooltip.type-dark.border.place-bottom:before{border-bottom:8px solid #fff}.__react_component_tooltip.type-dark.border.place-left:before{border-left:8px solid #fff}.__react_component_tooltip.type-dark.border.place-right:before{border-right:8px solid #fff}.__react_component_tooltip.type-success{color:#fff;background-color:#8DC572}.__react_component_tooltip.type-success.place-top:after{border-top-color:#8DC572;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-success.place-bottom:after{border-bottom-color:#8DC572;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-success.place-left:after{border-left-color:#8DC572;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-success.place-right:after{border-right-color:#8DC572;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-success.border{border:1px solid #fff}.__react_component_tooltip.type-success.border.place-top:before{border-top:8px solid #fff}.__react_component_tooltip.type-success.border.place-bottom:before{border-bottom:8px solid #fff}.__react_component_tooltip.type-success.border.place-left:before{border-left:8px solid #fff}.__react_component_tooltip.type-success.border.place-right:before{border-right:8px solid #fff}.__react_component_tooltip.type-warning{color:#fff;background-color:#F0AD4E}.__react_component_tooltip.type-warning.place-top:after{border-top-color:#F0AD4E;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-warning.place-bottom:after{border-bottom-color:#F0AD4E;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-warning.place-left:after{border-left-color:#F0AD4E;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-warning.place-right:after{border-right-color:#F0AD4E;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-warning.border{border:1px solid #fff}.__react_component_tooltip.type-warning.border.place-top:before{border-top:8px solid #fff}.__react_component_tooltip.type-warning.border.place-bottom:before{border-bottom:8px solid #fff}.__react_component_tooltip.type-warning.border.place-left:before{border-left:8px solid #fff}.__react_component_tooltip.type-warning.border.place-right:before{border-right:8px solid #fff}.__react_component_tooltip.type-error{color:#fff;background-color:#BE6464}.__react_component_tooltip.type-error.place-top:after{border-top-color:#BE6464;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-error.place-bottom:after{border-bottom-color:#BE6464;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-error.place-left:after{border-left-color:#BE6464;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-error.place-right:after{border-right-color:#BE6464;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-error.border{border:1px solid #fff}.__react_component_tooltip.type-error.border.place-top:before{border-top:8px solid #fff}.__react_component_tooltip.type-error.border.place-bottom:before{border-bottom:8px solid #fff}.__react_component_tooltip.type-error.border.place-left:before{border-left:8px solid #fff}.__react_component_tooltip.type-error.border.place-right:before{border-right:8px solid #fff}.__react_component_tooltip.type-info{color:#fff;background-color:#337AB7}.__react_component_tooltip.type-info.place-top:after{border-top-color:#337AB7;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-info.place-bottom:after{border-bottom-color:#337AB7;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-info.place-left:after{border-left-color:#337AB7;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-info.place-right:after{border-right-color:#337AB7;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-info.border{border:1px solid #fff}.__react_component_tooltip.type-info.border.place-top:before{border-top:8px solid #fff}.__react_component_tooltip.type-info.border.place-bottom:before{border-bottom:8px solid #fff}.__react_component_tooltip.type-info.border.place-left:before{border-left:8px solid #fff}.__react_component_tooltip.type-info.border.place-right:before{border-right:8px solid #fff}.__react_component_tooltip.type-light{color:#222;background-color:#fff}.__react_component_tooltip.type-light.place-top:after{border-top-color:#fff;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-light.place-bottom:after{border-bottom-color:#fff;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-light.place-left:after{border-left-color:#fff;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-light.place-right:after{border-right-color:#fff;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-light.border{border:1px solid #222}.__react_component_tooltip.type-light.border.place-top:before{border-top:8px solid #222}.__react_component_tooltip.type-light.border.place-bottom:before{border-bottom:8px solid #222}.__react_component_tooltip.type-light.border.place-left:before{border-left:8px solid #222}.__react_component_tooltip.type-light.border.place-right:before{border-right:8px solid #222}.__react_component_tooltip.place-top{margin-top:-10px}.__react_component_tooltip.place-top:before{border-left:10px solid transparent;border-right:10px solid transparent;bottom:-8px;left:50%;margin-left:-10px}.__react_component_tooltip.place-top:after{border-left:8px solid transparent;border-right:8px solid transparent;bottom:-6px;left:50%;margin-left:-8px}.__react_component_tooltip.place-bottom{margin-top:10px}.__react_component_tooltip.place-bottom:before{border-left:10px solid transparent;border-right:10px solid transparent;top:-8px;left:50%;margin-left:-10px}.__react_component_tooltip.place-bottom:after{border-left:8px solid transparent;border-right:8px solid transparent;top:-6px;left:50%;margin-left:-8px}.__react_component_tooltip.place-left{margin-left:-10px}.__react_component_tooltip.place-left:before{border-top:6px solid transparent;border-bottom:6px solid transparent;right:-8px;top:50%;margin-top:-5px}.__react_component_tooltip.place-left:after{border-top:5px solid transparent;border-bottom:5px solid transparent;right:-6px;top:50%;margin-top:-4px}.__react_component_tooltip.place-right{margin-left:10px}.__react_component_tooltip.place-right:before{border-top:6px solid transparent;border-bottom:6px solid transparent;left:-8px;top:50%;margin-top:-5px}.__react_component_tooltip.place-right:after{border-top:5px solid transparent;border-bottom:5px solid transparent;left:-6px;top:50%;margin-top:-4px}.__react_component_tooltip .multi-line{display:block;padding:2px 0px;text-align:center}';
});

unwrapExports(style$1);

var dist$1 = createCommonjsModule(function (module) {

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _class2, _temp;

/* Decoraters */


/* Utils */


/* CSS */




var _react2 = _interopRequireDefault(React__default);



var _propTypes2 = _interopRequireDefault(propTypes);



var _reactDom2 = _interopRequireDefault(ReactDOM__default);



var _classnames2 = _interopRequireDefault(classnames);



var _staticMethods2 = _interopRequireDefault(staticMethods);



var _windowListener2 = _interopRequireDefault(windowListener);



var _customEvent2 = _interopRequireDefault(customEvent);



var _isCapture2 = _interopRequireDefault(isCapture);



var _getEffect2 = _interopRequireDefault(getEffect);



var _trackRemoval2 = _interopRequireDefault(trackRemoval);



var _getPosition2 = _interopRequireDefault(getPosition);



var _getTipContent2 = _interopRequireDefault(getTipContent);





var _nodeListToArray2 = _interopRequireDefault(nodeListToArray);



var _style2 = _interopRequireDefault(style$1);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ReactTooltip = (0, _staticMethods2.default)(_class = (0, _windowListener2.default)(_class = (0, _customEvent2.default)(_class = (0, _isCapture2.default)(_class = (0, _getEffect2.default)(_class = (0, _trackRemoval2.default)(_class = (_temp = _class2 = function (_React$Component) {
  _inherits(ReactTooltip, _React$Component);

  function ReactTooltip(props) {
    _classCallCheck(this, ReactTooltip);

    var _this = _possibleConstructorReturn(this, (ReactTooltip.__proto__ || Object.getPrototypeOf(ReactTooltip)).call(this, props));

    _this.state = {
      place: 'top', // Direction of tooltip
      type: 'dark', // Color theme of tooltip
      effect: 'float', // float or fixed
      show: false,
      border: false,
      offset: {},
      extraClass: '',
      html: false,
      delayHide: 0,
      delayShow: 0,
      event: props.event || null,
      eventOff: props.eventOff || null,
      currentEvent: null, // Current mouse event
      currentTarget: null, // Current target of mouse event
      ariaProps: (0, aria.parseAria)(props), // aria- and role attributes
      isEmptyTip: false,
      disable: false,
      originTooltip: null,
      isMultiline: false
    };

    _this.bind(['showTooltip', 'updateTooltip', 'hideTooltip', 'getTooltipContent', 'globalRebuild', 'globalShow', 'globalHide', 'onWindowResize', 'mouseOnToolTip']);

    _this.mount = true;
    _this.delayShowLoop = null;
    _this.delayHideLoop = null;
    _this.delayReshow = null;
    _this.intervalUpdateContent = null;
    return _this;
  }

  /**
   * For unify the bind and unbind listener
   */


  _createClass(ReactTooltip, [{
    key: 'bind',
    value: function bind(methodArray) {
      var _this2 = this;

      methodArray.forEach(function (method) {
        _this2[method] = _this2[method].bind(_this2);
      });
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      var _props = this.props,
          insecure = _props.insecure,
          resizeHide = _props.resizeHide;

      if (insecure) {
        this.setStyleHeader(); // Set the style to the <link>
      }
      this.bindListener(); // Bind listener for tooltip
      this.bindWindowEvents(resizeHide); // Bind global event for static method
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(props) {
      var ariaProps = this.state.ariaProps;

      var newAriaProps = (0, aria.parseAria)(props);

      var isChanged = Object.keys(newAriaProps).some(function (props) {
        return newAriaProps[props] !== ariaProps[props];
      });
      if (isChanged) {
        this.setState({ ariaProps: newAriaProps });
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.mount = false;

      this.clearTimer();

      this.unbindListener();
      this.removeScrollListener();
      this.unbindWindowEvents();
    }

    /**
     * Return if the mouse is on the tooltip.
     * @returns {boolean} true - mouse is on the tooltip
     */

  }, {
    key: 'mouseOnToolTip',
    value: function mouseOnToolTip() {
      var show = this.state.show;


      if (show && this.tooltipRef) {
        /* old IE work around */
        if (!this.tooltipRef.matches) {
          this.tooltipRef.matches = this.tooltipRef.msMatchesSelector;
        }
        return this.tooltipRef.matches(':hover');
      }
      return false;
    }
    /**
     * Pick out corresponded target elements
     */

  }, {
    key: 'getTargetArray',
    value: function getTargetArray(id) {
      var targetArray = void 0;
      if (!id) {
        targetArray = document.querySelectorAll('[data-tip]:not([data-for])');
      } else {
        var escaped = id.replace(/\\/g, '\\\\').replace(/"/g, '\\"');
        targetArray = document.querySelectorAll('[data-tip][data-for="' + escaped + '"]');
      }
      // targetArray is a NodeList, convert it to a real array
      return (0, _nodeListToArray2.default)(targetArray);
    }

    /**
     * Bind listener to the target elements
     * These listeners used to trigger showing or hiding the tooltip
     */

  }, {
    key: 'bindListener',
    value: function bindListener() {
      var _this3 = this;

      var _props2 = this.props,
          id = _props2.id,
          globalEventOff = _props2.globalEventOff;

      var targetArray = this.getTargetArray(id);

      targetArray.forEach(function (target) {
        var isCaptureMode = _this3.isCapture(target);
        var effect = _this3.getEffect(target);
        if (target.getAttribute('currentItem') === null) {
          target.setAttribute('currentItem', 'false');
        }
        _this3.unbindBasicListener(target);

        if (_this3.isCustomEvent(target)) {
          _this3.customBindListener(target);
          return;
        }

        target.addEventListener('mouseenter', _this3.showTooltip, isCaptureMode);
        if (effect === 'float') {
          target.addEventListener('mousemove', _this3.updateTooltip, isCaptureMode);
        }
        target.addEventListener('mouseleave', _this3.hideTooltip, isCaptureMode);
      });

      // Global event to hide tooltip
      if (globalEventOff) {
        window.removeEventListener(globalEventOff, this.hideTooltip);
        window.addEventListener(globalEventOff, this.hideTooltip, false);
      }

      // Track removal of targetArray elements from DOM
      this.bindRemovalTracker();
    }

    /**
     * Unbind listeners on target elements
     */

  }, {
    key: 'unbindListener',
    value: function unbindListener() {
      var _this4 = this;

      var _props3 = this.props,
          id = _props3.id,
          globalEventOff = _props3.globalEventOff;

      var targetArray = this.getTargetArray(id);
      targetArray.forEach(function (target) {
        _this4.unbindBasicListener(target);
        if (_this4.isCustomEvent(target)) _this4.customUnbindListener(target);
      });

      if (globalEventOff) window.removeEventListener(globalEventOff, this.hideTooltip);
      this.unbindRemovalTracker();
    }

    /**
     * Invoke this before bind listener and ummount the compont
     * it is necessary to invloke this even when binding custom event
     * so that the tooltip can switch between custom and default listener
     */

  }, {
    key: 'unbindBasicListener',
    value: function unbindBasicListener(target) {
      var isCaptureMode = this.isCapture(target);
      target.removeEventListener('mouseenter', this.showTooltip, isCaptureMode);
      target.removeEventListener('mousemove', this.updateTooltip, isCaptureMode);
      target.removeEventListener('mouseleave', this.hideTooltip, isCaptureMode);
    }
  }, {
    key: 'getTooltipContent',
    value: function getTooltipContent() {
      var _props4 = this.props,
          getContent = _props4.getContent,
          children = _props4.children;

      // Generate tooltip content

      var content = void 0;
      if (getContent) {
        if (Array.isArray(getContent)) {
          content = getContent[0] && getContent[0](this.state.originTooltip);
        } else {
          content = getContent(this.state.originTooltip);
        }
      }

      return (0, _getTipContent2.default)(this.state.originTooltip, children, content, this.state.isMultiline);
    }
  }, {
    key: 'isEmptyTip',
    value: function isEmptyTip(placeholder) {
      return typeof placeholder === 'string' && placeholder === '' || placeholder === null;
    }

    /**
     * When mouse enter, show the tooltip
     */

  }, {
    key: 'showTooltip',
    value: function showTooltip(e, isGlobalCall) {
      if (isGlobalCall) {
        // Don't trigger other elements belongs to other ReactTooltip
        var targetArray = this.getTargetArray(this.props.id);
        var isMyElement = targetArray.some(function (ele) {
          return ele === e.currentTarget;
        });
        if (!isMyElement) return;
      }
      // Get the tooltip content
      // calculate in this phrase so that tip width height can be detected
      var _props5 = this.props,
          multiline = _props5.multiline,
          getContent = _props5.getContent;

      var originTooltip = e.currentTarget.getAttribute('data-tip');
      var isMultiline = e.currentTarget.getAttribute('data-multiline') || multiline || false;

      // If it is focus event or called by ReactTooltip.show, switch to `solid` effect
      var switchToSolid = e instanceof window.FocusEvent || isGlobalCall;

      // if it needs to skip adding hide listener to scroll
      var scrollHide = true;
      if (e.currentTarget.getAttribute('data-scroll-hide')) {
        scrollHide = e.currentTarget.getAttribute('data-scroll-hide') === 'true';
      } else if (this.props.scrollHide != null) {
        scrollHide = this.props.scrollHide;
      }

      // To prevent previously created timers from triggering
      this.clearTimer();

      var target = e.currentTarget;

      var reshowDelay = this.state.show ? target.getAttribute('data-delay-update') || this.props.delayUpdate : 0;

      var self = this;

      var updateState = function updateState() {
        self.setState({
          originTooltip: originTooltip,
          isMultiline: isMultiline,
          desiredPlace: target.getAttribute('data-place') || self.props.place || 'top',
          place: target.getAttribute('data-place') || self.props.place || 'top',
          type: target.getAttribute('data-type') || self.props.type || 'dark',
          effect: switchToSolid && 'solid' || self.getEffect(target),
          offset: target.getAttribute('data-offset') || self.props.offset || {},
          html: target.getAttribute('data-html') ? target.getAttribute('data-html') === 'true' : self.props.html || false,
          delayShow: target.getAttribute('data-delay-show') || self.props.delayShow || 0,
          delayHide: target.getAttribute('data-delay-hide') || self.props.delayHide || 0,
          delayUpdate: target.getAttribute('data-delay-update') || self.props.delayUpdate || 0,
          border: target.getAttribute('data-border') ? target.getAttribute('data-border') === 'true' : self.props.border || false,
          extraClass: target.getAttribute('data-class') || self.props.class || self.props.className || '',
          disable: target.getAttribute('data-tip-disable') ? target.getAttribute('data-tip-disable') === 'true' : self.props.disable || false,
          currentTarget: target
        }, function () {
          if (scrollHide) self.addScrollListener(self.state.currentTarget);
          self.updateTooltip(e);

          if (getContent && Array.isArray(getContent)) {
            self.intervalUpdateContent = setInterval(function () {
              if (self.mount) {
                var _getContent = self.props.getContent;

                var placeholder = (0, _getTipContent2.default)(originTooltip, '', _getContent[0](), isMultiline);
                var isEmptyTip = self.isEmptyTip(placeholder);
                self.setState({
                  isEmptyTip: isEmptyTip
                });
                self.updatePosition();
              }
            }, getContent[1]);
          }
        });
      };

      // If there is no delay call immediately, don't allow events to get in first.
      if (reshowDelay) {
        this.delayReshow = setTimeout(updateState, reshowDelay);
      } else {
        updateState();
      }
    }

    /**
     * When mouse hover, updatetooltip
     */

  }, {
    key: 'updateTooltip',
    value: function updateTooltip(e) {
      var _this5 = this;

      var _state = this.state,
          delayShow = _state.delayShow,
          disable = _state.disable;
      var afterShow = this.props.afterShow;

      var placeholder = this.getTooltipContent();
      var delayTime = parseInt(delayShow, 10);
      var eventTarget = e.currentTarget || e.target;

      // Check if the mouse is actually over the tooltip, if so don't hide the tooltip
      if (this.mouseOnToolTip()) {
        return;
      }

      if (this.isEmptyTip(placeholder) || disable) return; // if the tooltip is empty, disable the tooltip
      var updateState = function updateState() {
        if (Array.isArray(placeholder) && placeholder.length > 0 || placeholder) {
          var isInvisible = !_this5.state.show;
          _this5.setState({
            currentEvent: e,
            currentTarget: eventTarget,
            show: true
          }, function () {
            _this5.updatePosition();
            if (isInvisible && afterShow) afterShow();
          });
        }
      };

      clearTimeout(this.delayShowLoop);
      if (delayShow) {
        this.delayShowLoop = setTimeout(updateState, delayTime);
      } else {
        updateState();
      }
    }

    /*
    * If we're mousing over the tooltip remove it when we leave.
     */

  }, {
    key: 'listenForTooltipExit',
    value: function listenForTooltipExit() {
      var show = this.state.show;


      if (show && this.tooltipRef) {
        this.tooltipRef.addEventListener('mouseleave', this.hideTooltip);
      }
    }
  }, {
    key: 'removeListenerForTooltipExit',
    value: function removeListenerForTooltipExit() {
      var show = this.state.show;


      if (show && this.tooltipRef) {
        this.tooltipRef.removeEventListener('mouseleave', this.hideTooltip);
      }
    }

    /**
     * When mouse leave, hide tooltip
     */

  }, {
    key: 'hideTooltip',
    value: function hideTooltip(e, hasTarget) {
      var _this6 = this;

      var _state2 = this.state,
          delayHide = _state2.delayHide,
          disable = _state2.disable;
      var afterHide = this.props.afterHide;

      var placeholder = this.getTooltipContent();
      if (!this.mount) return;
      if (this.isEmptyTip(placeholder) || disable) return; // if the tooltip is empty, disable the tooltip
      if (hasTarget) {
        // Don't trigger other elements belongs to other ReactTooltip
        var targetArray = this.getTargetArray(this.props.id);
        var isMyElement = targetArray.some(function (ele) {
          return ele === e.currentTarget;
        });
        if (!isMyElement || !this.state.show) return;
      }

      var resetState = function resetState() {
        var isVisible = _this6.state.show;
        // Check if the mouse is actually over the tooltip, if so don't hide the tooltip
        if (_this6.mouseOnToolTip()) {
          _this6.listenForTooltipExit();
          return;
        }
        _this6.removeListenerForTooltipExit();

        _this6.setState({
          show: false
        }, function () {
          _this6.removeScrollListener();
          if (isVisible && afterHide) afterHide();
        });
      };

      this.clearTimer();
      if (delayHide) {
        this.delayHideLoop = setTimeout(resetState, parseInt(delayHide, 10));
      } else {
        resetState();
      }
    }

    /**
     * Add scroll eventlistener when tooltip show
     * automatically hide the tooltip when scrolling
     */

  }, {
    key: 'addScrollListener',
    value: function addScrollListener(currentTarget) {
      var isCaptureMode = this.isCapture(currentTarget);
      window.addEventListener('scroll', this.hideTooltip, isCaptureMode);
    }
  }, {
    key: 'removeScrollListener',
    value: function removeScrollListener() {
      window.removeEventListener('scroll', this.hideTooltip);
    }

    // Calculation the position

  }, {
    key: 'updatePosition',
    value: function updatePosition() {
      var _this7 = this;

      var _state3 = this.state,
          currentEvent = _state3.currentEvent,
          currentTarget = _state3.currentTarget,
          place = _state3.place,
          desiredPlace = _state3.desiredPlace,
          effect = _state3.effect,
          offset = _state3.offset;

      var node = _reactDom2.default.findDOMNode(this);
      var result = (0, _getPosition2.default)(currentEvent, currentTarget, node, place, desiredPlace, effect, offset);

      if (result.isNewState) {
        // Switch to reverse placement
        return this.setState(result.newState, function () {
          _this7.updatePosition();
        });
      }
      // Set tooltip position
      node.style.left = result.position.left + 'px';
      node.style.top = result.position.top + 'px';
    }

    /**
     * Set style tag in header
     * in this way we can insert default css
     */

  }, {
    key: 'setStyleHeader',
    value: function setStyleHeader() {
      var head = document.getElementsByTagName('head')[0];
      if (!head.querySelector('style[id="react-tooltip"]')) {
        var tag = document.createElement('style');
        tag.id = 'react-tooltip';
        tag.innerHTML = _style2.default;
        /* eslint-disable */
        if (typeof __webpack_nonce__ !== 'undefined' && __webpack_nonce__) {
          tag.setAttribute('nonce', __webpack_nonce__);
        }
        /* eslint-enable */
        head.insertBefore(tag, head.firstChild);
      }
    }

    /**
     * CLear all kinds of timeout of interval
     */

  }, {
    key: 'clearTimer',
    value: function clearTimer() {
      clearTimeout(this.delayShowLoop);
      clearTimeout(this.delayHideLoop);
      clearTimeout(this.delayReshow);
      clearInterval(this.intervalUpdateContent);
    }
  }, {
    key: 'render',
    value: function render$$1() {
      var _this8 = this;

      var _state4 = this.state,
          extraClass = _state4.extraClass,
          html = _state4.html,
          ariaProps = _state4.ariaProps,
          disable = _state4.disable;

      var placeholder = this.getTooltipContent();
      var isEmptyTip = this.isEmptyTip(placeholder);
      var tooltipClass = (0, _classnames2.default)('__react_component_tooltip', { 'show': this.state.show && !disable && !isEmptyTip }, { 'border': this.state.border }, { 'place-top': this.state.place === 'top' }, { 'place-bottom': this.state.place === 'bottom' }, { 'place-left': this.state.place === 'left' }, { 'place-right': this.state.place === 'right' }, { 'type-dark': this.state.type === 'dark' }, { 'type-success': this.state.type === 'success' }, { 'type-warning': this.state.type === 'warning' }, { 'type-error': this.state.type === 'error' }, { 'type-info': this.state.type === 'info' }, { 'type-light': this.state.type === 'light' }, { 'allow_hover': this.props.delayUpdate });

      var Wrapper = this.props.wrapper;
      if (ReactTooltip.supportedWrappers.indexOf(Wrapper) < 0) {
        Wrapper = ReactTooltip.defaultProps.wrapper;
      }

      if (html) {
        return _react2.default.createElement(Wrapper, _extends({ className: tooltipClass + ' ' + extraClass,
          id: this.props.id,
          ref: function ref(_ref) {
            return _this8.tooltipRef = _ref;
          }
        }, ariaProps, {
          'data-id': 'tooltip',
          dangerouslySetInnerHTML: { __html: placeholder } }));
      } else {
        return _react2.default.createElement(
          Wrapper,
          _extends({ className: tooltipClass + ' ' + extraClass,
            id: this.props.id
          }, ariaProps, {
            ref: function ref(_ref2) {
              return _this8.tooltipRef = _ref2;
            },
            'data-id': 'tooltip' }),
          placeholder
        );
      }
    }
  }]);

  return ReactTooltip;
}(_react2.default.Component), _class2.propTypes = {
  children: _propTypes2.default.any,
  place: _propTypes2.default.string,
  type: _propTypes2.default.string,
  effect: _propTypes2.default.string,
  offset: _propTypes2.default.object,
  multiline: _propTypes2.default.bool,
  border: _propTypes2.default.bool,
  insecure: _propTypes2.default.bool,
  class: _propTypes2.default.string,
  className: _propTypes2.default.string,
  id: _propTypes2.default.string,
  html: _propTypes2.default.bool,
  delayHide: _propTypes2.default.number,
  delayUpdate: _propTypes2.default.number,
  delayShow: _propTypes2.default.number,
  event: _propTypes2.default.string,
  eventOff: _propTypes2.default.string,
  watchWindow: _propTypes2.default.bool,
  isCapture: _propTypes2.default.bool,
  globalEventOff: _propTypes2.default.string,
  getContent: _propTypes2.default.any,
  afterShow: _propTypes2.default.func,
  afterHide: _propTypes2.default.func,
  disable: _propTypes2.default.bool,
  scrollHide: _propTypes2.default.bool,
  resizeHide: _propTypes2.default.bool,
  wrapper: _propTypes2.default.string
}, _class2.defaultProps = {
  insecure: true,
  resizeHide: true,
  wrapper: 'div'
}, _class2.supportedWrappers = ['div', 'span'], _class2.displayName = 'ReactTooltip', _temp)) || _class) || _class) || _class) || _class) || _class) || _class;

/* export default not fit for standalone, it will exports {default:...} */


module.exports = ReactTooltip;
});

var ReactTooltip = unwrapExports(dist$1);

var Companion = function (_React$Component) {
  inherits(Companion, _React$Component);

  function Companion(props) {
    classCallCheck(this, Companion);

    var _this = possibleConstructorReturn(this, (Companion.__proto__ || Object.getPrototypeOf(Companion)).call(this, props));

    _this.state = {};
    _this.element = createRef();
    return _this;
  }

  createClass(Companion, [{
    key: 'createCompanion',
    value: function createCompanion() {
      var _this2 = this;

      var walls = [];
      var lines = this.props.lines;
      var revertIndex = false;
      if (lines && lines.length) {
        var sortedLines = lines.concat().sort(function (a, b) {
          return a.bottom.x - b.bottom.x;
        });
        if (sortedLines[0] !== lines[0]) {
          revertIndex = true;
        }
        var containerWidth = this.props.container.offsetWidth;
        var containerHeight = this.props.container.offsetHeight;
        var imageSize = this.props.preview.size;
        var fittedSize = this.getObjectFitSize(true, containerWidth, containerHeight, imageSize.width, imageSize.height);
        var wallCount = sortedLines.length - 1;
        var widthMultiplier = fittedSize.width / sortedLines[sortedLines.length - 1].bottom.x;
        var heightMultiplier = widthMultiplier / imageSize.width * imageSize.height;

        var _loop = function _loop(i) {
          var points = [];
          [sortedLines[i], sortedLines[i + 1]].forEach(function (line) {
            points.push(line.bottom);
            points.push(line.top);
          });
          var centroid = _this2.getPointsCentroid(points);
          var top = fittedSize.y + centroid.y * heightMultiplier - 7;
          top = top > fittedSize.y + fittedSize.height ? fittedSize.y + centroid.y * 4.95 / imageSize.width * imageSize.height - 7 : top;
          walls.push({
            left: fittedSize.x + centroid.x * widthMultiplier - 7 + 'px',
            top: top + 'px'
          });
        };

        for (var i = 0; i < wallCount; i++) {
          _loop(i);
        }
      }
      return walls.map(function (item, index) {
        return createElement(
          'div',
          { key: index,
            className: styles$3.companion,
            'data-tip': revertIndex ? walls.length - index - 1 : index,
            'data-for': 'companion',
            style: item },
          createElement('img', { key: index,
            src: RollerIcon,
            alt: '' })
        );
      });
    }
  }, {
    key: 'getWallpapers',
    value: function getWallpapers(wallIndex) {
      var _this3 = this;

      return createElement(
        'div',
        { className: styles$3.companionsContainer },
        createElement('div', { className: styles$3.close, onClick: function onClick(event) {
            ReactTooltip.hide();
            var element = event.currentTarget.offsetParent.offsetParent;
            element.style.display = 'none';
            setTimeout(function () {
              element.style.display = null;
            }, 100);
          } }),
        createElement(
          'div',
          { className: styles$3.companionsTitle },
          '\u041A\u043E\u043C\u043F\u0430\u043D\u044C\u043E\u043D\u044B'
        ),
        this.props.wallpapers ? this.props.wallpapers.map(function (wallpaper, wallpaperIndex) {
          return createElement('img', { key: wallpaperIndex,
            src: wallpaper.preview ? wallpaper.preview.path : '/',
            onClick: function onClick() {
              _this3.onClick(wallpaper, wallIndex, wallpaperIndex);
            } });
        }) : ''
      );
    }
  }, {
    key: 'onClick',
    value: function onClick(wallpaper, wallIndex, wallpaperIndex) {
      this.props.onSelect(wallpaper, wallIndex, wallpaperIndex);
    }
  }, {
    key: 'render',
    value: function render$$1() {
      var _this4 = this;

      return createElement(
        'div',
        { ref: this.element },
        this.createCompanion(),
        createElement(ReactTooltip, { id: 'companion',
          className: styles$3.companionTooltip,
          getContent: function getContent(wallIndex) {
            return _this4.getWallpapers(wallIndex);
          },
          globalEventOff: 'click',
          event: 'click',
          place: 'bottom' })
      );
    }
  }, {
    key: 'getObjectFitSize',
    value: function getObjectFitSize(contains, containerWidth, containerHeight, width, height) {
      var doRatio = width / height;
      var cRatio = containerWidth / containerHeight;
      var targetWidth = 0;
      var targetHeight = 0;
      var test = contains ? doRatio > cRatio : doRatio < cRatio;

      if (test) {
        targetWidth = containerWidth;
        targetHeight = targetWidth / doRatio;
      } else {
        targetHeight = containerHeight;
        targetWidth = targetHeight * doRatio;
      }

      return {
        width: targetWidth,
        height: targetHeight,
        x: (containerWidth - targetWidth) / 2,
        y: (containerHeight - targetHeight) / 2
      };
    }
  }, {
    key: 'getPointsCentroid',
    value: function getPointsCentroid(points) {
      var centroid = { x: 0, y: 0 };
      for (var i = 0; i < points.length; i++) {
        var point = points[i];
        centroid.x += point.x;
        centroid.y += point.y;
      }
      centroid.x /= points.length;
      centroid.y /= points.length;
      return centroid;
    }
  }]);
  return Companion;
}(Component);

var Loader = reactLoaderSpinner.default;

var PreviewResult = function (_React$Component) {
  inherits(PreviewResult, _React$Component);

  function PreviewResult(props) {
    classCallCheck(this, PreviewResult);

    var _this = possibleConstructorReturn(this, (PreviewResult.__proto__ || Object.getPrototypeOf(PreviewResult)).call(this, props));

    _this.state = {
      isLoading: true,
      isImageOpen: false
    };
    _this.element = createRef();
    return _this;
  }

  createClass(PreviewResult, [{
    key: 'componentWillUpdate',
    value: function componentWillUpdate(newProps, newState) {
      if (!newProps.dataSource || !this.props.dataSource) {
        return;
      }
      var newPath = newProps.dataSource.images[0].path;
      var oldPath = this.props.dataSource.images[0].path;
      if (newPath !== oldPath || !this.state.isLoading && newProps.isLoading) {
        this.setState({
          isLoading: true
        });
      } else if (newPath === oldPath && this.state.isLoading && !newProps.isLoading) {
        this.setState({
          isLoading: false
        });
      }
    }
  }, {
    key: 'render',
    value: function render$$1() {
      var _this2 = this;

      var preview = this.props.dataSource ? this.props.dataSource.images[0] : {};
      var path = preview ? preview.path : '/';
      return createElement(
        'div',
        { className: styles$2.w_preview_container,
          ref: this.element,
          style: this.props.style },
        this.state.isLoading ? this.createLoader() : '',
        createElement('img', { src: path,
          className: styles$2.preview_image,
          onLoad: function onLoad() {
            _this2.setState({
              isLoading: false
            });
            _this2.props.handlerOnLoad();
          },
          alt: '' }),
        createElement('img', { src: FullScreenIcon,
          className: styles$2.full_screen_image,
          onClick: function onClick() {
            return _this2.setState({ isImageOpen: true });
          },
          alt: '' }),
        this.state.isImageOpen ? createElement(ReactImageLightbox, {
          mainSrc: path,
          enableZoom: false,
          reactModalStyle: { overlay: { zIndex: 10000 } },
          onCloseRequest: function onCloseRequest() {
            return _this2.setState({ isImageOpen: false });
          }
        }) : '',
        !this.state.isLoading && this.props.properties.walls ? createElement(Companion, { lines: this.props.lines,
          container: this.element.current,
          preview: preview,
          wallpapers: this.props.wallpapers,
          onSelect: function onSelect(wallpaper, wallIndex, wallpaperIndex) {
            _this2.props.onSelect(wallpaper, wallIndex, wallpaperIndex);
          } }) : ''
      );
    }
  }, {
    key: 'createLoader',
    value: function createLoader() {
      return createElement(
        'div',
        { className: styles$2.loader_container },
        createElement(Loader, { color: '#fff', width: 200, heigth: 200, type: 'Watch' })
      );
    }
  }]);
  return PreviewResult;
}(Component);

var Components = {
  Filter: 'FilterWallpaper',
  PreviewResult: 'PreviewResult',
  Rooms: 'RoomGallery',
  Wallpaper: 'WallpaperGallery',
  FilterModule: 'FilterModule'
};

var css$8 = "/*\n * Container style\n */\n.ps {\n  overflow: hidden !important;\n  overflow-anchor: none;\n  -ms-overflow-style: none;\n  touch-action: auto;\n  -ms-touch-action: auto;\n}\n\n/*\n * Scrollbar rail styles\n */\n.ps__rail-x {\n  display: none;\n  opacity: 0;\n  transition: background-color .2s linear, opacity .2s linear;\n  -webkit-transition: background-color .2s linear, opacity .2s linear;\n  height: 15px;\n  /* there must be 'bottom' or 'top' for ps__rail-x */\n  bottom: 0px;\n  /* please don't change 'position' */\n  position: absolute;\n}\n\n.ps__rail-y {\n  display: none;\n  opacity: 0;\n  transition: background-color .2s linear, opacity .2s linear;\n  -webkit-transition: background-color .2s linear, opacity .2s linear;\n  width: 15px;\n  /* there must be 'right' or 'left' for ps__rail-y */\n  right: 0;\n  /* please don't change 'position' */\n  position: absolute;\n}\n\n.ps--active-x > .ps__rail-x,\n.ps--active-y > .ps__rail-y {\n  display: block;\n  background-color: transparent;\n}\n\n.ps:hover > .ps__rail-x,\n.ps:hover > .ps__rail-y,\n.ps--focus > .ps__rail-x,\n.ps--focus > .ps__rail-y,\n.ps--scrolling-x > .ps__rail-x,\n.ps--scrolling-y > .ps__rail-y {\n  opacity: 0.6;\n}\n\n.ps .ps__rail-x:hover,\n.ps .ps__rail-y:hover,\n.ps .ps__rail-x:focus,\n.ps .ps__rail-y:focus,\n.ps .ps__rail-x.ps--clicking,\n.ps .ps__rail-y.ps--clicking {\n  background-color: #eee;\n  opacity: 0.9;\n}\n\n/*\n * Scrollbar thumb styles\n */\n.ps__thumb-x {\n  background-color: #aaa;\n  border-radius: 6px;\n  transition: background-color .2s linear, height .2s ease-in-out;\n  -webkit-transition: background-color .2s linear, height .2s ease-in-out;\n  height: 6px;\n  /* there must be 'bottom' for ps__thumb-x */\n  bottom: 2px;\n  /* please don't change 'position' */\n  position: absolute;\n}\n\n.ps__thumb-y {\n  background-color: #aaa;\n  border-radius: 6px;\n  transition: background-color .2s linear, width .2s ease-in-out;\n  -webkit-transition: background-color .2s linear, width .2s ease-in-out;\n  width: 6px;\n  /* there must be 'right' for ps__thumb-y */\n  right: 2px;\n  /* please don't change 'position' */\n  position: absolute;\n}\n\n.ps__rail-x:hover > .ps__thumb-x,\n.ps__rail-x:focus > .ps__thumb-x,\n.ps__rail-x.ps--clicking .ps__thumb-x {\n  background-color: #999;\n  height: 11px;\n}\n\n.ps__rail-y:hover > .ps__thumb-y,\n.ps__rail-y:focus > .ps__thumb-y,\n.ps__rail-y.ps--clicking .ps__thumb-y {\n  background-color: #999;\n  width: 11px;\n}\n\n/* MS supports */\n@supports (-ms-overflow-style: none) {\n  .ps {\n    overflow: auto !important;\n  }\n}\n\n@media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {\n  .ps {\n    overflow: auto !important;\n  }\n}\n.scrollbar-container {\n  position: relative;\n  height: 100%; }";
styleInject(css$8);

var css$9 = ".ImageListView_list_view_container__2tam2 {\n  position: relative;\n  overflow: hidden;\n  border: 1px solid #ccc;\n  box-sizing: border-box;\n}\n\n.ImageListView_list_view_container__2tam2.ImageListView_grid__1khse {\n  height: 100%;\n}\n\n.ImageListView_list_view_container__2tam2.ImageListView_vertical__24gdq {\n  height: 100%;\n}\n\n.ImageListView_list_view_container__2tam2.ImageListView_horizontal__306dH {\n  width: 100%;\n}\n\n.ImageListView_list_view__Bbiko {\n  display: flex;\n}\n\n.ImageListView_list_view__Bbiko.ImageListView_vertical__24gdq {\n  flex-direction: column;\n}\n\n.ImageListView_list_view__Bbiko.ImageListView_horizontal__306dH {\n  flex-direction: row;\n}\n\n.ImageListView_list_view__Bbiko.ImageListView_grid__1khse {\n  flex-direction: row;\n  flex-wrap: wrap;\n}\n\n.ImageListView_image_preview__3p9v- {\n  flex: 0 0 auto;\n  cursor: pointer;\n  max-width: inherit;\n}\n\n.ImageListView_list_view__Bbiko.ImageListView_vertical__24gdq .ImageListView_image_preview__3p9v- {\n  margin-top: 15px;\n}\n\n.ImageListView_list_view__Bbiko.ImageListView_vertical__24gdq .ImageListView_image_group__Xoh9Z {\n  flex-direction: column;\n  padding: 0 15px 15px 15px;\n}\n\n.ImageListView_list_view__Bbiko.ImageListView_vertical__24gdq .ImageListView_image_group__Xoh9Z img {\n  margin-left: 0;\n}\n\n.ImageListView_list_view__Bbiko.ImageListView_vertical__24gdq .ImageListView_image_group_separator__3ciYe {\n  border-left: none;\n}\n\n.ImageListView_list_view__Bbiko.ImageListView_vertical__24gdq .ImageListView_image_group_header__2XM_d {\n  font-weight: bold;\n  text-align: center;\n  display: block;\n  margin-bottom: 10px;\n  top: -5px;\n}\n\n.ImageListView_image_group__Xoh9Z {\n  display: flex;\n  flex-direction: row;\n  position: relative;\n  padding: 25px 15px 15px 15px;\n  align-items: center;\n}\n\n.ImageListView_image_group__Xoh9Z > img:last-child:after {\n  border-right: 1px solid #ccc;\n}\n\n.ImageListView_image_group__Xoh9Z img + img {\n  margin-left: 15px;\n}\n\n.ImageListView_image_group__Xoh9Z img {\n  object-fit: cover;\n}\n\n.ImageListView_image_group_header__2XM_d {\n  position: absolute;\n  top: 5px;\n  font-size: 13px;\n}\n\n.ImageListView_image_group_separator__3ciYe {\n  border-left: 1px solid #ccc;\n  margin-left: 15px;\n  position: absolute;\n  right: 0;\n  top: 25px;\n  bottom: 15px;\n}\n\n.ImageListView_image_group__Xoh9Z:last-child .ImageListView_image_group_separator__3ciYe {\n  display: none;\n}\n\n.ImageListView_image_selected__Sm2ve {\n  outline: 2px #03e5fc solid;\n}\n\n.ImageListView_label_header__3V6h- {\n  font-weight: bold;\n  margin-top: 15px;\n  text-align: center;\n  display: block;\n}\n\n.ImageListView_image_row_group__2rsxq img{\n  margin-left: 15px;\n}\n\n.ImageListView_no_result_label__21Z-- {\n  margin: 15px;\n}\n";
var styles$5 = { "list_view_container": "ImageListView_list_view_container__2tam2", "grid": "ImageListView_grid__1khse", "vertical": "ImageListView_vertical__24gdq", "horizontal": "ImageListView_horizontal__306dH", "list_view": "ImageListView_list_view__Bbiko", "image_preview": "ImageListView_image_preview__3p9v-", "image_group": "ImageListView_image_group__Xoh9Z", "image_group_separator": "ImageListView_image_group_separator__3ciYe", "image_group_header": "ImageListView_image_group_header__2XM_d", "image_selected": "ImageListView_image_selected__Sm2ve", "label_header": "ImageListView_label_header__3V6h-", "image_row_group": "ImageListView_image_row_group__2rsxq", "no_result_label": "ImageListView_no_result_label__21Z--" };
styleInject(css$9);

/*!
 * perfect-scrollbar v1.4.0
 * (c) 2018 Hyunje Jun
 * @license MIT
 */
function get$1(element) {
  return getComputedStyle(element);
}

function set$1(element, obj) {
  for (var key in obj) {
    var val = obj[key];
    if (typeof val === 'number') {
      val = val + "px";
    }
    element.style[key] = val;
  }
  return element;
}

function div(className) {
  var div = document.createElement('div');
  div.className = className;
  return div;
}

var elMatches =
  typeof Element !== 'undefined' &&
  (Element.prototype.matches ||
    Element.prototype.webkitMatchesSelector ||
    Element.prototype.mozMatchesSelector ||
    Element.prototype.msMatchesSelector);

function matches(element, query) {
  if (!elMatches) {
    throw new Error('No element matching method supported');
  }

  return elMatches.call(element, query);
}

function remove(element) {
  if (element.remove) {
    element.remove();
  } else {
    if (element.parentNode) {
      element.parentNode.removeChild(element);
    }
  }
}

function queryChildren(element, selector) {
  return Array.prototype.filter.call(element.children, function (child) { return matches(child, selector); }
  );
}

var cls = {
  main: 'ps',
  element: {
    thumb: function (x) { return ("ps__thumb-" + x); },
    rail: function (x) { return ("ps__rail-" + x); },
    consuming: 'ps__child--consume',
  },
  state: {
    focus: 'ps--focus',
    clicking: 'ps--clicking',
    active: function (x) { return ("ps--active-" + x); },
    scrolling: function (x) { return ("ps--scrolling-" + x); },
  },
};

/*
 * Helper methods
 */
var scrollingClassTimeout = { x: null, y: null };

function addScrollingClass(i, x) {
  var classList = i.element.classList;
  var className = cls.state.scrolling(x);

  if (classList.contains(className)) {
    clearTimeout(scrollingClassTimeout[x]);
  } else {
    classList.add(className);
  }
}

function removeScrollingClass(i, x) {
  scrollingClassTimeout[x] = setTimeout(
    function () { return i.isAlive && i.element.classList.remove(cls.state.scrolling(x)); },
    i.settings.scrollingThreshold
  );
}

function setScrollingClassInstantly(i, x) {
  addScrollingClass(i, x);
  removeScrollingClass(i, x);
}

var EventElement = function EventElement(element) {
  this.element = element;
  this.handlers = {};
};

var prototypeAccessors = { isEmpty: { configurable: true } };

EventElement.prototype.bind = function bind (eventName, handler) {
  if (typeof this.handlers[eventName] === 'undefined') {
    this.handlers[eventName] = [];
  }
  this.handlers[eventName].push(handler);
  this.element.addEventListener(eventName, handler, false);
};

EventElement.prototype.unbind = function unbind (eventName, target) {
    var this$1 = this;

  this.handlers[eventName] = this.handlers[eventName].filter(function (handler) {
    if (target && handler !== target) {
      return true;
    }
    this$1.element.removeEventListener(eventName, handler, false);
    return false;
  });
};

EventElement.prototype.unbindAll = function unbindAll () {
    var this$1 = this;

  for (var name in this$1.handlers) {
    this$1.unbind(name);
  }
};

prototypeAccessors.isEmpty.get = function () {
    var this$1 = this;

  return Object.keys(this.handlers).every(
    function (key) { return this$1.handlers[key].length === 0; }
  );
};

Object.defineProperties( EventElement.prototype, prototypeAccessors );

var EventManager = function EventManager() {
  this.eventElements = [];
};

EventManager.prototype.eventElement = function eventElement (element) {
  var ee = this.eventElements.filter(function (ee) { return ee.element === element; })[0];
  if (!ee) {
    ee = new EventElement(element);
    this.eventElements.push(ee);
  }
  return ee;
};

EventManager.prototype.bind = function bind (element, eventName, handler) {
  this.eventElement(element).bind(eventName, handler);
};

EventManager.prototype.unbind = function unbind (element, eventName, handler) {
  var ee = this.eventElement(element);
  ee.unbind(eventName, handler);

  if (ee.isEmpty) {
    // remove
    this.eventElements.splice(this.eventElements.indexOf(ee), 1);
  }
};

EventManager.prototype.unbindAll = function unbindAll () {
  this.eventElements.forEach(function (e) { return e.unbindAll(); });
  this.eventElements = [];
};

EventManager.prototype.once = function once (element, eventName, handler) {
  var ee = this.eventElement(element);
  var onceHandler = function (evt) {
    ee.unbind(eventName, onceHandler);
    handler(evt);
  };
  ee.bind(eventName, onceHandler);
};

function createEvent(name) {
  if (typeof window.CustomEvent === 'function') {
    return new CustomEvent(name);
  } else {
    var evt = document.createEvent('CustomEvent');
    evt.initCustomEvent(name, false, false, undefined);
    return evt;
  }
}

var processScrollDiff = function(
  i,
  axis,
  diff,
  useScrollingClass,
  forceFireReachEvent
) {
  if ( useScrollingClass === void 0 ) useScrollingClass = true;
  if ( forceFireReachEvent === void 0 ) forceFireReachEvent = false;

  var fields;
  if (axis === 'top') {
    fields = [
      'contentHeight',
      'containerHeight',
      'scrollTop',
      'y',
      'up',
      'down' ];
  } else if (axis === 'left') {
    fields = [
      'contentWidth',
      'containerWidth',
      'scrollLeft',
      'x',
      'left',
      'right' ];
  } else {
    throw new Error('A proper axis should be provided');
  }

  processScrollDiff$1(i, diff, fields, useScrollingClass, forceFireReachEvent);
};

function processScrollDiff$1(
  i,
  diff,
  ref,
  useScrollingClass,
  forceFireReachEvent
) {
  var contentHeight = ref[0];
  var containerHeight = ref[1];
  var scrollTop = ref[2];
  var y = ref[3];
  var up = ref[4];
  var down = ref[5];
  if ( useScrollingClass === void 0 ) useScrollingClass = true;
  if ( forceFireReachEvent === void 0 ) forceFireReachEvent = false;

  var element = i.element;

  // reset reach
  i.reach[y] = null;

  // 1 for subpixel rounding
  if (element[scrollTop] < 1) {
    i.reach[y] = 'start';
  }

  // 1 for subpixel rounding
  if (element[scrollTop] > i[contentHeight] - i[containerHeight] - 1) {
    i.reach[y] = 'end';
  }

  if (diff) {
    element.dispatchEvent(createEvent(("ps-scroll-" + y)));

    if (diff < 0) {
      element.dispatchEvent(createEvent(("ps-scroll-" + up)));
    } else if (diff > 0) {
      element.dispatchEvent(createEvent(("ps-scroll-" + down)));
    }

    if (useScrollingClass) {
      setScrollingClassInstantly(i, y);
    }
  }

  if (i.reach[y] && (diff || forceFireReachEvent)) {
    element.dispatchEvent(createEvent(("ps-" + y + "-reach-" + (i.reach[y]))));
  }
}

function toInt(x) {
  return parseInt(x, 10) || 0;
}

function isEditable(el) {
  return (
    matches(el, 'input,[contenteditable]') ||
    matches(el, 'select,[contenteditable]') ||
    matches(el, 'textarea,[contenteditable]') ||
    matches(el, 'button,[contenteditable]')
  );
}

function outerWidth(element) {
  var styles = get$1(element);
  return (
    toInt(styles.width) +
    toInt(styles.paddingLeft) +
    toInt(styles.paddingRight) +
    toInt(styles.borderLeftWidth) +
    toInt(styles.borderRightWidth)
  );
}

var env = {
  isWebKit:
    typeof document !== 'undefined' &&
    'WebkitAppearance' in document.documentElement.style,
  supportsTouch:
    typeof window !== 'undefined' &&
    ('ontouchstart' in window ||
      (window.DocumentTouch && document instanceof window.DocumentTouch)),
  supportsIePointer:
    typeof navigator !== 'undefined' && navigator.msMaxTouchPoints,
  isChrome:
    typeof navigator !== 'undefined' &&
    /Chrome/i.test(navigator && navigator.userAgent),
};

var updateGeometry = function(i) {
  var element = i.element;
  var roundedScrollTop = Math.floor(element.scrollTop);

  i.containerWidth = element.clientWidth;
  i.containerHeight = element.clientHeight;
  i.contentWidth = element.scrollWidth;
  i.contentHeight = element.scrollHeight;

  if (!element.contains(i.scrollbarXRail)) {
    // clean up and append
    queryChildren(element, cls.element.rail('x')).forEach(function (el) { return remove(el); }
    );
    element.appendChild(i.scrollbarXRail);
  }
  if (!element.contains(i.scrollbarYRail)) {
    // clean up and append
    queryChildren(element, cls.element.rail('y')).forEach(function (el) { return remove(el); }
    );
    element.appendChild(i.scrollbarYRail);
  }

  if (
    !i.settings.suppressScrollX &&
    i.containerWidth + i.settings.scrollXMarginOffset < i.contentWidth
  ) {
    i.scrollbarXActive = true;
    i.railXWidth = i.containerWidth - i.railXMarginWidth;
    i.railXRatio = i.containerWidth / i.railXWidth;
    i.scrollbarXWidth = getThumbSize(
      i,
      toInt(i.railXWidth * i.containerWidth / i.contentWidth)
    );
    i.scrollbarXLeft = toInt(
      (i.negativeScrollAdjustment + element.scrollLeft) *
        (i.railXWidth - i.scrollbarXWidth) /
        (i.contentWidth - i.containerWidth)
    );
  } else {
    i.scrollbarXActive = false;
  }

  if (
    !i.settings.suppressScrollY &&
    i.containerHeight + i.settings.scrollYMarginOffset < i.contentHeight
  ) {
    i.scrollbarYActive = true;
    i.railYHeight = i.containerHeight - i.railYMarginHeight;
    i.railYRatio = i.containerHeight / i.railYHeight;
    i.scrollbarYHeight = getThumbSize(
      i,
      toInt(i.railYHeight * i.containerHeight / i.contentHeight)
    );
    i.scrollbarYTop = toInt(
      roundedScrollTop *
        (i.railYHeight - i.scrollbarYHeight) /
        (i.contentHeight - i.containerHeight)
    );
  } else {
    i.scrollbarYActive = false;
  }

  if (i.scrollbarXLeft >= i.railXWidth - i.scrollbarXWidth) {
    i.scrollbarXLeft = i.railXWidth - i.scrollbarXWidth;
  }
  if (i.scrollbarYTop >= i.railYHeight - i.scrollbarYHeight) {
    i.scrollbarYTop = i.railYHeight - i.scrollbarYHeight;
  }

  updateCss(element, i);

  if (i.scrollbarXActive) {
    element.classList.add(cls.state.active('x'));
  } else {
    element.classList.remove(cls.state.active('x'));
    i.scrollbarXWidth = 0;
    i.scrollbarXLeft = 0;
    element.scrollLeft = 0;
  }
  if (i.scrollbarYActive) {
    element.classList.add(cls.state.active('y'));
  } else {
    element.classList.remove(cls.state.active('y'));
    i.scrollbarYHeight = 0;
    i.scrollbarYTop = 0;
    element.scrollTop = 0;
  }
};

function getThumbSize(i, thumbSize) {
  if (i.settings.minScrollbarLength) {
    thumbSize = Math.max(thumbSize, i.settings.minScrollbarLength);
  }
  if (i.settings.maxScrollbarLength) {
    thumbSize = Math.min(thumbSize, i.settings.maxScrollbarLength);
  }
  return thumbSize;
}

function updateCss(element, i) {
  var xRailOffset = { width: i.railXWidth };
  var roundedScrollTop = Math.floor(element.scrollTop);

  if (i.isRtl) {
    xRailOffset.left =
      i.negativeScrollAdjustment +
      element.scrollLeft +
      i.containerWidth -
      i.contentWidth;
  } else {
    xRailOffset.left = element.scrollLeft;
  }
  if (i.isScrollbarXUsingBottom) {
    xRailOffset.bottom = i.scrollbarXBottom - roundedScrollTop;
  } else {
    xRailOffset.top = i.scrollbarXTop + roundedScrollTop;
  }
  set$1(i.scrollbarXRail, xRailOffset);

  var yRailOffset = { top: roundedScrollTop, height: i.railYHeight };
  if (i.isScrollbarYUsingRight) {
    if (i.isRtl) {
      yRailOffset.right =
        i.contentWidth -
        (i.negativeScrollAdjustment + element.scrollLeft) -
        i.scrollbarYRight -
        i.scrollbarYOuterWidth;
    } else {
      yRailOffset.right = i.scrollbarYRight - element.scrollLeft;
    }
  } else {
    if (i.isRtl) {
      yRailOffset.left =
        i.negativeScrollAdjustment +
        element.scrollLeft +
        i.containerWidth * 2 -
        i.contentWidth -
        i.scrollbarYLeft -
        i.scrollbarYOuterWidth;
    } else {
      yRailOffset.left = i.scrollbarYLeft + element.scrollLeft;
    }
  }
  set$1(i.scrollbarYRail, yRailOffset);

  set$1(i.scrollbarX, {
    left: i.scrollbarXLeft,
    width: i.scrollbarXWidth - i.railBorderXWidth,
  });
  set$1(i.scrollbarY, {
    top: i.scrollbarYTop,
    height: i.scrollbarYHeight - i.railBorderYWidth,
  });
}

var clickRail = function(i) {
  i.event.bind(i.scrollbarY, 'mousedown', function (e) { return e.stopPropagation(); });
  i.event.bind(i.scrollbarYRail, 'mousedown', function (e) {
    var positionTop =
      e.pageY -
      window.pageYOffset -
      i.scrollbarYRail.getBoundingClientRect().top;
    var direction = positionTop > i.scrollbarYTop ? 1 : -1;

    i.element.scrollTop += direction * i.containerHeight;
    updateGeometry(i);

    e.stopPropagation();
  });

  i.event.bind(i.scrollbarX, 'mousedown', function (e) { return e.stopPropagation(); });
  i.event.bind(i.scrollbarXRail, 'mousedown', function (e) {
    var positionLeft =
      e.pageX -
      window.pageXOffset -
      i.scrollbarXRail.getBoundingClientRect().left;
    var direction = positionLeft > i.scrollbarXLeft ? 1 : -1;

    i.element.scrollLeft += direction * i.containerWidth;
    updateGeometry(i);

    e.stopPropagation();
  });
};

var dragThumb = function(i) {
  bindMouseScrollHandler(i, [
    'containerWidth',
    'contentWidth',
    'pageX',
    'railXWidth',
    'scrollbarX',
    'scrollbarXWidth',
    'scrollLeft',
    'x',
    'scrollbarXRail' ]);
  bindMouseScrollHandler(i, [
    'containerHeight',
    'contentHeight',
    'pageY',
    'railYHeight',
    'scrollbarY',
    'scrollbarYHeight',
    'scrollTop',
    'y',
    'scrollbarYRail' ]);
};

function bindMouseScrollHandler(
  i,
  ref
) {
  var containerHeight = ref[0];
  var contentHeight = ref[1];
  var pageY = ref[2];
  var railYHeight = ref[3];
  var scrollbarY = ref[4];
  var scrollbarYHeight = ref[5];
  var scrollTop = ref[6];
  var y = ref[7];
  var scrollbarYRail = ref[8];

  var element = i.element;

  var startingScrollTop = null;
  var startingMousePageY = null;
  var scrollBy = null;

  function mouseMoveHandler(e) {
    element[scrollTop] =
      startingScrollTop + scrollBy * (e[pageY] - startingMousePageY);
    addScrollingClass(i, y);
    updateGeometry(i);

    e.stopPropagation();
    e.preventDefault();
  }

  function mouseUpHandler() {
    removeScrollingClass(i, y);
    i[scrollbarYRail].classList.remove(cls.state.clicking);
    i.event.unbind(i.ownerDocument, 'mousemove', mouseMoveHandler);
  }

  i.event.bind(i[scrollbarY], 'mousedown', function (e) {
    startingScrollTop = element[scrollTop];
    startingMousePageY = e[pageY];
    scrollBy =
      (i[contentHeight] - i[containerHeight]) /
      (i[railYHeight] - i[scrollbarYHeight]);

    i.event.bind(i.ownerDocument, 'mousemove', mouseMoveHandler);
    i.event.once(i.ownerDocument, 'mouseup', mouseUpHandler);

    i[scrollbarYRail].classList.add(cls.state.clicking);

    e.stopPropagation();
    e.preventDefault();
  });
}

var keyboard = function(i) {
  var element = i.element;

  var elementHovered = function () { return matches(element, ':hover'); };
  var scrollbarFocused = function () { return matches(i.scrollbarX, ':focus') || matches(i.scrollbarY, ':focus'); };

  function shouldPreventDefault(deltaX, deltaY) {
    var scrollTop = Math.floor(element.scrollTop);
    if (deltaX === 0) {
      if (!i.scrollbarYActive) {
        return false;
      }
      if (
        (scrollTop === 0 && deltaY > 0) ||
        (scrollTop >= i.contentHeight - i.containerHeight && deltaY < 0)
      ) {
        return !i.settings.wheelPropagation;
      }
    }

    var scrollLeft = element.scrollLeft;
    if (deltaY === 0) {
      if (!i.scrollbarXActive) {
        return false;
      }
      if (
        (scrollLeft === 0 && deltaX < 0) ||
        (scrollLeft >= i.contentWidth - i.containerWidth && deltaX > 0)
      ) {
        return !i.settings.wheelPropagation;
      }
    }
    return true;
  }

  i.event.bind(i.ownerDocument, 'keydown', function (e) {
    if (
      (e.isDefaultPrevented && e.isDefaultPrevented()) ||
      e.defaultPrevented
    ) {
      return;
    }

    if (!elementHovered() && !scrollbarFocused()) {
      return;
    }

    var activeElement = document.activeElement
      ? document.activeElement
      : i.ownerDocument.activeElement;
    if (activeElement) {
      if (activeElement.tagName === 'IFRAME') {
        activeElement = activeElement.contentDocument.activeElement;
      } else {
        // go deeper if element is a webcomponent
        while (activeElement.shadowRoot) {
          activeElement = activeElement.shadowRoot.activeElement;
        }
      }
      if (isEditable(activeElement)) {
        return;
      }
    }

    var deltaX = 0;
    var deltaY = 0;

    switch (e.which) {
      case 37: // left
        if (e.metaKey) {
          deltaX = -i.contentWidth;
        } else if (e.altKey) {
          deltaX = -i.containerWidth;
        } else {
          deltaX = -30;
        }
        break;
      case 38: // up
        if (e.metaKey) {
          deltaY = i.contentHeight;
        } else if (e.altKey) {
          deltaY = i.containerHeight;
        } else {
          deltaY = 30;
        }
        break;
      case 39: // right
        if (e.metaKey) {
          deltaX = i.contentWidth;
        } else if (e.altKey) {
          deltaX = i.containerWidth;
        } else {
          deltaX = 30;
        }
        break;
      case 40: // down
        if (e.metaKey) {
          deltaY = -i.contentHeight;
        } else if (e.altKey) {
          deltaY = -i.containerHeight;
        } else {
          deltaY = -30;
        }
        break;
      case 32: // space bar
        if (e.shiftKey) {
          deltaY = i.containerHeight;
        } else {
          deltaY = -i.containerHeight;
        }
        break;
      case 33: // page up
        deltaY = i.containerHeight;
        break;
      case 34: // page down
        deltaY = -i.containerHeight;
        break;
      case 36: // home
        deltaY = i.contentHeight;
        break;
      case 35: // end
        deltaY = -i.contentHeight;
        break;
      default:
        return;
    }

    if (i.settings.suppressScrollX && deltaX !== 0) {
      return;
    }
    if (i.settings.suppressScrollY && deltaY !== 0) {
      return;
    }

    element.scrollTop -= deltaY;
    element.scrollLeft += deltaX;
    updateGeometry(i);

    if (shouldPreventDefault(deltaX, deltaY)) {
      e.preventDefault();
    }
  });
};

var wheel = function(i) {
  var element = i.element;

  function shouldPreventDefault(deltaX, deltaY) {
    var roundedScrollTop = Math.floor(element.scrollTop);
    var isTop = element.scrollTop === 0;
    var isBottom =
      roundedScrollTop + element.offsetHeight === element.scrollHeight;
    var isLeft = element.scrollLeft === 0;
    var isRight =
      element.scrollLeft + element.offsetWidth === element.scrollWidth;

    var hitsBound;

    // pick axis with primary direction
    if (Math.abs(deltaY) > Math.abs(deltaX)) {
      hitsBound = isTop || isBottom;
    } else {
      hitsBound = isLeft || isRight;
    }

    return hitsBound ? !i.settings.wheelPropagation : true;
  }

  function getDeltaFromEvent(e) {
    var deltaX = e.deltaX;
    var deltaY = -1 * e.deltaY;

    if (typeof deltaX === 'undefined' || typeof deltaY === 'undefined') {
      // OS X Safari
      deltaX = -1 * e.wheelDeltaX / 6;
      deltaY = e.wheelDeltaY / 6;
    }

    if (e.deltaMode && e.deltaMode === 1) {
      // Firefox in deltaMode 1: Line scrolling
      deltaX *= 10;
      deltaY *= 10;
    }

    if (deltaX !== deltaX && deltaY !== deltaY /* NaN checks */) {
      // IE in some mouse drivers
      deltaX = 0;
      deltaY = e.wheelDelta;
    }

    if (e.shiftKey) {
      // reverse axis with shift key
      return [-deltaY, -deltaX];
    }
    return [deltaX, deltaY];
  }

  function shouldBeConsumedByChild(target, deltaX, deltaY) {
    // FIXME: this is a workaround for <select> issue in FF and IE #571
    if (!env.isWebKit && element.querySelector('select:focus')) {
      return true;
    }

    if (!element.contains(target)) {
      return false;
    }

    var cursor = target;

    while (cursor && cursor !== element) {
      if (cursor.classList.contains(cls.element.consuming)) {
        return true;
      }

      var style = get$1(cursor);
      var overflow = [style.overflow, style.overflowX, style.overflowY].join(
        ''
      );

      // if scrollable
      if (overflow.match(/(scroll|auto)/)) {
        var maxScrollTop = cursor.scrollHeight - cursor.clientHeight;
        if (maxScrollTop > 0) {
          if (
            !(cursor.scrollTop === 0 && deltaY > 0) &&
            !(cursor.scrollTop === maxScrollTop && deltaY < 0)
          ) {
            return true;
          }
        }
        var maxScrollLeft = cursor.scrollWidth - cursor.clientWidth;
        if (maxScrollLeft > 0) {
          if (
            !(cursor.scrollLeft === 0 && deltaX < 0) &&
            !(cursor.scrollLeft === maxScrollLeft && deltaX > 0)
          ) {
            return true;
          }
        }
      }

      cursor = cursor.parentNode;
    }

    return false;
  }

  function mousewheelHandler(e) {
    var ref = getDeltaFromEvent(e);
    var deltaX = ref[0];
    var deltaY = ref[1];

    if (shouldBeConsumedByChild(e.target, deltaX, deltaY)) {
      return;
    }

    var shouldPrevent = false;
    if (!i.settings.useBothWheelAxes) {
      // deltaX will only be used for horizontal scrolling and deltaY will
      // only be used for vertical scrolling - this is the default
      element.scrollTop -= deltaY * i.settings.wheelSpeed;
      element.scrollLeft += deltaX * i.settings.wheelSpeed;
    } else if (i.scrollbarYActive && !i.scrollbarXActive) {
      // only vertical scrollbar is active and useBothWheelAxes option is
      // active, so let's scroll vertical bar using both mouse wheel axes
      if (deltaY) {
        element.scrollTop -= deltaY * i.settings.wheelSpeed;
      } else {
        element.scrollTop += deltaX * i.settings.wheelSpeed;
      }
      shouldPrevent = true;
    } else if (i.scrollbarXActive && !i.scrollbarYActive) {
      // useBothWheelAxes and only horizontal bar is active, so use both
      // wheel axes for horizontal bar
      if (deltaX) {
        element.scrollLeft += deltaX * i.settings.wheelSpeed;
      } else {
        element.scrollLeft -= deltaY * i.settings.wheelSpeed;
      }
      shouldPrevent = true;
    }

    updateGeometry(i);

    shouldPrevent = shouldPrevent || shouldPreventDefault(deltaX, deltaY);
    if (shouldPrevent && !e.ctrlKey) {
      e.stopPropagation();
      e.preventDefault();
    }
  }

  if (typeof window.onwheel !== 'undefined') {
    i.event.bind(element, 'wheel', mousewheelHandler);
  } else if (typeof window.onmousewheel !== 'undefined') {
    i.event.bind(element, 'mousewheel', mousewheelHandler);
  }
};

var touch = function(i) {
  if (!env.supportsTouch && !env.supportsIePointer) {
    return;
  }

  var element = i.element;

  function shouldPrevent(deltaX, deltaY) {
    var scrollTop = Math.floor(element.scrollTop);
    var scrollLeft = element.scrollLeft;
    var magnitudeX = Math.abs(deltaX);
    var magnitudeY = Math.abs(deltaY);

    if (magnitudeY > magnitudeX) {
      // user is perhaps trying to swipe up/down the page

      if (
        (deltaY < 0 && scrollTop === i.contentHeight - i.containerHeight) ||
        (deltaY > 0 && scrollTop === 0)
      ) {
        // set prevent for mobile Chrome refresh
        return window.scrollY === 0 && deltaY > 0 && env.isChrome;
      }
    } else if (magnitudeX > magnitudeY) {
      // user is perhaps trying to swipe left/right across the page

      if (
        (deltaX < 0 && scrollLeft === i.contentWidth - i.containerWidth) ||
        (deltaX > 0 && scrollLeft === 0)
      ) {
        return true;
      }
    }

    return true;
  }

  function applyTouchMove(differenceX, differenceY) {
    element.scrollTop -= differenceY;
    element.scrollLeft -= differenceX;

    updateGeometry(i);
  }

  var startOffset = {};
  var startTime = 0;
  var speed = {};
  var easingLoop = null;

  function getTouch(e) {
    if (e.targetTouches) {
      return e.targetTouches[0];
    } else {
      // Maybe IE pointer
      return e;
    }
  }

  function shouldHandle(e) {
    if (e.pointerType && e.pointerType === 'pen' && e.buttons === 0) {
      return false;
    }
    if (e.targetTouches && e.targetTouches.length === 1) {
      return true;
    }
    if (
      e.pointerType &&
      e.pointerType !== 'mouse' &&
      e.pointerType !== e.MSPOINTER_TYPE_MOUSE
    ) {
      return true;
    }
    return false;
  }

  function touchStart(e) {
    if (!shouldHandle(e)) {
      return;
    }

    var touch = getTouch(e);

    startOffset.pageX = touch.pageX;
    startOffset.pageY = touch.pageY;

    startTime = new Date().getTime();

    if (easingLoop !== null) {
      clearInterval(easingLoop);
    }
  }

  function shouldBeConsumedByChild(target, deltaX, deltaY) {
    if (!element.contains(target)) {
      return false;
    }

    var cursor = target;

    while (cursor && cursor !== element) {
      if (cursor.classList.contains(cls.element.consuming)) {
        return true;
      }

      var style = get$1(cursor);
      var overflow = [style.overflow, style.overflowX, style.overflowY].join(
        ''
      );

      // if scrollable
      if (overflow.match(/(scroll|auto)/)) {
        var maxScrollTop = cursor.scrollHeight - cursor.clientHeight;
        if (maxScrollTop > 0) {
          if (
            !(cursor.scrollTop === 0 && deltaY > 0) &&
            !(cursor.scrollTop === maxScrollTop && deltaY < 0)
          ) {
            return true;
          }
        }
        var maxScrollLeft = cursor.scrollLeft - cursor.clientWidth;
        if (maxScrollLeft > 0) {
          if (
            !(cursor.scrollLeft === 0 && deltaX < 0) &&
            !(cursor.scrollLeft === maxScrollLeft && deltaX > 0)
          ) {
            return true;
          }
        }
      }

      cursor = cursor.parentNode;
    }

    return false;
  }

  function touchMove(e) {
    if (shouldHandle(e)) {
      var touch = getTouch(e);

      var currentOffset = { pageX: touch.pageX, pageY: touch.pageY };

      var differenceX = currentOffset.pageX - startOffset.pageX;
      var differenceY = currentOffset.pageY - startOffset.pageY;

      if (shouldBeConsumedByChild(e.target, differenceX, differenceY)) {
        return;
      }

      applyTouchMove(differenceX, differenceY);
      startOffset = currentOffset;

      var currentTime = new Date().getTime();

      var timeGap = currentTime - startTime;
      if (timeGap > 0) {
        speed.x = differenceX / timeGap;
        speed.y = differenceY / timeGap;
        startTime = currentTime;
      }

      if (shouldPrevent(differenceX, differenceY)) {
        e.preventDefault();
      }
    }
  }
  function touchEnd() {
    if (i.settings.swipeEasing) {
      clearInterval(easingLoop);
      easingLoop = setInterval(function() {
        if (i.isInitialized) {
          clearInterval(easingLoop);
          return;
        }

        if (!speed.x && !speed.y) {
          clearInterval(easingLoop);
          return;
        }

        if (Math.abs(speed.x) < 0.01 && Math.abs(speed.y) < 0.01) {
          clearInterval(easingLoop);
          return;
        }

        applyTouchMove(speed.x * 30, speed.y * 30);

        speed.x *= 0.8;
        speed.y *= 0.8;
      }, 10);
    }
  }

  if (env.supportsTouch) {
    i.event.bind(element, 'touchstart', touchStart);
    i.event.bind(element, 'touchmove', touchMove);
    i.event.bind(element, 'touchend', touchEnd);
  } else if (env.supportsIePointer) {
    if (window.PointerEvent) {
      i.event.bind(element, 'pointerdown', touchStart);
      i.event.bind(element, 'pointermove', touchMove);
      i.event.bind(element, 'pointerup', touchEnd);
    } else if (window.MSPointerEvent) {
      i.event.bind(element, 'MSPointerDown', touchStart);
      i.event.bind(element, 'MSPointerMove', touchMove);
      i.event.bind(element, 'MSPointerUp', touchEnd);
    }
  }
};

var defaultSettings = function () { return ({
  handlers: ['click-rail', 'drag-thumb', 'keyboard', 'wheel', 'touch'],
  maxScrollbarLength: null,
  minScrollbarLength: null,
  scrollingThreshold: 1000,
  scrollXMarginOffset: 0,
  scrollYMarginOffset: 0,
  suppressScrollX: false,
  suppressScrollY: false,
  swipeEasing: true,
  useBothWheelAxes: false,
  wheelPropagation: true,
  wheelSpeed: 1,
}); };

var handlers = {
  'click-rail': clickRail,
  'drag-thumb': dragThumb,
  keyboard: keyboard,
  wheel: wheel,
  touch: touch,
};

var PerfectScrollbar = function PerfectScrollbar(element, userSettings) {
  var this$1 = this;
  if ( userSettings === void 0 ) userSettings = {};

  if (typeof element === 'string') {
    element = document.querySelector(element);
  }

  if (!element || !element.nodeName) {
    throw new Error('no element is specified to initialize PerfectScrollbar');
  }

  this.element = element;

  element.classList.add(cls.main);

  this.settings = defaultSettings();
  for (var key in userSettings) {
    this$1.settings[key] = userSettings[key];
  }

  this.containerWidth = null;
  this.containerHeight = null;
  this.contentWidth = null;
  this.contentHeight = null;

  var focus = function () { return element.classList.add(cls.state.focus); };
  var blur = function () { return element.classList.remove(cls.state.focus); };

  this.isRtl = get$1(element).direction === 'rtl';
  this.isNegativeScroll = (function () {
    var originalScrollLeft = element.scrollLeft;
    var result = null;
    element.scrollLeft = -1;
    result = element.scrollLeft < 0;
    element.scrollLeft = originalScrollLeft;
    return result;
  })();
  this.negativeScrollAdjustment = this.isNegativeScroll
    ? element.scrollWidth - element.clientWidth
    : 0;
  this.event = new EventManager();
  this.ownerDocument = element.ownerDocument || document;

  this.scrollbarXRail = div(cls.element.rail('x'));
  element.appendChild(this.scrollbarXRail);
  this.scrollbarX = div(cls.element.thumb('x'));
  this.scrollbarXRail.appendChild(this.scrollbarX);
  this.scrollbarX.setAttribute('tabindex', 0);
  this.event.bind(this.scrollbarX, 'focus', focus);
  this.event.bind(this.scrollbarX, 'blur', blur);
  this.scrollbarXActive = null;
  this.scrollbarXWidth = null;
  this.scrollbarXLeft = null;
  var railXStyle = get$1(this.scrollbarXRail);
  this.scrollbarXBottom = parseInt(railXStyle.bottom, 10);
  if (isNaN(this.scrollbarXBottom)) {
    this.isScrollbarXUsingBottom = false;
    this.scrollbarXTop = toInt(railXStyle.top);
  } else {
    this.isScrollbarXUsingBottom = true;
  }
  this.railBorderXWidth =
    toInt(railXStyle.borderLeftWidth) + toInt(railXStyle.borderRightWidth);
  // Set rail to display:block to calculate margins
  set$1(this.scrollbarXRail, { display: 'block' });
  this.railXMarginWidth =
    toInt(railXStyle.marginLeft) + toInt(railXStyle.marginRight);
  set$1(this.scrollbarXRail, { display: '' });
  this.railXWidth = null;
  this.railXRatio = null;

  this.scrollbarYRail = div(cls.element.rail('y'));
  element.appendChild(this.scrollbarYRail);
  this.scrollbarY = div(cls.element.thumb('y'));
  this.scrollbarYRail.appendChild(this.scrollbarY);
  this.scrollbarY.setAttribute('tabindex', 0);
  this.event.bind(this.scrollbarY, 'focus', focus);
  this.event.bind(this.scrollbarY, 'blur', blur);
  this.scrollbarYActive = null;
  this.scrollbarYHeight = null;
  this.scrollbarYTop = null;
  var railYStyle = get$1(this.scrollbarYRail);
  this.scrollbarYRight = parseInt(railYStyle.right, 10);
  if (isNaN(this.scrollbarYRight)) {
    this.isScrollbarYUsingRight = false;
    this.scrollbarYLeft = toInt(railYStyle.left);
  } else {
    this.isScrollbarYUsingRight = true;
  }
  this.scrollbarYOuterWidth = this.isRtl ? outerWidth(this.scrollbarY) : null;
  this.railBorderYWidth =
    toInt(railYStyle.borderTopWidth) + toInt(railYStyle.borderBottomWidth);
  set$1(this.scrollbarYRail, { display: 'block' });
  this.railYMarginHeight =
    toInt(railYStyle.marginTop) + toInt(railYStyle.marginBottom);
  set$1(this.scrollbarYRail, { display: '' });
  this.railYHeight = null;
  this.railYRatio = null;

  this.reach = {
    x:
      element.scrollLeft <= 0
        ? 'start'
        : element.scrollLeft >= this.contentWidth - this.containerWidth
          ? 'end'
          : null,
    y:
      element.scrollTop <= 0
        ? 'start'
        : element.scrollTop >= this.contentHeight - this.containerHeight
          ? 'end'
          : null,
  };

  this.isAlive = true;

  this.settings.handlers.forEach(function (handlerName) { return handlers[handlerName](this$1); });

  this.lastScrollTop = Math.floor(element.scrollTop); // for onScroll only
  this.lastScrollLeft = element.scrollLeft; // for onScroll only
  this.event.bind(this.element, 'scroll', function (e) { return this$1.onScroll(e); });
  updateGeometry(this);
};

PerfectScrollbar.prototype.update = function update () {
  if (!this.isAlive) {
    return;
  }

  // Recalcuate negative scrollLeft adjustment
  this.negativeScrollAdjustment = this.isNegativeScroll
    ? this.element.scrollWidth - this.element.clientWidth
    : 0;

  // Recalculate rail margins
  set$1(this.scrollbarXRail, { display: 'block' });
  set$1(this.scrollbarYRail, { display: 'block' });
  this.railXMarginWidth =
    toInt(get$1(this.scrollbarXRail).marginLeft) +
    toInt(get$1(this.scrollbarXRail).marginRight);
  this.railYMarginHeight =
    toInt(get$1(this.scrollbarYRail).marginTop) +
    toInt(get$1(this.scrollbarYRail).marginBottom);

  // Hide scrollbars not to affect scrollWidth and scrollHeight
  set$1(this.scrollbarXRail, { display: 'none' });
  set$1(this.scrollbarYRail, { display: 'none' });

  updateGeometry(this);

  processScrollDiff(this, 'top', 0, false, true);
  processScrollDiff(this, 'left', 0, false, true);

  set$1(this.scrollbarXRail, { display: '' });
  set$1(this.scrollbarYRail, { display: '' });
};

PerfectScrollbar.prototype.onScroll = function onScroll (e) {
  if (!this.isAlive) {
    return;
  }

  updateGeometry(this);
  processScrollDiff(this, 'top', this.element.scrollTop - this.lastScrollTop);
  processScrollDiff(
    this,
    'left',
    this.element.scrollLeft - this.lastScrollLeft
  );

  this.lastScrollTop = Math.floor(this.element.scrollTop);
  this.lastScrollLeft = this.element.scrollLeft;
};

PerfectScrollbar.prototype.destroy = function destroy () {
  if (!this.isAlive) {
    return;
  }

  this.event.unbindAll();
  remove(this.scrollbarX);
  remove(this.scrollbarY);
  remove(this.scrollbarXRail);
  remove(this.scrollbarYRail);
  this.removePsClasses();

  // unset elements
  this.element = null;
  this.scrollbarX = null;
  this.scrollbarY = null;
  this.scrollbarXRail = null;
  this.scrollbarYRail = null;

  this.isAlive = false;
};

PerfectScrollbar.prototype.removePsClasses = function removePsClasses () {
  this.element.className = this.element.className
    .split(' ')
    .filter(function (name) { return !name.match(/^ps([-_].+|)$/); })
    .join(' ');
};

var scrollbar = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();



var _react2 = _interopRequireDefault(React__default);





var _perfectScrollbar2 = _interopRequireDefault(PerfectScrollbar);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var handlerNameByEvent = {
  'ps-scroll-y': 'onScrollY',
  'ps-scroll-x': 'onScrollX',
  'ps-scroll-up': 'onScrollUp',
  'ps-scroll-down': 'onScrollDown',
  'ps-scroll-left': 'onScrollLeft',
  'ps-scroll-right': 'onScrollRight',
  'ps-y-reach-start': 'onYReachStart',
  'ps-y-reach-end': 'onYReachEnd',
  'ps-x-reach-start': 'onXReachStart',
  'ps-x-reach-end': 'onXReachEnd'
};
Object.freeze(handlerNameByEvent);

var ScrollBar = function (_Component) {
  _inherits(ScrollBar, _Component);

  function ScrollBar(props) {
    _classCallCheck(this, ScrollBar);

    var _this = _possibleConstructorReturn(this, (ScrollBar.__proto__ || Object.getPrototypeOf(ScrollBar)).call(this, props));

    _this.handleRef = _this.handleRef.bind(_this);
    _this._handlerByEvent = {};
    return _this;
  }

  _createClass(ScrollBar, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this._ps = new _perfectScrollbar2.default(this._container, this.props.option);
      // hook up events
      this._updateEventHook();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      this._updateEventHook(prevProps);
      this._ps.update();
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      var _this2 = this;

      // unhook up evens
      Object.keys(this._handlerByEvent).forEach(function (key) {
        var value = _this2._handlerByEvent[key];

        if (value) {
          _this2._container.removeEventListener(key, value, false);
        }
      });
      this._handlerByEvent = {};
      this._ps.destroy();
      this._ps = null;
    }
  }, {
    key: '_updateEventHook',
    value: function _updateEventHook() {
      var _this3 = this;

      var prevProps = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      // hook up events
      Object.keys(handlerNameByEvent).forEach(function (key) {
        var callback = _this3.props[handlerNameByEvent[key]];
        var prevCallback = prevProps[handlerNameByEvent[key]];
        if (callback !== prevCallback) {
          if (prevCallback) {
            var prevHandler = _this3._handlerByEvent[key];
            _this3._container.removeEventListener(key, prevHandler, false);
            _this3._handlerByEvent[key] = null;
          }
          if (callback) {
            var handler = function handler() {
              return callback(_this3._container);
            };
            _this3._container.addEventListener(key, handler, false);
            _this3._handlerByEvent[key] = handler;
          }
        }
      });
    }
  }, {
    key: 'updateScroll',
    value: function updateScroll() {
      this._ps.update();
    }
  }, {
    key: 'handleRef',
    value: function handleRef(ref) {
      this._container = ref;
      this.props.containerRef(ref);
    }
  }, {
    key: 'render',
    value: function render$$1() {
      var _props = this.props,
          children = _props.children,
          component = _props.component,
          className = _props.className,
          style = _props.style;

      var Comp = component;

      return _react2.default.createElement(
        Comp,
        { style: style, className: 'scrollbar-container ' + className, ref: this.handleRef },
        children
      );
    }
  }]);

  return ScrollBar;
}(React__default.Component);

exports.default = ScrollBar;


ScrollBar.defaultProps = {
  className: '',
  style: undefined,
  option: undefined,
  containerRef: function containerRef() {},
  onScrollY: undefined,
  onScrollX: undefined,
  onScrollUp: undefined,
  onScrollDown: undefined,
  onScrollLeft: undefined,
  onScrollRight: undefined,
  onYReachStart: undefined,
  onYReachEnd: undefined,
  onXReachStart: undefined,
  onXReachEnd: undefined,
  component: 'div'
};

ScrollBar.propTypes = {
  children: propTypes.PropTypes.node.isRequired,
  className: propTypes.PropTypes.string,
  style: propTypes.PropTypes.object,
  option: propTypes.PropTypes.object,
  containerRef: propTypes.PropTypes.func,
  onScrollY: propTypes.PropTypes.func,
  onScrollX: propTypes.PropTypes.func,
  onScrollUp: propTypes.PropTypes.func,
  onScrollDown: propTypes.PropTypes.func,
  onScrollLeft: propTypes.PropTypes.func,
  onScrollRight: propTypes.PropTypes.func,
  onYReachStart: propTypes.PropTypes.func,
  onYReachEnd: propTypes.PropTypes.func,
  onXReachStart: propTypes.PropTypes.func,
  onXReachEnd: propTypes.PropTypes.func,
  component: propTypes.PropTypes.string
};
module.exports = exports['default'];
});

unwrapExports(scrollbar);

var lib$1 = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});



var _scrollbar2 = _interopRequireDefault(scrollbar);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _scrollbar2.default;
module.exports = exports['default'];
});

var PerfectScrollbar$1 = unwrapExports(lib$1);

var ImageListView = function (_React$Component) {
  inherits(ImageListView, _React$Component);

  function ImageListView(props) {
    classCallCheck(this, ImageListView);

    var _this = possibleConstructorReturn(this, (ImageListView.__proto__ || Object.getPrototypeOf(ImageListView)).call(this, props));

    _this.state = {
      selectedItem: null
    };
    return _this;
  }

  createClass(ImageListView, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      ReactTooltip.rebuild();
      if (this.state.selectedItem) {
        var selectedItemId = this.state.selectedItem.id;
        var findIndex = -1;
        this.props.dataSource.forEach(function (item, index) {
          if (item.id === selectedItemId && findIndex < 0) {
            findIndex = index;
          }
        });
        if (findIndex > 0) {
          this.refs[selectedItemId + findIndex].scrollIntoView({ block: 'start' });
        }
      }
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps() {
      if (!this.state.selectedItem) {
        this.selectFirstItem();
      }
    }
  }, {
    key: 'selectFirstItem',
    value: function selectFirstItem() {
      var items = this.props.dataSource || [];
      var selectedItem = null;
      if (items.length) {
        var findSelectedItem = items.find(function (item) {
          return item.selected;
        });
        selectedItem = findSelectedItem || items[0];
      }
      this.setState({
        selectedItem: selectedItem
      });
    }
  }, {
    key: 'handleSelectItem',
    value: function handleSelectItem(item) {
      this.state.selectedItem = item;
      this.setState({
        selectedItem: Object.assign(item)
      });
      if (item.handleClick) {
        item.handleClick();
      }
      this.props.onSelect(item);
    }
  }, {
    key: 'createImagePreview',
    value: function createImagePreview() {
      var _this2 = this;

      var items = this.props.dataSource || [];
      if (!this.props.properties.isGrouped) {
        if (!items.length) {
          return createElement(
            'p',
            { className: styles$5.no_result_label },
            this.props.properties.noResultLabel
          );
        }
        return this.getPatchItemByRow(items).map(function (patchItem, index) {
          return createElement(
            'div',
            { key: 'item' + index,
              className: styles$5.image_row_group },
            _this2.createImages(patchItem)
          );
        });
      }
      var groupedItems = items.reduce(function (rv, x) {
        (rv[x.groupTypeName] = rv[x.groupTypeName] || []).push(x);
        return rv;
      }, {});
      return Object.keys(groupedItems).map(function (key, index) {
        return createElement(
          'div',
          { key: index, className: styles$5.image_group },
          key && key.length ? createElement(
            'div',
            { className: styles$5.image_group_header },
            key
          ) : '',
          _this2.createImages(groupedItems[key]),
          createElement('div', { className: styles$5.image_group_separator })
        );
      });
    }
  }, {
    key: 'getPatchItemByRow',
    value: function getPatchItemByRow(items) {
      var _this3 = this;

      return items.reduce(function (all, currentItem, index) {
        var count = Math.floor(index / _this3.props.properties.countItemIntoRow);
        all[count] = [].concat(all[count] || [], currentItem);
        return all;
      }, []);
    }
  }, {
    key: 'createImages',
    value: function createImages(items) {
      var _this4 = this;

      var _props$properties$ima = this.props.properties.image,
          image = _props$properties$ima === undefined ? {} : _props$properties$ima;

      return items.map(function (item, index) {
        return createElement('img', { key: index,
          className: styles$5.image_preview + ' ' + (_this4.state.selectedItem === item ? styles$5.image_selected : ''),
          'data-tip': item.title,
          width: image ? image.width : 'auto',
          height: image ? image.height : '100%',
          src: item.preview ? item.preview.path : '/',
          alt: '',
          ref: item.id + index,
          onClick: function onClick() {
            _this4.handleSelectItem(item);
          } });
      });
    }
  }, {
    key: 'render',
    value: function render$$1() {
      var _this5 = this;

      var options = {
        suppressScrollX: this.props.properties.type === 'vertical',
        suppressScrollY: this.props.properties.type === 'horizontal'
      };
      return createElement(
        'div',
        { className: styles$5.list_view_container + ' ' + styles$5[this.props.properties.type], style: this.props.style },
        createElement(
          PerfectScrollbar$1,
          { option: options,
            onScrollY: function onScrollY() {
              return ReactTooltip.hide();
            },
            onScrollX: function onScrollX() {
              return ReactTooltip.hide();
            },
            onXReachEnd: function onXReachEnd() {
              if (_this5.props.properties.type === 'horizontal') {
                _this5.props.onListEnd();
              }
            },
            onYReachEnd: function onYReachEnd() {
              if (_this5.props.properties.type === 'vertical') {
                _this5.props.onListEnd();
              }
            } },
          createElement(
            'div',
            { className: styles$5.list_view + ' ' + styles$5[this.props.properties.type] },
            createElement(ReactTooltip, { delayShow: 1000 }),
            this.createImagePreview()
          )
        )
      );
    }
  }]);
  return ImageListView;
}(Component);

var css$a = ".FilterModule_w_filter_container__1_zcP {\n  border: 1px solid #ccc;\n  box-sizing: border-box;\n  padding: 15px 7px 15px 7px;\n}\n\n.FilterModule_w_filter_container__1_zcP div {\n  float: left;\n}\n\n.FilterModule_filter_item__24Da2 {\n  width: 100%;\n  border: 1px solid #bcbcbc;\n  cursor: pointer;\n  margin-bottom: 5px;\n  font-weight: bold;\n  display: block;\n  font-size: 12px;\n}\n\n.FilterModule_filter_item_header__3Bsmp {\n  position: relative;\n  display: flex;\n  justify-content: flex-start;\n  align-items: center;\n  width: 100%;\n}\n\n.FilterModule_filter_item_header__3Bsmp label{\n  cursor: pointer;\n  margin-left: 4px;\n  margin-top: 4px;\n  margin-bottom: 3px;\n  margin-right: 16px;\n}\n\n.FilterModule_filter_title__nSKNs {\n  width: calc(100% - 11px);\n  margin-left: 8px;\n  vertical-align: middle;\n  font-weight: bold;\n  font-size: 13px;\n}\n\n.FilterModule_arrow_up__3ZeI4 {\n  display: block;\n  width: 6px;\n  height: 6px;\n  border-right: 1px solid #626060;\n  border-top: 1px solid #626060;\n  right: 5px;\n  position: absolute;\n  transform: rotate(-45deg);\n}\n\n.FilterModule_arrow_down__2whaF {\n  display: block;\n  width: 6px;\n  height: 6px;\n  border-right: 1px solid #626060;\n  border-top: 1px solid #626060;\n  right: 5px;\n  position: absolute;\n  transform: rotate(135deg);\n}\n\n.FilterModule_filter_body__39IVG {\n  width: 100%;\n}\n\n.FilterModule_filter_value__3wANc {\n  padding: 1px;\n  max-height: 180px;\n  overflow-y: auto;\n  width: 100%;\n  overflow-x: hidden;\n}\n\n.FilterModule_filter_value__3wANc div {\n  margin-bottom: 5px;\n}\n\n.FilterModule_filter_value__3wANc::-webkit-scrollbar-thumb {\n  border-radius: 10px;\n}\n\n.FilterModule_filter_value__3wANc::-webkit-scrollbar-track {\n  border-radius: 10px;\n  background-color: #F5F5F5;\n}\n\n.FilterModule_filter_value__3wANc::-webkit-scrollbar {\n  width: 10px;\n  background-color: #F5F5F5;\n}\n\n.FilterModule_filter_value__3wANc::-webkit-scrollbar-thumb {\n  border-radius: 10px;\n  background-color: #aaa;\n}\n";
var styles$6 = { "w_filter_container": "FilterModule_w_filter_container__1_zcP", "filter_item": "FilterModule_filter_item__24Da2", "filter_item_header": "FilterModule_filter_item_header__3Bsmp", "filter_title": "FilterModule_filter_title__nSKNs", "arrow_up": "FilterModule_arrow_up__3ZeI4", "arrow_down": "FilterModule_arrow_down__2whaF", "filter_body": "FilterModule_filter_body__39IVG", "filter_value": "FilterModule_filter_value__3wANc" };
styleInject(css$a);

var HierarchicalDataSource = function (_DataSource) {
  inherits(HierarchicalDataSource, _DataSource);

  function HierarchicalDataSource() {
    var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    classCallCheck(this, HierarchicalDataSource);

    var _this = possibleConstructorReturn(this, (HierarchicalDataSource.__proto__ || Object.getPrototypeOf(HierarchicalDataSource)).call(this, config));

    _this.level = 0;
    _this.path = [];

    _this.customLoadItem = config.customLoadItem;
    return _this;
  }

  createClass(HierarchicalDataSource, [{
    key: 'getItems',
    value: function getItems() {
      var _this2 = this;

      var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var more = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      this.url = this.config.urls[this.level];
      if (this.path.length) {
        this.path.forEach(function (id, index) {
          _this2.url = _this2.url.replace('{' + index + '}', id);
        });
      }
      return get(HierarchicalDataSource.prototype.__proto__ || Object.getPrototypeOf(HierarchicalDataSource.prototype), 'getItems', this).call(this, params, more);
    }
  }, {
    key: 'goDown',
    value: function goDown(id) {
      var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      this.path.push(id);
      this.level = this.path.length;
      this.state = DataSourceState.DEFAULT;
      if (this.pagination) {
        this.pagination.offset = 0;
        this.result = [].concat(this.config.data || []);
        return this.loadMore();
      }
      return this.getItems(params);
    }
  }, {
    key: 'goUp',
    value: function goUp() {
      var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      this.path.pop();
      this.level = this.path.length;
      this.state = DataSourceState.DEFAULT;
      if (this.pagination) {
        this.pagination.offset = 0;
        this.result = [].concat(this.config.data || []);
        return this.loadMore();
      }
      return this.getItems(params);
    }
  }]);
  return HierarchicalDataSource;
}(DataSource);

var createCollectionItem = function createCollectionItem(title, id, previewImage, brandId) {
  var Collection = function Collection() {
    classCallCheck(this, Collection);

    this.title = title;
    this.id = id;
    this.previewImage = previewImage;
    this.brandId = brandId;
  };

  return new Collection();
};

var createBrandItem = function createBrandItem(id, title, previewImage) {
  var Brand = function Brand() {
    classCallCheck(this, Brand);

    this.id = id;
    this.title = title;
    this.previewImage = previewImage;
  };

  return new Brand();
};

var createWallpaperItem = function createWallpaperItem(title, id, previewImage, fullImage, brandId, collectionId, width) {
  var Wallpaper = function Wallpaper() {
    classCallCheck(this, Wallpaper);

    this.id = id;
    this.title = title;
    this.brandId = brandId;
    this.collection_id = collectionId;
    this.previewImage = previewImage;
    this.fullImage = fullImage;
    this.width = width;
    this.isSelected = false;
  };

  return new Wallpaper();
};

var createRoomItem = function createRoomItem(title, id, roomType, path, status) {
  var Room = function Room() {
    classCallCheck(this, Room);

    this.title = title;
    this.id = id;
    this.roomType = roomType;
    this.previewPath = path;
    this.isError = false;
    this.uploadingStatus = status;
    this.isTooltipVisible = false;
  };

  return new Room();
};

var ROOM_TYPE_ID_WITH_UPLOAD = -1;

var UPLOADING_STATUS = {
  BEFORE_DEPLOY: 'waiting',
  STARTED: 'started',
  IN_PROCESSING: 'inProgress',
  PROCESSED: 'finished',
  ERROR: 'error'
};

var proxyResponse = function proxyResponse(type, response, params) {
  var responseData = isDef(response) ? response.data : [];
  switch (type) {
    case 'rooms':
      return {
        rooms: response.rooms.map(function (room) {
          room.preview.path = '' + params.PREVIEW_PREFIX_URL + room.preview.path;
          room.images = room.images.map(function (image) {
            return Object.assign(image, { path: '' + params.PREVIEW_PREFIX_URL + image.path });
          });
          room.groupTypeName = response.roomTypes.find(function (roomType) {
            return roomType.id === room.roomType[0];
          }).title;
          return room;
        }) || [],
        roomTypes: response.roomTypes || []
      };
    case 'uploadedRooms':
      var uploadedRooms = response.response.data || [];
      var collection = uploadedRooms.images || uploadedRooms;

      return collection.map(function (room) {
        if (room.status === 'NEW') room.status = UPLOADING_STATUS.STARTED;
        if (room.status === 'IN_PROCESSING') room.status = UPLOADING_STATUS.IN_PROCESSING;
        if (room.status === 'PROCESSED') room.status = UPLOADING_STATUS.PROCESSED;
        return createRoomItem(room.id, room.id, [ROOM_TYPE_ID_WITH_UPLOAD], room.path, response.status || room.status);
      });
    case 'brands':
      return responseData.map(function (brand) {
        return {
          id: brand.id,
          title: brand.identifier,
          preview: { path: '' + params.BRAND_PREFIX_URL + brand.preview_image },
          groupTypeName: brand.groupTypeName || '',
          handleClick: brand.handleClick,
          selected: brand.selected
        };
      });
    case 'collections':
      return responseData.map(function (collection) {
        var title = collection.identifier;
        var id = collection.id;
        var previewImage = collection.preview_image;

        return createCollectionItem(title, id, previewImage, params.brandId);
      });
    case 'wallpapers':
      return responseData.map(function (wallpaper) {
        var title = wallpaper.identifier;
        var id = wallpaper.id;
        var previewImage = wallpaper.preview_image;
        var fullImage = wallpaper.tile_image;
        var collectionId = params && params.collectionId ? params.collectionId : null;
        var brandId = params && params.brandId ? params.brandId : null;
        var width = wallpaper.width;

        return createWallpaperItem(title, id, previewImage, fullImage, brandId, collectionId, width);
      });
    case 'all':
      if (!isDef(response)) return {};

      return {
        data: {
          brands: response.data.hasOwnProperty('brands') ? response.data.brands.map(function (brand) {
            var title = brand.identifier;
            var id = brand.id;
            var previewImage = brand.preview_image;

            return createBrandItem(id, title, previewImage);
          }) : [],
          collections: response.data.hasOwnProperty('collections') ? response.data.collections.map(function (collection) {
            var title = collection.identifier;
            var id = collection.id;
            var previewImage = collection.preview_image;

            return createCollectionItem(title, id, previewImage, collection.brand_id);
          }) : [],
          articles: response.data.hasOwnProperty('articles') ? response.data.articles.map(function (wallpaper) {
            var title = wallpaper.identifier;
            var id = wallpaper.id;
            var previewImage = wallpaper.preview_image;
            var fullImage = wallpaper.tile_image;
            var collectionId = params && params.collectionId ? params.collectionId : null;
            var brandId = params && params.brandId ? params.brandId : null;
            var width = wallpaper.width;

            return createWallpaperItem(title, id, previewImage, fullImage, brandId, collectionId, width);
          }) : []
        },
        meta: response.meta
      };
    default:
      return;
  }
};

var isDef = function isDef(response) {
  return response && response.data;
};

var FilterModuleDataSource = function FilterModuleDataSource(config) {
  classCallCheck(this, FilterModuleDataSource);
  this.filterValues = [];

  this.filterValues = config.filterValues;
  this.filterValueChange = config.filterValueChange;
};

var _config;

var PREVIEW_PREFIX = 'http://wallpaper-ost.wizart.tech/static/';
var BRAND_PREFIX = 'http://wallpaper2.exposit.com/files/products/';
var BASE_HREF = 'http://wallpaper-ost.wizart.tech/';
var CURRENT_LANGUAGE = 'ru';

var FILTER_TYPES = {
  RANGE: 'range',
  MULTI_CHECKBOX: 'multiCheckbox',
  RADIO_BUTTONS: 'radioButtons',
  INPUT: 'input'
};

var config = (_config = {}, defineProperty(_config, Components.Wallpaper, {
  dataSource: new HierarchicalDataSource({
    urls: ['http://cache.oboi.aiia.space/api/brand/24/collection/988/article?lang=en'],
    pagination: {
      offset: 0,
      limit: 20
    },
    transformParams: function transformParams(params) {
      return { 'page[limit]': params.limit, 'page[offset]': params.offset };
    },
    transform: function transform(response) {
      return proxyResponse('brands', response, {
        PREVIEW_PREFIX_URL: PREVIEW_PREFIX,
        BRAND_PREFIX_URL: BRAND_PREFIX
      });
    }
  })
}), defineProperty(_config, Components.Filter, {
  dataSource: new DataSource({
    url: 'http://wallpaper2.exposit.com/v2/rooms?lang=' + CURRENT_LANGUAGE,
    pagination: null,
    transform: function transform(response) {
      return proxyResponse('rooms', response.payload || {}, {
        PREVIEW_PREFIX_URL: PREVIEW_PREFIX,
        BRAND_PREFIX_URL: BRAND_PREFIX
      }).roomTypes;
    }
  }),
  belongTo: Components.Rooms,
  filter: function filter(filterData, items) {
    var result = [];
    filterData.forEach(function (data) {
      result = [].concat(toConsumableArray(result), toConsumableArray(items.filter(function (item) {
        return item.roomType.indexOf(data.id) > -1;
      })));
    });
    return result;
  }
}), defineProperty(_config, Components.Rooms, {
  dataSource: new DataSource({
    url: 'http://wallpaper2.exposit.com/v2/rooms?lang=' + CURRENT_LANGUAGE,
    pagination: null,
    transform: function transform(response) {
      return proxyResponse('rooms', response.payload || {}, {
        PREVIEW_PREFIX_URL: PREVIEW_PREFIX,
        BRAND_PREFIX_URL: BRAND_PREFIX
      }).rooms;
    }
  })
}), defineProperty(_config, Components.PreviewResult, {
  dataSource: new DataSource({
    url: 'http://wallpaper2.exposit.com/v3/apply'
  }),
  width: 1.06
}), defineProperty(_config, Components.FilterModule, {
  dataSource: new FilterModuleDataSource({
    filterValues: [{ title: 'Цена', key: 'price', type: FILTER_TYPES.RANGE, values: [{ key: 'from', title: 'От' }, { key: 'to', title: 'До' }] }, { title: 'Наличие', key: 'existence', type: FILTER_TYPES.MULTI_CHECKBOX, values: [{ key: 'stock', title: 'На складе' }] }, {
      title: 'Основной цвет', key: 'color',
      type: FILTER_TYPES.RADIO_BUTTONS,
      values: [{ key: '012', title: 'Черный', value: '#000' }, { key: '245', title: 'Белый', value: '#FFF' }, { key: '563', title: 'Красный', value: 'red' }]
    }, { title: 'Тип покрытия', key: 'typeCoat', type: FILTER_TYPES.MULTI_CHECKBOX,
      values: [{ key: '0235', title: 'Флоковые' }, { key: '23234', title: 'Виниловые' }] }, { title: 'Название', key: 'name', type: FILTER_TYPES.INPUT }],
    filterValueChange: function filterValueChange() {}
  })
}), defineProperty(_config, 'layout', {
  columns: [{
    flex: '0 0 130px',
    offset: '20px',
    rows: [{
      flex: '1 1 100px',
      offset: '20px',
      component: Components.FilterModule,
      properties: {
        type: 'vertical'
      }
    }, {
      flex: '0 1 100px',
      component: Components.Filter,
      properties: {
        header: 'ИНТЕРЬЕРЫ'
      }
    }]
  }, {
    flex: '1 1 auto',
    rows: [{
      columns: [{
        flex: '0 0 335px',
        offset: '20px',
        rows: [{
          flex: '1 1 100px',
          offset: '20px',
          component: Components.Wallpaper,
          properties: {
            type: 'vertical',
            isGrouped: false,
            countItemIntoRow: 3,
            header: 'КОМПАНЬОНЫ',
            noResultLabel: 'Нет товаров по выбранным параметрам фильтрации',
            image: {
              width: '90px',
              height: '90px'
            }
          }
        }]
      }, {
        flex: '1 1 auto',
        rows: [{
          flex: '1 1 100px',
          offset: '20px',
          component: Components.PreviewResult,
          properties: {
            header: 'КОМПАНЬОНЫ',
            walls: []
          }
        }]
      }]
    }, {
      component: Components.Rooms,
      padding: '0 0 25px 0',
      properties: {
        type: 'horizontal',
        isGrouped: true,
        image: {
          width: 'auto',
          height: '90px'
        }
      }
    }]
  }]
}), defineProperty(_config, 'PREVIEW_PREFIX_URL', PREVIEW_PREFIX), defineProperty(_config, 'BRAND_PREFIX_URL', BRAND_PREFIX), defineProperty(_config, 'LANGUAGE', CURRENT_LANGUAGE), defineProperty(_config, 'BASE_HREF', BASE_HREF), _config);

var css$b = ".RangeComponent_filter_container__e3jqc {\n  margin-left: 4px;\n  margin-top: 4px;\n}\n\n.RangeComponent_range_input__29rjM {\n  width: 60px;\n  margin-left: 5px;\n}\n";
var styles$7 = { "filter_container": "RangeComponent_filter_container__e3jqc", "range_input": "RangeComponent_range_input__29rjM" };
styleInject(css$b);

var RangeComponent = function (_React$Component) {
  inherits(RangeComponent, _React$Component);

  function RangeComponent(props) {
    classCallCheck(this, RangeComponent);

    var _this = possibleConstructorReturn(this, (RangeComponent.__proto__ || Object.getPrototypeOf(RangeComponent)).call(this, props));

    _this.state = {
      firstValue: '',
      secondValue: '',
      isValid: true
    };
    return _this;
  }

  createClass(RangeComponent, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      ReactTooltip.rebuild();
    }
  }, {
    key: 'handleRangeChange',
    value: function handleRangeChange(e, index) {
      var value = e.target.value;
      if (value === '' || /^[0-9\b]+$/.test(value)) {
        var changedValue = value;
        var _firstValue = index === 0 ? changedValue : this.state.firstValue;
        var _secondValue = index === 1 ? changedValue : this.state.secondValue;
        this.setState({
          firstValue: _firstValue,
          secondValue: _secondValue
        });
        var isValidValues = _firstValue !== '' && _secondValue !== '' ? +_firstValue <= +_secondValue : true;
        this.setState({
          isValid: isValidValues
        });
        if (isValidValues) {
          var _value;

          this.props.changeRange({
            key: this.props.item.key,
            value: (_value = {}, defineProperty(_value, this.props.item.values[0].key, _firstValue), defineProperty(_value, this.props.item.values[1].key, _secondValue), _value)
          });
        }
      }
    }
  }, {
    key: 'render',
    value: function render$$1() {
      var _this2 = this;

      return this.props.item.values.map(function (value, index) {
        return createElement(
          'div',
          { key: 'range-value' + index,
            'data-tip': value.title,
            className: styles$7.filter_container },
          createElement(
            'label',
            null,
            value.title
          ),
          createElement('input', { className: styles$7.range_input,
            style: { border: _this2.state.isValid ? '' : '1px solid #fd0000' },
            value: index === 0 ? _this2.state.firstValue : _this2.state.secondValue,
            onChange: function onChange(e) {
              return _this2.handleRangeChange(e, index);
            },
            step: 'any' })
        );
      });
    }
  }]);
  return RangeComponent;
}(Component);

var css$c = ".MultiCheckBoxComponent_filter_container__2r55d {\n  width: 100%;\n  margin-left: 4px;\n  margin-top: 4px;\n}\n\n.MultiCheckBoxComponent_w_filter_container__2gRyI div{\n  float: left;\n  position: relative;\n}\n\n.MultiCheckBoxComponent_label_header__2jSzh {\n  font-weight: bold;\n  display: block;\n  margin-bottom: 10px;\n  font-size: 13px;\n}\n\n.MultiCheckBoxComponent_w_filter_container__2gRyI input[type=checkbox] {\n  position: relative;\n  cursor: pointer;\n  margin: 0;\n  visibility: hidden;\n}\n\n.MultiCheckBoxComponent_w_filter_container__2gRyI input[type=checkbox] + span {\n  content: \"\";\n  position: absolute;\n  display: inline-block;\n  width: 14px;\n  height: 14px;\n  top: -1px;\n  left: -8px;\n  border: 1px solid #c0c0c0;\n  border-radius: 3px;\n  background-color: white;\n  cursor: pointer;\n}\n\n.MultiCheckBoxComponent_w_filter_container__2gRyI input[type=checkbox]:checked + span::before {\n  content: \"\";\n  display: block;\n  width: 3px;\n  height: 7px;\n  border: solid #4dd0e1;\n  border-width: 0 2px 2px 0;\n  transform: rotate(45deg);\n  position: absolute;\n  top: 2px;\n  left: 5px;\n}\n\n.MultiCheckBoxComponent_checkboxWrapper__G7ntg {\n  margin-left: 8px;\n  vertical-align: middle;\n  font-weight: bold;\n  font-size: 13px;\n  position: relative;\n}\n";
var styles$8 = { "filter_container": "MultiCheckBoxComponent_filter_container__2r55d", "w_filter_container": "MultiCheckBoxComponent_w_filter_container__2gRyI", "label_header": "MultiCheckBoxComponent_label_header__2jSzh", "checkboxWrapper": "MultiCheckBoxComponent_checkboxWrapper__G7ntg" };
styleInject(css$c);

var MultiCheckBoxComponent = function (_React$Component) {
  inherits(MultiCheckBoxComponent, _React$Component);

  function MultiCheckBoxComponent(props) {
    classCallCheck(this, MultiCheckBoxComponent);

    var _this = possibleConstructorReturn(this, (MultiCheckBoxComponent.__proto__ || Object.getPrototypeOf(MultiCheckBoxComponent)).call(this, props));

    _this.state = {
      selectedCheckBoxes: []
    };
    return _this;
  }

  createClass(MultiCheckBoxComponent, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      ReactTooltip.rebuild();
    }
  }, {
    key: 'handleSelectCheckBox',
    value: function handleSelectCheckBox(item) {
      var changedItemIndex = this.state.selectedCheckBoxes.findIndex(function (value) {
        return value.key === item.key;
      });
      if (changedItemIndex < 0) {
        this.state.selectedCheckBoxes.push(item);
      } else {
        this.state.selectedCheckBoxes.splice(changedItemIndex, 1);
      }
      this.setState({
        selectedCheckBoxes: Object.assign(this.state.selectedCheckBoxes)
      });
      this.props.selectCheckBoxes({
        key: this.props.item.key,
        value: this.state.selectedCheckBoxes.map(function (selectedValue) {
          return selectedValue.key;
        })
      });
    }
  }, {
    key: 'render',
    value: function render$$1() {
      var _this2 = this;

      return this.props.item.values.map(function (property, index) {
        var labelItem = property.title;
        return createElement(
          'div',
          { key: 'checkbox-value' + index,
            className: styles$8.filter_container,
            'data-tip': labelItem },
          createElement(
            'label',
            { className: styles$8.checkboxWrapper },
            createElement('input', { type: 'checkbox',
              checked: _this2.state.selectedCheckBoxes.findIndex(function (item) {
                return item.key === property.key;
              }) >= 0,
              onChange: function onChange() {
                return _this2.handleSelectCheckBox(property);
              } }),
            createElement('span', null),
            labelItem
          )
        );
      });
    }
  }]);
  return MultiCheckBoxComponent;
}(Component);

var css$d = ".RadioButtonComponent_filter_container__2y6Ar {\n  width: 100%;\n  margin-left: 4px;\n  margin-top: 4px;\n}\n\n.RadioButtonComponent_color_filter__1Hyw8 {\n  width: 25px;\n  height: 15px;\n  border: 1px solid #dedede;\n  border-radius: 10px;\n  margin-right: 5px;\n}\n\n.RadioButtonComponent_active_color_filter__111m9 {\n  width: 25px;\n  height: 15px;\n  border-radius: 10px;\n  margin-right: 5px;\n  border: 2px solid #4dd0e1;\n}\n\n.RadioButtonComponent_filter_title__1hrD8 {\n  display: inline-block;\n  width: calc(100% - 35px);\n  white-space: nowrap;\n  overflow: hidden !important;\n  text-overflow: ellipsis;\n}\n";
var styles$9 = { "filter_container": "RadioButtonComponent_filter_container__2y6Ar", "color_filter": "RadioButtonComponent_color_filter__1Hyw8", "active_color_filter": "RadioButtonComponent_active_color_filter__111m9", "filter_title": "RadioButtonComponent_filter_title__1hrD8" };
styleInject(css$d);

var RadioButtonComponent = function (_React$Component) {
  inherits(RadioButtonComponent, _React$Component);

  function RadioButtonComponent(props) {
    classCallCheck(this, RadioButtonComponent);

    var _this = possibleConstructorReturn(this, (RadioButtonComponent.__proto__ || Object.getPrototypeOf(RadioButtonComponent)).call(this, props));

    _this.state = {
      selectedItem: null
    };
    return _this;
  }

  createClass(RadioButtonComponent, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      ReactTooltip.rebuild();
    }
  }, {
    key: 'handleOptionChange',
    value: function handleOptionChange(item) {
      this.setState({
        selectedItem: item
      });
      this.props.selectRadioButton({
        key: this.props.item.key,
        value: item.key
      });
    }
  }, {
    key: 'render',
    value: function render$$1() {
      var _this2 = this;

      return this.props.item.values.map(function (property, index) {
        return createElement(
          'div',
          { key: 'checkbox-value' + index,
            className: styles$9.filter_container,
            onClick: function onClick() {
              return _this2.handleOptionChange(property);
            },
            'data-tip': property.title },
          createElement('div', { style: { background: property.value },
            className: _this2.state.selectedItem === property ? styles$9.active_color_filter : styles$9.color_filter }),
          createElement(
            'div',
            { className: styles$9.filter_title },
            property.title
          )
        );
      });
    }
  }]);
  return RadioButtonComponent;
}(Component);

var css$e = ".InputComponent_filter_container__1KVCe {\n  width: 100%;\n  margin-left: 4px;\n  margin-top: 4px;\n}\n\n.InputComponent_filter_title__yPRfs {\n  display: inline-block;\n  width: calc(100% - 35px);\n  white-space: nowrap;\n  overflow: hidden !important;\n  text-overflow: ellipsis;\n}\n\n.InputComponent_input_filter__2RQ64 {\n  width: calc(100% - 14px);\n}\n";
var styles$a = { "filter_container": "InputComponent_filter_container__1KVCe", "filter_title": "InputComponent_filter_title__yPRfs", "input_filter": "InputComponent_input_filter__2RQ64" };
styleInject(css$e);

var InputComponent = function (_React$Component) {
  inherits(InputComponent, _React$Component);

  function InputComponent(props) {
    classCallCheck(this, InputComponent);

    var _this = possibleConstructorReturn(this, (InputComponent.__proto__ || Object.getPrototypeOf(InputComponent)).call(this, props));

    _this.state = {
      value: ''
    };
    return _this;
  }

  createClass(InputComponent, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      ReactTooltip.rebuild();
    }
  }, {
    key: 'handleChangeText',
    value: function handleChangeText(e) {
      var value = e.target.value;
      if (value.length <= 256) {
        this.setState({
          value: value
        });
        this.props.changeText({
          key: this.props.item.key,
          value: value
        });
      }
    }
  }, {
    key: 'render',
    value: function render$$1() {
      var _this2 = this;

      var labelItem = this.props.item.title;
      return createElement(
        'div',
        { className: styles$a.filter_container,
          'data-tip': labelItem },
        createElement(
          'div',
          { className: styles$a.filter_title },
          labelItem
        ),
        createElement('input', { type: 'text',
          className: styles$a.input_filter,
          onChange: function onChange(e) {
            _this2.handleChangeText(e);
          } })
      );
    }
  }]);
  return InputComponent;
}(Component);

var FilterModule = function (_React$Component) {
  inherits(FilterModule, _React$Component);

  function FilterModule(props) {
    classCallCheck(this, FilterModule);

    var _this = possibleConstructorReturn(this, (FilterModule.__proto__ || Object.getPrototypeOf(FilterModule)).call(this, props));

    _this.state = {
      activeMenu: null,
      filterValue: {}
    };
    return _this;
  }

  createClass(FilterModule, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      ReactTooltip.rebuild();
    }
  }, {
    key: 'setActiveItem',
    value: function setActiveItem(item) {
      this.setState({
        activeMenu: this.state.activeMenu !== item ? item : null
      });
    }
  }, {
    key: 'handleFilterValueChange',
    value: function handleFilterValueChange(data) {
      var _this2 = this;

      this.state.filterValue[data.key] = data.value;
      this.setState({
        filterValue: Object.assign(this.state.filterValue)
      });
      setTimeout(function () {
        _this2.props.filterValueChange(_this2.state.filterValue);
        console.log(_this2.state.filterValue);
      }, 2000);
    }
  }, {
    key: 'createItems',
    value: function createItems() {
      var _this3 = this;

      return (this.props.dataSource || []).map(function (item, index) {
        var isActiveItem = _this3.state.activeMenu === item;
        return createElement(
          'div',
          { key: 'filter-item' + index,
            className: styles$6.filter_item },
          createElement(ReactTooltip, { delayShow: 1000 }),
          createElement(
            'div',
            { key: 'header' + index,
              className: styles$6.filter_item_header,
              onClick: function onClick() {
                return _this3.setActiveItem(item);
              } },
            createElement(
              'label',
              { className: styles$6.filter_title,
                'data-tip': item.title },
              item.title
            ),
            createElement('div', { className: isActiveItem ? styles$6.arrow_up : styles$6.arrow_down })
          ),
          createElement(
            'div',
            { key: 'body' + index,
              style: isActiveItem ? { borderTop: '1px solid #bcbcbc' } : {},
              className: styles$6.filter_body },
            _this3.createFilterItem(item, isActiveItem)
          )
        );
      });
    }
  }, {
    key: 'createFilterItem',
    value: function createFilterItem(item, isActiveItem) {
      return createElement(
        'div',
        { className: styles$6.filter_value,
          style: { visibility: isActiveItem ? 'visible' : 'hidden', height: isActiveItem ? '100%' : '0' } },
        this.getItemByType(item)
      );
    }
  }, {
    key: 'getItemByType',
    value: function getItemByType(item) {
      var _this4 = this;

      switch (item.type) {
        case FILTER_TYPES.MULTI_CHECKBOX:
          return createElement(MultiCheckBoxComponent, { item: item,
            selectCheckBoxes: function selectCheckBoxes(data) {
              return _this4.handleFilterValueChange(data);
            } });
        case FILTER_TYPES.RADIO_BUTTONS:
          return createElement(RadioButtonComponent, { item: item,
            selectRadioButton: function selectRadioButton(data) {
              return _this4.handleFilterValueChange(data);
            } });
        case FILTER_TYPES.RANGE:
          return createElement(RangeComponent, { item: item,
            changeRange: function changeRange(data) {
              return _this4.handleFilterValueChange(data);
            } });
        case FILTER_TYPES.INPUT:
          return createElement(InputComponent, { item: item,
            changeText: function changeText(data) {
              return _this4.handleFilterValueChange(data);
            } });
        default:
          return '';
      }
    }
  }, {
    key: 'render',
    value: function render$$1() {
      return createElement(
        'div',
        { className: styles$6.w_filter_container,
          style: this.props.style },
        this.createItems()
      );
    }
  }]);
  return FilterModule;
}(Component);

var WallpaperWrapper = function (_React$Component) {
  inherits(WallpaperWrapper, _React$Component);

  function WallpaperWrapper(props) {
    classCallCheck(this, WallpaperWrapper);

    var _this = possibleConstructorReturn(this, (WallpaperWrapper.__proto__ || Object.getPrototypeOf(WallpaperWrapper)).call(this, props));

    _this.selectedWalls = [];

    _this.state = {
      isLoading: false,
      filteredRooms: null,
      filterValues: null,
      selectedWallpaper: null,
      selectedRoom: null,
      applyWallpaper: null,
      lines: []
    };
    _this.handlerSelectRoom = _this.handlerSelectRoom.bind(_this);
    _this.handlerSelectWallpaper = _this.handlerSelectWallpaper.bind(_this);
    return _this;
  }

  createClass(WallpaperWrapper, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      var _this2 = this;

      this.getRoomSource().then(function (room) {
        return _this2.getLayout(room);
      }).then(function (lines) {
        _this2.setState({
          lines: lines
        });
        _this2.applyWallpaper(_this2.state.selectedRoom, _this2.state.selectedWallpaper);
      });
      var wallpaperPromise = this.getWallpaperSource() || Promise.resolve();
      wallpaperPromise.then(function () {
        _this2.applyWallpaper(_this2.state.selectedRoom, _this2.state.selectedWallpaper);
      });
      this.getFilterSource();
    }
  }, {
    key: 'render',
    value: function render$$1() {
      return createElement(
        'div',
        { key: 'general-layout',
          className: styles.w_wrapper_container },
        this.generateLayout(this.props.config.layout)
      );
    }
  }, {
    key: 'generateLayout',
    value: function generateLayout(layout) {
      var _this3 = this;

      return layout.columns.map(function (column, index) {
        return createElement(
          'div',
          { key: 'layout' + index + '//',
            style: { flex: column.flex, marginRight: column.offset },
            className: styles.w_column },
          column.rows.map(function (row) {
            if (row.hasOwnProperty('columns')) {
              return createElement(
                'div',
                { key: 'general-layout' + index + '//',
                  className: styles.w_wrapper_container },
                _this3.generateLayout(row)
              );
            } else {
              return row.properties.visibility === 'hidden' ? '' : _this3.createComponent(row);
            }
          })
        );
      });
    }
  }, {
    key: 'getRoomSource',
    value: function getRoomSource() {
      var _this4 = this;

      var dataSource = this.props.config[Components.Rooms].dataSource;
      if (dataSource.state === DataSourceState.ALL_LOADED) {
        if (!this.state.selectedRoom) {
          return dataSource.getItems().then(function (response) {
            var rooms = dataSource.data || dataSource.result;
            var selectedRoom = rooms.length ? rooms[0] : null;
            _this4.setState({
              selectedRoom: selectedRoom
            });
            return Promise.resolve(selectedRoom);
          }).catch(function () {});
        }
        return Promise.resolve(this.state.selectedRoom);
      }
      var pagination = dataSource.pagination;
      var promise = pagination ? dataSource.loadMore() : dataSource.getItems();
      return promise.then(function (response) {
        var rooms = dataSource.data || dataSource.result;
        var selectedRoom = rooms.length ? rooms[0] : null;
        _this4.setState({
          selectedRoom: selectedRoom
        });
        return Promise.resolve(selectedRoom);
      }).catch(function () {});
    }
  }, {
    key: 'getFilterSource',
    value: function getFilterSource() {
      var _this5 = this;

      if (!this.props.config[Components.Filter]) {
        return;
      }
      var dataSource = this.props.config[Components.Filter].dataSource;
      if (dataSource.state === DataSourceState.ALL_LOADED) {
        if (!this.state.filterValues) {
          dataSource.getItems().then(function (response) {
            _this5.setState({
              filterValues: response.map(function (item) {
                return _extends({}, item, { isSelect: true });
              })
            });
            return Promise.resolve();
          }).catch(function () {});
        }
        return;
      }
      var pagination = dataSource.pagination;
      var promise = pagination ? dataSource.loadMore() : dataSource.getItems();
      promise.then(function (response) {
        _this5.setState({
          filterValues: response.map(function (item) {
            return _extends({}, item, { isSelect: true });
          })
        });
        return Promise.resolve();
      }).catch(function () {});
    }
  }, {
    key: 'getWallpaperSource',
    value: function getWallpaperSource() {
      var _this6 = this;

      var dataSource = this.props.config[Components.Wallpaper].dataSource;
      if (dataSource.state === DataSourceState.ALL_LOADED) {
        if (!this.state.selectedWallpaper) {
          return dataSource.getItems().then(function (response) {
            var wallpapers = dataSource.data || dataSource.result;
            var selectedWallpaper = null;
            if (wallpapers.length) {
              var findSelectedWallpaper = wallpapers.find(function (wallpaper) {
                return wallpaper.selected;
              });
              selectedWallpaper = findSelectedWallpaper || wallpapers[0];
            }
            _this6.setState({
              selectedWallpaper: selectedWallpaper
            });
            return Promise.resolve();
          }).catch(function () {});
        }
        return Promise.resolve();
      }
      if (dataSource.customLoadItem) {
        return dataSource.customLoadItem();
      }
      if (dataSource.customLoadItem) {
        return dataSource.customLoadItem();
      }
      var pagination = dataSource.pagination;
      var promise = pagination ? dataSource.loadMore() : dataSource.getItems();
      return promise.then(function (response) {
        if (!_this6.state.selectedWallpaper) {
          var wallpapers = dataSource.data || dataSource.result;
          var _selectedWallpaper = null;
          if (wallpapers.length) {
            var findSelectedWallpaper = wallpapers.find(function (wallpaper) {
              return wallpaper.selected;
            });
            _selectedWallpaper = findSelectedWallpaper || wallpapers[0];
          }
          _this6.setState({
            selectedWallpaper: _selectedWallpaper
          });
        } else {
          _this6.setState({
            selectedWallpaper: Object.assign(_this6.state.selectedWallpaper)
          });
        }
        return Promise.resolve();
      }).catch(function () {});
    }
  }, {
    key: 'handlerSelectRoom',
    value: function handlerSelectRoom(data) {
      if (this.state.selectedRoom === data) {
        return;
      }
      this.selectedWalls = [];
      this.applyWallpaper(data, this.state.selectedWallpaper);
    }
  }, {
    key: 'handlerSelectWallpaper',
    value: function handlerSelectWallpaper(data) {
      var wallIndex = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : -1;

      var dataSource = this.props.config[Components.Wallpaper].dataSource;
      if (dataSource.level >= dataSource.config.urls.length - 1) {
        this.applyWallpaper(this.state.selectedRoom, data, wallIndex);
        return;
      }
      dataSource.goDown(data.id).then(function (response) {
        return;
      });
    }
  }, {
    key: 'handlerFilterChange',
    value: function handlerFilterChange(data) {
      var filterConfig = this.props.config[Components.Filter];
      if (filterConfig.belongTo === Components.Rooms) {
        var dataSource = this.props.config[Components.Rooms].dataSource;
        this.setState({
          filteredRooms: filterConfig.filter(data.filter(function (item) {
            return item.isSelect;
          }), dataSource.data || dataSource.result)
        });
      }
    }
  }, {
    key: 'applyWallpaper',
    value: function applyWallpaper(selectedRoom, selectedWallpaper) {
      var _this7 = this;

      var wallIndex = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : -1;

      if (!selectedRoom || !selectedWallpaper || selectedRoom.isSimple) {
        if (selectedRoom) {
          this.setState({
            selectedRoom: selectedRoom,
            lines: []
          });
        }
        return;
      }

      this.setState({
        isLoading: true,
        lines: []
      });

      var applyConfig = this.props.config[Components.PreviewResult];

      var roomPath = selectedRoom.images[0].path;
      var roomPaths = roomPath.split('/');

      var wallpaperPath = selectedWallpaper.preview.path;
      var wallpaperPaths = wallpaperPath.split('/');
      this.getLayout(selectedRoom).then(function (lines) {

        var applyValue = _this7.state.applyValue;
        var wallpaperSource = _this7.props.config[Components.Wallpaper].dataSource;

        if (wallIndex === -1) {
          applyValue = {
            room_id: _this7.getItemIdByPath(roomPaths) || roomPath,
            wallpapers: []
          };
          for (var i = 0; i < lines.length - 1; i++) {
            applyValue.wallpapers.push({
              wallpaper_id: _this7.getItemIdByPath(wallpaperPaths) || wallpaperPath,
              wallpaper_width: applyConfig.width,
              is_active: true
            });
            _this7.selectedWalls[i] = (wallpaperSource.data || wallpaperSource.result).indexOf(selectedWallpaper);
          }
        } else {
          if (applyValue) {
            applyValue.wallpapers[wallIndex] = {
              wallpaper_id: _this7.getItemIdByPath(wallpaperPaths) || wallpaperPath,
              wallpaper_width: applyConfig.width,
              is_active: true
            };
            _this7.selectedWalls[wallIndex] = (wallpaperSource.data || wallpaperSource.result).indexOf(selectedWallpaper);
          } else {
            applyValue = {
              room_id: _this7.getItemIdByPath(roomPaths) || roomPath,
              wallpapers: []
            };
            for (var _i = 0; _i < lines.length - 1; _i++) {
              applyValue.wallpapers.push({
                wallpaper_id: _this7.getItemIdByPath(wallpaperPaths) || wallpaperPath,
                wallpaper_width: applyConfig.width,
                is_active: _i === wallIndex
              });
              _this7.selectedWalls[_i] = (wallpaperSource.data || wallpaperSource.result).indexOf(selectedWallpaper);
            }
          }
        }

        return applyConfig.dataSource.post(applyValue).then(function (response) {
          var path = response.payload ? response.payload.path : null;
          if (path) {
            _this7.setState({
              applyWallpaper: {
                images: [{
                  path: '' + _this7.props.config.PREVIEW_PREFIX_URL + path,
                  size: response.payload.size
                }]
              },
              applyValue: applyValue,
              selectedRoom: selectedRoom,
              selectedWallpaper: wallIndex === -1 ? selectedWallpaper : _this7.state.selectedWallpaper,
              selectedWallIndex: wallIndex,
              lines: lines
            });
          }
          return Promise.resolve();
        });
      }).catch(function () {
        _this7.setState({
          selectedRoom: selectedRoom,
          selectedWallpaper: selectedWallpaper,
          isLoading: false
        });
      });
    }
  }, {
    key: 'getItemIdByPath',
    value: function getItemIdByPath(paths) {
      var itemId = void 0;
      if (paths.length) {
        var param = paths[paths.length - 1];
        var indexParam = param.indexOf('?');
        itemId = indexParam > 0 ? param.substring(0, indexParam) : param;
      }
      return itemId;
    }
  }, {
    key: 'createComponent',
    value: function createComponent(_ref) {
      var _this8 = this;

      var offset = _ref.offset,
          flex = _ref.flex,
          padding = _ref.padding,
          component = _ref.component,
          _ref$properties = _ref.properties,
          properties = _ref$properties === undefined ? {} : _ref$properties;

      var wallpaperSource = this.props.config[Components.Wallpaper].dataSource;
      switch (component) {
        case Components.Wallpaper:
          return createElement(ImageListView, { key: component,
            style: { marginBottom: offset, flex: flex },
            dataSource: wallpaperSource.data || wallpaperSource.result,
            properties: properties,
            onSelect: function onSelect(data) {
              if (_this8.state.selectedWallpaper === data) {
                return;
              }
              _this8.handlerSelectWallpaper(data);
            },
            onListEnd: function onListEnd() {
              return _this8.getWallpaperSource();
            }
          });
        case Components.Rooms:
          var roomDataSource = this.props.config[Components.Rooms].dataSource;
          return createElement(ImageListView, { key: component,
            style: { marginBottom: offset, flex: flex, padding: padding },
            dataSource: this.state.filteredRooms || roomDataSource.data || roomDataSource.result,
            properties: properties,
            onSelect: function onSelect(data) {
              return _this8.handlerSelectRoom(data);
            },
            onListEnd: function onListEnd() {
              return _this8.getRoomSource();
            }
          });
        case Components.PreviewResult:
          var dataSource = this.state.selectedRoom && this.state.selectedRoom.isSimple ? this.state.selectedRoom : this.state.applyWallpaper || this.state.selectedRoom;
          return createElement(PreviewResult, { style: { marginBottom: offset, flex: flex },
            key: component,
            isLoading: this.state.isLoading,
            dataSource: dataSource,
            lines: this.state.lines,
            wallpapers: wallpaperSource.data || wallpaperSource.result,
            properties: properties,
            onSelect: function onSelect(wallpaper, wallIndex, wallpaperIndex) {
              if (_this8.selectedWalls[wallIndex] === wallpaperIndex) {
                return;
              }
              _this8.selectedWalls[wallIndex] = wallpaperIndex;
              _this8.handlerSelectWallpaper(wallpaper, wallIndex);
            },
            handlerOnLoad: function handlerOnLoad() {
              _this8.setState({
                isLoading: false
              });
            } });
        case Components.Filter:
          return createElement(FilterWallpaper, { key: component,
            style: { marginBottom: offset, flex: flex },
            dataSource: this.state.filterValues,
            onChange: function onChange(data) {
              return _this8.handlerFilterChange(data);
            },
            properties: properties });
        case Components.FilterModule:
          var filterModuleSource = this.props.config[Components.FilterModule].dataSource;
          return createElement(FilterModule, { key: component,
            style: { marginBottom: offset, flex: flex },
            dataSource: filterModuleSource.filterValues,
            properties: properties,
            filterValueChange: function filterValueChange(data) {
              return filterModuleSource.filterValueChange(data);
            } });
      }
    }
  }, {
    key: 'getLayout',
    value: function getLayout(room) {
      if (!room) {
        return Promise.resolve([]);
      }
      var layoutDataSource = new DataSource({
        url: this.props.config.BASE_HREF + 'v3/room_layout/{0}',
        pagination: null
      });
      return layoutDataSource.get({}, [room.id]).then(function (data) {
        var lines = [];
        if (data.payload) {
          lines = data.payload.layout.lines;
        }
        return lines;
      });
    }
  }]);
  return WallpaperWrapper;
}(Component);

var css$f = "/* add css styles here (optional) */\n/* Perfect scrollbar */\n.ps__rail-x,\n.ps__rail-y {\n  opacity: 0.6;\n}\n";
styleInject(css$f);

var _config$1;

var PREVIEW_PREFIX$1 = 'http://wallpaper2.exposit.com/static/';
var BRAND_PREFIX$1 = 'http://wallpaper2.exposit.com/files/products/';
var BASE_HREF$1 = 'http://wallpaper-ost.wizart.tech/';
var CURRENT_LANGUAGE$1 = 'ru';

var config$1 = (_config$1 = {}, defineProperty(_config$1, Components.Wallpaper, {
  dataSource: new HierarchicalDataSource({
    urls: [/*'http://cache.oboi.exposit.com/api/brand?lang=ru',
           'http://cache.oboi.aiia.space/api/brand/{0}/collection?lang=en',
           'http://cache.oboi.aiia.space/api/brand/{0}/collection/{1}/article?lang=en'*/
    'http://cache.oboi.aiia.space/api/brand/24/collection/988/article?lang=en'],
    pagination: {
      offset: 0,
      limit: 10
    },
    data: proxyResponse('brands', {
      data: [{
        id: 44032,
        identifier: '30043-3 Обои A.S. Creation Wood\'n stone',
        preview_image: '30043-3-oboi-as-creation-wood-n-stone-copy-133167.jpg',
        groupTypeName: ''
      }, {
        id: 44034,
        identifier: '7070-17 Обои A.S. Creation Wood\'n stone',
        preview_image: '7070-17-oboi-as-creation-wood-n-stone-14060.jpg',
        groupTypeName: 'КОМПАНЬОНЫ'
      }]
    }, {
      PREVIEW_PREFIX_URL: PREVIEW_PREFIX$1,
      BRAND_PREFIX_URL: BRAND_PREFIX$1
    }),
    transformParams: function transformParams(params) {
      return { 'page[limit]': params.limit, 'page[offset]': params.offset };
    },
    transform: function transform(response) {
      return proxyResponse('brands', response, {
        PREVIEW_PREFIX_URL: PREVIEW_PREFIX$1,
        BRAND_PREFIX_URL: BRAND_PREFIX$1
      });
    }
  })
}), defineProperty(_config$1, Components.Filter, {
  dataSource: new DataSource({
    url: 'http://wallpaper2.exposit.com/v2/rooms?lang=' + CURRENT_LANGUAGE$1,
    pagination: null,
    transform: function transform(response) {
      return proxyResponse('rooms', response.payload || {}, {
        PREVIEW_PREFIX_URL: PREVIEW_PREFIX$1,
        BRAND_PREFIX_URL: BRAND_PREFIX$1
      }).roomTypes;
    }
  }),
  belongTo: Components.Rooms,
  filter: function filter(filterData, items) {
    var result = [];
    filterData.forEach(function (data) {
      result = [].concat(toConsumableArray(result), toConsumableArray(items.filter(function (item) {
        return item.roomType.indexOf(data.id) > -1;
      })));
    });
    return result;
  }
}), defineProperty(_config$1, Components.Rooms, {
  dataSource: new DataSource({
    url: 'http://wallpaper2.exposit.com/v2/rooms?lang=' + CURRENT_LANGUAGE$1,
    pagination: null,
    transform: function transform(response) {
      return proxyResponse('rooms', response.payload || {}, {
        PREVIEW_PREFIX_URL: PREVIEW_PREFIX$1,
        BRAND_PREFIX_URL: BRAND_PREFIX$1
      }).rooms;
    }
  })
}), defineProperty(_config$1, Components.PreviewResult, {
  dataSource: new DataSource({
    url: 'http://wallpaper2.exposit.com/v3/apply_walls'
  }),
  width: 1.06
}), defineProperty(_config$1, 'layout', {
  columns: [{
    flex: '0 0 130px',
    offset: '20px',
    rows: [{
      flex: '1 1 100px',
      offset: '20px',
      component: Components.Wallpaper,
      properties: {
        type: 'vertical',
        isGrouped: true,
        header: 'КОМПАНЬОНЫ',
        noResultLabel: 'Нет товаров по выбранным параметрам фильтрации',
        image: {
          width: '90px',
          height: '90px'
        }
      }
    }, {
      flex: '0 1 100px',
      component: Components.Filter,
      properties: {
        header: 'ИНТЕРЬЕРЫ'
      }
    }]
  }, {
    flex: '1 1 auto',
    rows: [{
      component: Components.PreviewResult,
      offset: '20px',
      properties: {
        header: 'КОМПАНЬОНЫ',
        walls: true
      }
    }, {
      component: Components.Rooms,
      properties: {
        type: 'horizontal',
        isGrouped: true,
        image: {
          width: 'auto',
          height: '90px'
        }
      }
    }]
  }]
}), defineProperty(_config$1, 'PREVIEW_PREFIX_URL', PREVIEW_PREFIX$1), defineProperty(_config$1, 'BRAND_PREFIX_URL', BRAND_PREFIX$1), defineProperty(_config$1, 'LANGUAGE', CURRENT_LANGUAGE$1), defineProperty(_config$1, 'BASE_HREF', BASE_HREF$1), _config$1);

var WallpaperComponent = function (_Component) {
  inherits(WallpaperComponent, _Component);

  function WallpaperComponent() {
    classCallCheck(this, WallpaperComponent);
    return possibleConstructorReturn(this, (WallpaperComponent.__proto__ || Object.getPrototypeOf(WallpaperComponent)).apply(this, arguments));
  }

  createClass(WallpaperComponent, [{
    key: 'render',
    value: function render$$1() {
      var config = this.props.config;


      return React__default.createElement(WallpaperWrapper, { config: config || config$1 });
    }
  }]);
  return WallpaperComponent;
}(Component);

var WallpaperRenderer = function () {
  function WallpaperRenderer() {
    classCallCheck(this, WallpaperRenderer);
  }

  createClass(WallpaperRenderer, null, [{
    key: 'setConfig',
    value: function setConfig(config) {
      WallpaperRenderer.config = config;
    }
  }, {
    key: 'render',
    value: function render$$1(element) {
      render(React__default.createElement(WallpaperComponent, { config: WallpaperRenderer.config }), element);
    }
  }]);
  return WallpaperRenderer;
}();

WallpaperRenderer.Components = {
  Filter: 'FilterWallpaper',
  PreviewResult: 'PreviewResult',
  Rooms: 'RoomGallery',
  Wallpaper: 'WallpaperGallery',
  FilterModule: 'FilterModule'
};


var DataSourceConfig = function () {
  function DataSourceConfig() {
    classCallCheck(this, DataSourceConfig);
  }

  createClass(DataSourceConfig, null, [{
    key: 'createDataSource',
    value: function createDataSource(config) {
      return new DataSource(config);
    }
  }]);
  return DataSourceConfig;
}();

var HierarchicalDataSourceConfig = function () {
  function HierarchicalDataSourceConfig() {
    classCallCheck(this, HierarchicalDataSourceConfig);
  }

  createClass(HierarchicalDataSourceConfig, null, [{
    key: 'createHierarchicalDataSource',
    value: function createHierarchicalDataSource(config) {
      return new HierarchicalDataSource(config);
    }
  }]);
  return HierarchicalDataSourceConfig;
}();

export default WallpaperRenderer;
export { DataSourceConfig, HierarchicalDataSourceConfig, DataSource, HierarchicalDataSource, FilterModule, FilterWallpaper, FilterWallpaperMobile, ImageListView, PreviewResult, proxyResponse };
//# sourceMappingURL=index.es.js.map
